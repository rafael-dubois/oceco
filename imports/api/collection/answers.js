import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Answers = new Mongo.Collection('answers', { idGeneration: 'MONGO' });
