/* global */
/* eslint-disable meteor/no-session */
import { Meteor } from 'meteor/meteor';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Howl } from 'howler';
import { moment } from 'meteor/momentjs:moment';

// import { StatusTimes } from '../helpers.js';

export const podomoroSession = new ReactiveDict('podomoro');

/*
pomodoroStatus : paused, resting
pomodoroTimer
pomodoroCount
lastPlayedTask
playerStatus : play, pause, stop

work : 25 minutes
repo court : 5 minutes
repo long : 15 minutes
*/

export const Clock = {
  init(seconds) {
    this.seconds = seconds;
  },

  actualMinutes() {
    return Math.floor(this.seconds / 60);
  },

  actualSeconds() {
    const realSeconds = (this.seconds % 60);

    return (realSeconds < 10 ? `0${realSeconds}` : realSeconds);
  },

  timer() {
    return `${this.actualMinutes()}:${this.actualSeconds()}`;
  },

  alarm() {
    // console.log('alarm start');
    const sound = new Howl({
      src: ['/sounds/trim.mp3'],
    });
    sound.play();
  },
};

export const Pomodoro = {
  initialize(typeParam) {
    const type = typeParam || 'work';
    Meteor.clearInterval(this.interval);
    if (this.podomoroSessionGet('pomodoroInProgress')) {
      this.totalSeconds = this.podomoroSessionGet('pomodoroRemainingTime');
      this.pomodoroEndTime = this.totalSeconds;
      // console.log('initialize prog');
      Clock.init(this.totalSeconds);
      this.podomoroSessionSet('pomodoroTimer', Clock.timer());
    } else {
      this.totalSeconds = this.StatusTimes()[type];
      this.pomodoroEndTime = this.totalSeconds;
      Clock.init(this.totalSeconds);
      this.podomoroSessionSet('pomodoroTimer', Clock.timer());
    }
  },
  initializeTime(remainingTime) {
    Meteor.clearInterval(this.interval);

    this.totalSeconds = remainingTime;
    this.pomodoroEndTime = this.totalSeconds;

    Clock.init(this.totalSeconds);
    this.podomoroSessionSet('pomodoroTimer', Clock.timer());
  },

  isTimeEnd(startedAt, type) {
    const secondes = this.StatusTimes()[type];
    const endDate = moment(startedAt).add(secondes, 'seconds');
    if (moment().isBefore(endDate)) {
      return true;
    }
    return false; // True
  },
  restartWorkingNotFinish(startedAt, type) {
    // console.log(type);
    const secondes = this.StatusTimes()[type];
    const endDate = moment(startedAt).add(secondes, 'seconds');
    if (moment().isBefore(endDate)) {
      const diff = moment(endDate).diff();
      this.totalSeconds = Math.floor(diff / 1000);
      this.pomodoroEndTime = this.totalSeconds;
      this.podomoroSessionSet('playerStatus', 'play');
      Clock.init(this.totalSeconds);
      this.podomoroSessionSet('pomodoroTimer', Clock.timer());
      // console.log(Clock.timer());
      this.play();
      // console.log(diff / 1000);
      return diff / 1000;
    }
    return false; // True
  },

  restartPausedNotFinish(secondes) {
    this.totalSeconds = Math.floor(secondes);
    this.pomodoroEndTime = this.totalSeconds;
    this.podomoroSessionSet('playerStatus', 'play');
    Clock.init(this.totalSeconds);
    this.podomoroSessionSet('pomodoroTimer', Clock.timer());
    // console.log(Clock.timer());
    this.play();
    return true;
  },

  status() {
    return this.podomoroSessionGet('pomodoroStatus');
  },

  resting() {
    return this.podomoroSessionGet('pomodoroStatus') === 'resting';
  },

  ongoing() {
    // console.log(`ongoing: ${this.podomoroSessionGet('playerStatus')}`);
    return (this.podomoroSessionGet('playerStatus') === 'pause' && !this.resting() && this.podomoroSessionGet('pomodoroStatus') !== undefined);
  },

  sendNotification(title, text) {
    // console.log('sendNotification');
    if (!Meteor.isCordova) {
      if (!Notification) return false;

      // eslint-disable-next-line no-unused-vars
      const notification = new Notification(title, {
        body: text,
        icon: '/icon.png',
      });

      // TODO: Open pomotrail tab
      // notification.onclick = function() {
      // };
    }
    return true;
  },

  requestNotificationPermission() {
    if (!Meteor.isCordova) {
      if (('Notification' in window)) {
        if (Notification.permission !== 'granted') { Notification.requestPermission(); }
      }
    }
  },

  podomoroSessionSet(key, value) {
    podomoroSession.set(key, value);
  },
  podomoroSessionSetDefault(key, value) {
    podomoroSession.setDefault(key, value);
  },

  podomoroSessionGet(key) {
    return podomoroSession.get(key);
  },
  StatusTimes() {
    return {
      work: this.podomoroSessionGet('pomodoroTimeWork'),
      short_rest: this.podomoroSessionGet('pomodoroTimeShortRest'),
      long_rest: this.podomoroSessionGet('pomodoroTimeLongRest'),
    };
  },
  getSecondes() {
    return this.totalSeconds;
  },
  getEndTime() {
    return this.pomodoroEndTime;
  },

  playRest() {
    /* repos */
    Clock.alarm();
    this.sendNotification('Pomodoro terminé', 'Début du temps de repos');

    this.podomoroSessionSet('pomodoroStatus', 'resting');
    // console.log({ actionId: this.podomoroSessionGet('lastPlayedTask'), statusPodomoro: 'resting', secondes: this.totalSeconds, totalSeconds: this.pomodoroEndTime });
    Meteor.call('podomoroActionStatus', {
      actionId: this.podomoroSessionGet('lastPlayedTask'), statusPodomoro: 'resting', secondes: this.totalSeconds, totalSeconds: this.pomodoroEndTime,
    });

    if (this.podomoroSessionGet('pomodoroCount') % 4 === 0) {
      this.initialize('long_rest');
      this.podomoroSessionSet('pomodoroCount', 0);
      Meteor.call('podomoroActionStatus', { statusPodomoro: 'resetPomodoroCount' });
    } else {
      this.initialize('short_rest');
    }

    this.play();
  },

  play() {
    /* travail */
    if (this.ongoing()) {
      this.initialize();
    }

    Meteor.clearInterval(this.interval);

    this.podomoroSessionSet('playerStatus', 'pause');
    // console.log(this.podomoroSessionGet('pomodoroCount'));

    const that = this;

    const timeLeft = function () {
      if (that.totalSeconds > 0) {
        // that.totalSeconds--;
        that.totalSeconds -= 1;
        that.podomoroSessionSet('pomodoroRemainingTime', that.totalSeconds);
        if (that.podomoroSessionGet('pomodoroInProgress') !== true) {
          that.podomoroSessionSet('pomodoroInProgress', true);
        }

        Clock.init(that.totalSeconds);

        that.podomoroSessionSet('pomodoroTimer', Clock.timer());
      } else {
        if (that.ongoing() && that.podomoroSessionGet('pomodoroStatus') !== undefined) {
          that.podomoroSessionSet('pomodoroRemainingTime', null);
          that.podomoroSessionSet('pomodoroInProgress', false);
          that.podomoroSessionSet('pomodoroCount', that.podomoroSessionGet('pomodoroCount') + 1);
          that.playRest();
        } else {
          that.podomoroSessionSet('pomodoroInProgress', false);
          that.podomoroSessionSet('pomodoroRemainingTime', null);

          if (that.podomoroSessionGet('pomodoroStatus') !== undefined) {
            Clock.alarm();
            that.sendNotification('Temps de repos terminé', 'Il est temps de retourner au travail!');
          }
          Meteor.call('podomoroActionStatus', { actionId: that.podomoroSessionGet('lastPlayedTask'), statusPodomoro: 'paused' });

          that.podomoroSessionSet('playerStatus', 'play');
          const pomodoroStatus = that.podomoroSessionGet('pomodoroStatus');
          that.podomoroSessionSet('pomodoroStatus', 'paused');

          that.initialize();
          if (pomodoroStatus === undefined) {
            that.play();
          }
        }

        return Meteor.clearInterval(this.interval);
      }
    };

    this.interval = Meteor.setInterval(timeLeft, 1000);

    return true;
  },

  pause() {
    this.podomoroSessionSet('pomodoroInProgress', false);
    this.podomoroSessionSet('playerStatus', 'play');
    Meteor.clearInterval(this.interval);
    this.podomoroSessionSet('pomodoroTimer', Clock.timer());

    return true;
  },
};
