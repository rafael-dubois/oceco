import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';

import { News } from '../collection/news.js';
import { Documents } from '../collection/documents.js';
import { Citoyens } from '../collection/citoyens.js';
import { Organizations } from '../collection/organizations.js';
import { Projects } from '../collection/projects.js';
import { Events } from '../collection/events.js';
import { Comments } from '../collection/comments.js';
// DDA
import { Actions } from '../collection/actions.js';
import { Resolutions } from '../collection/resolutions.js';
import { Rooms } from '../collection/rooms.js';
import { Proposals } from '../collection/proposals.js';
import { nameToCollection } from '../helpers.js';

// collection
if (Meteor.isClient) {
  if (Meteor.isClient) {
    window.Organizations = Organizations;
    window.Projects = Projects;
    window.Citoyens = Citoyens;
    window.Events = Events;
    window.News = News;
    window.Actions = Actions;
    window.Resolutions = Resolutions;
    window.Rooms = Rooms;
    window.Proposals = Proposals;
  }

  News.helpers({
    authorNews() {
      if (this.targetIsAuthor === 'true') {
        if (this.target && this.target.type && this.target.id) {
          const collection = nameToCollection(this.target.type);
          return collection.findOne({ _id: new Mongo.ObjectID(this.target.id) });
        }
      } else {
        return Citoyens.findOne({ _id: new Mongo.ObjectID(this.author) });
      }
      return undefined;
    },
    targetNews() {
      const queryOptions = {
        fields: {
          _id: 1,
          name: 1,
        },
      };
      if (this.target && this.target.type && this.target.id) {
        const collection = nameToCollection(this.target.type);
        return collection.findOne({ _id: new Mongo.ObjectID(this.target.id) }, queryOptions);
      }
      return undefined;
    },
    objectNews() {
      const queryOptions = {};
      if (this.object && this.object.type === 'actions') {
        queryOptions.fields = {
          _id: 1,
          name: 1,
          idParentRoom: 1,
        };
      } else if (this.object && this.object.type === 'proposals') {
        queryOptions.fields = {
          _id: 1,
          title: 1,
          idParentRoom: 1,
        };
      } else {
        queryOptions.fields = {
          _id: 1,
          name: 1,
        };
      }
      if (this.object && this.object.type && this.object.id) {
        // console.log(this.object.type);
        const collection = nameToCollection(this.object.type);
        return collection.findOne({ _id: new Mongo.ObjectID(this.object.id) }, queryOptions);
      }
      return undefined;
    },
    photoNewsAlbums() {
      if (this.media && this.media.images) {
        const arrayId = this.media.images.map((_id) => new Mongo.ObjectID(_id));
        return Documents.find({ _id: { $in: arrayId } }).fetch();
      }
      return undefined;
    },
    likesCount() {
      if (this.voteUp && this.voteUpCount) {
        return this.voteUpCount;
      }
      return 0;
    },
    dislikesCount() {
      if (this.voteDown && this.voteDownCount) {
        return this.voteDownCount;
      }
      return 0;
    },
    isAuthor() {
      return this.author === Meteor.userId();
    },
    textMentions() {
      if (this.text) {
        let { text } = this;
        if (this.mentions) {
          Object.values(this.mentions).forEach((array) => {
            // text = text.replace(new RegExp(`@${array.value}`, 'g'), `<a href="${Router.path('detailList', {scope:array.type,_id:array.id})}" class="positive">@${array.value}</a>`);
            if (array.slug) {
              text = text.replace(new RegExp(`@?${array.slug}`, 'g'), `<a href="${Router.path('detailList', { scope: array.type, _id: array.id })}" class="positive">@${array.slug}</a>`);
            } else {
              text = text.replace(new RegExp(`@?${array.value}`, 'g'), `<a href="${Router.path('detailList', { scope: array.type, _id: array.id })}" class="positive">@${array.value}</a>`);
            }
          });
        }
        return text;
      }
      return undefined;
    },
    listComments() {
      // console.log('listComments');
      return Comments.find({
        contextId: this._id._str,
      }, { sort: { created: -1 } });
    },
    commentsCount() {
      if (this.commentCount) {
        return this.commentCount;
      }
      return 0;
    },
  });
} else {
  News.helpers({
    photoNewsAlbums() {
      if (this.media && this.media.images) {
        const arrayId = this.media.images.map((_id) => new Mongo.ObjectID(_id));
        return Documents.find({ _id: { $in: arrayId } });
      }
      return undefined;
    },
    authorNews() {
      return Citoyens.findOne({ _id: new Mongo.ObjectID(this.author) });
    },
    likesCount() {
      if (this.voteUp && this.voteUpCount) {
        return this.voteUpCount;
      }
      return 0;
    },
    dislikesCount() {
      if (this.voteDown && this.voteDownCount) {
        return this.voteDownCount;
      }
      return 0;
    },
    isAuthor() {
      return this.author === Meteor.userId();
    },
  });
}
