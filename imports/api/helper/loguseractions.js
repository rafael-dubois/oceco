import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';

import { LogUserActions } from '../collection/loguseractions.js';
import { Actions } from '../collection/actions';
import { Citoyens } from '../collection/citoyens.js';

LogUserActions.helpers({
  action() {
    return Actions.findOne({ _id: new Mongo.ObjectID(this.actionId) });
  },
  citoyen() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) });
  },
  formatCreatedDate() {
    return moment(this.createdAt).format('L LT');
  },
});
