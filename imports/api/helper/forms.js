/* eslint-disable meteor/no-session */
/* global */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import { Router } from 'meteor/iron:router';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { Forms } from '../collection/forms.js';
import { Citoyens } from '../collection/citoyens.js';
import { Answers } from '../collection/answers.js';

import {
  isAdminArray, arrayOrganizerParent, searchQueryAnswers, searchQuerySort,
} from '../helpers.js';

if (Meteor.isClient) {
  import { Chronos } from '../client/chronos.js';

  Forms.helpers({

    isStartDate() {
      if (this.startDate) {
        // todo date pas correct en base
        const start = moment(this.startDate, 'DD/MM/YYYY').toDate();
        return Chronos.moment(start).isBefore(); // True
      }
      return false;
    },
    isNotStartDate() {
      if (this.startDate) {
        const start = moment(this.startDate, 'DD/MM/YYYY').toDate();
        return Chronos.moment().isBefore(start); // True
      }
      return false;
    },
    isEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate, 'DD/MM/YYYY').toDate();
        return Chronos.moment(end).isBefore(); // True
      }
      return false;
    },
    isNotEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate, 'DD/MM/YYYY').toDate();
        return Chronos.moment().isBefore(end); // True
      }
      return false;
    },
  });
} else {
  Forms.helpers({
    isEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate, 'DD/MM/YYYY').toDate();
        return moment(end).isBefore(); // True
      }
      return false;
    },
    isNotEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate, 'DD/MM/YYYY').toDate();
        return moment().isBefore(end); // True
      }
      return false;
    },
  });
}

/*
{
    "_id" : ObjectId("61b1c99b750c0018ce6f4652"),
    "name" : "ocecoform",
    "description" : "dsds",
    "config" : "617b24eed009977344456ed4",
    "parent" : {
        "617b2482cd22dc00073a3124" : {
            "type" : "organizations",
            "name" : "Thanks"
        }
    },
    "subForms" : [
        "aapStep1",
        "aapStep2",
        "aapStep3",
        "aapStep4"
    ],
    "params" : {
        "eee" : "aaa",
        "inputForAnswerName" : "",
        "multiCheckboxPlusurgency" : "",
        "aapStep1" : {
            "onlymemberaccess" : false,
            "adminabsoluteaccess" : false,
            "haveEditingRules" : false,
            "haveReadingRules" : false,
            "canEdit" : "",
            "canRead" : ""
        },
        "aapStep2" : {
            "onlymemberaccess" : false,
            "adminabsoluteaccess" : false,
            "haveEditingRules" : false,
            "haveReadingRules" : false,
            "canEdit" : "Evaluateur,Financeur",
            "canRead" : "Evaluateur,Financeur"
        },
        "aapStep3" : {
            "onlymemberaccess" : false,
            "adminabsoluteaccess" : false,
            "haveEditingRules" : true,
            "haveReadingRules" : false,
            "canEdit" : "Evaluateur",
            "canRead" : "Evaluateur"
        },
        "aapStep4" : {
            "onlymemberaccess" : false,
            "adminabsoluteaccess" : false,
            "haveEditingRules" : false,
            "haveReadingRules" : false,
            "canEdit" : "",
            "canRead" : ""
        },
        "onlymemberaccess" : true,
        "adminabsoluteaccess" : false,
        "budgetdepense" : {
            "group" : [
                "Feature",
                "Costum",
                "Chef de Projet",
                "Data",
                "Mantenance"
            ],
            "nature" : [
                "investissement",
                "fonctionnement"
            ]
        }
    },
    "hasStepValidations" : "",
    "type" : "aap",
    "subType" : "ocecoform",
    "created" : 1639041435,
    "creator" : "5e26e2c0681c3338068b456b",
    "active" : false,
    "anyOnewithLinkCanAnswer" : false,
    "canModify" : true,
    "canReadOtherAnswers" : true,
    "document" : "",
    "endDate" : "17/11/2026",
    "endDateNoconfirmation" : "9/14/2021",
    "image" : "",
    "oneAnswerPerPers" : false,
    "private" : true,
    "showAnswers" : true,
    "startDate" : "9/12/2021",
    "startDateNoconfirmation" : "9/14/2021",
    "temporarymembercanreply" : false,
    "what" : "",
    "withconfirmation" : false,
    "updated" : 1639122421
}
*/

Forms.helpers({
  isVisibleFields() {
    /* if(this.isMe()){
        return true;
      }else{
        if(this.isPublicFields(field)){
          return true;
        }else{
          if(this.isFollowersMe() && this.isPrivateFields(field)){
            return true;
          }else{
            return false;
          }
        }
      } */
    return true;
  },
  /* creatorProfile() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.creator) });
  }, */
  isCreator() {
    return this.creator === Meteor.userId();
  },
  scopeVar() {
    return 'forms';
  },
  scopeEdit() {
    return 'formsEdit';
  },
  listScope() {
    return 'listForms';
  },
  creatorProfile() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.creator) });
  },
  isAdmin(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const citoyen = Citoyens.findOne({ _id: new Mongo.ObjectID(bothUserId) });
    const organizerProject = this.organizerForm();

    if (bothUserId && this.parent) {
      return isAdminArray(organizerProject, citoyen);
    }
  },
  organizerForm() {
    if (this.parent) {
      const childrenParent = arrayOrganizerParent(this.parent, ['organizations', 'projects']);
      if (childrenParent) {
        return childrenParent;
      }
    }
    return undefined;
  },
  isStart() {
    const start = moment(this.startDate, 'D/M/YYYY').toDate();
    return moment(start).isBefore(); // True
  },
  formatStartDate() {
    // todo : utilisé local
    // return moment(this.startDate).format('DD/MM/YYYY HH:mm');
    return moment(this.startDate, 'D/M/YYYY').format('L');
  },
  formatEndDate() {
    // todo : utilisé local
    // return moment(this.endDate).format('DD/MM/YYYY HH:mm');
    return moment(this.endDate, 'D/M/YYYY').format('L');
  },
  formatStartDateIso() {
    // todo : utilisé local
    // return moment(this.startDate).format('DD/MM/YYYY HH:mm');
    return moment(this.startDate, 'D/M/YYYY');
  },
  formatEndDateIso() {
    // todo : utilisé local
    // return moment(this.endDate).format('DD/MM/YYYY HH:mm');
    return moment(this.endDate, 'D/M/YYYY');
  },
  duration() {
    const a = moment(this.startDate, 'D/M/YYYY');
    const b = moment(this.endDate, 'D/M/YYYY');
    const diffInMs = b.diff(a); // 86400000 milliseconds
    // const diffInDays = b.diff(a, 'days'); // 1 day
    const diffInDayText = moment.duration(diffInMs).humanize();
    return diffInDayText;
  },
  listAnswers(search, searchSort) {
    let query = {};

    query['answers.aapStep1'] = { $exists: true };
    query.form = this._id._str;

    if (Meteor.isClient) {
      if (search) {
        query = searchQueryAnswers(query, search);
      }
    }

    const options = {};
    if (Meteor.isClient) {
      if (searchSort) {
        const arraySort = searchQuerySort('answers', searchSort);
        if (arraySort) {
          options.sort = arraySort;
        }
      }
    } else {
      options.sort = {
        created: 1,
      };
    }
    return Answers.find(query, options);
  },
  countAnswers(search) {
    return this.listAnswers(search) && this.listAnswers(search).count();
  },
  answer(answerId) {
    const paramAnswerId = answerId || Router.current().params.answerId;
    return Answers.findOne({ _id: new Mongo.ObjectID(paramAnswerId) });
  },
  budgetdepenseGroup() {
    return this.params && this.params.budgetdepense && this.params.budgetdepense.group.filter(String);
  },
  budgetdepenseNature() {
    return this.params && this.params.budgetdepense && this.params.budgetdepense.nature.filter(String);
  },
  paramsRoles(step, can) {
    if (this.params && this.params[step]) {
      /* "aapStep2": {
        "onlymemberaccess": false,
        "adminabsoluteaccess": false,
        "haveEditingRules": false,
        "haveReadingRules": false,
        "canEdit": "Evaluateur,Financeur",
        "canRead": "Evaluateur,Financeur" */
      const roles = {};
      if (this.params[step][can]) {
        roles[can] = this.params[step][can].split(',');
      } else {
        roles[can] = [];
      }
      return roles[can];
    }
  },
  evaluationCritereArray(index) {
    if (this.evaluationCriteria && this.evaluationCriteria.criterions && this.evaluationCriteria.criterions.length > 0) {
      // this.evaluationCriteria.activateLocalCriteria: true
      // this.evaluationCriteria.type: "starCriterionBased"
      /* criterions Array
      label: "Budget"
      coeff: "1"
      note: "0"
      */
      if (index !== null && index !== '' && this.evaluationCriteria.criterions[index]) {
        this.evaluationCriteria.criterions[index].labelNet = this.evaluationCriteria.criterions[index].label.replace(/\s/g, '')
      }
      let count = 0;
      return index !== null && index !== '' && this.evaluationCriteria.criterions[index] ? [this.evaluationCriteria.criterions[index]] : this.evaluationCriteria.criterions.map((criterion) => {
        criterion.labelNet = criterion.label.replace(/\s/g, '');
        count += 1;
        criterion.index = `addEvaluationAnswer${count}`;
        return criterion;
      });
    }
  },
  evaluationCritereSchema() {
    const arrayObjectCritere = this.evaluationCritereArray();
    const schema = {};
    arrayObjectCritere.forEach((critere) => {
      if (critere.label) {
        if (critere.note) {
          schema[critere.labelNet] = {};
          schema[critere.labelNet].type = Number;
          schema[critere.labelNet].min = Number(0);
          schema[critere.labelNet].max = Number(5);
          schema[critere.labelNet].autoform = { placeholder: critere.label };
          schema[critere.labelNet].autoform = { label: critere.label };
          schema[critere.labelNet].optional = true;
          // schema[critere.labelNet].defaultValue = 0;
        }
      }
    });

    return new SimpleSchema(
      {
        ...schema,
        answerId: {
          type: String,
        },
        parentId: {
          type: String,
        },
        parentType: {
          type: String,
          allowedValues: ['forms'],
        },
      },
      {
        tracker: Tracker,
        clean: {
          filter: true,
          autoConvert: true,
          removeEmptyStrings: true,
          trimStrings: true,
          getAutoValues: true,
          removeNullsFromArrays: true,
        },
      },
    );
  },
});
