/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { moment } from 'meteor/momentjs:moment';
import { Router } from 'meteor/iron:router';

import { Citoyens } from '../collection/citoyens.js';
// import { Lists } from '../collection/lists.js';
import { News } from '../collection/news.js';
import { Events } from '../collection/events.js';
import { Projects } from '../collection/projects.js';
import { Organizations } from '../collection/organizations.js';
import { Documents } from '../collection/documents.js';
import { Rooms } from '../collection/rooms.js';
import { Actions } from '../collection/actions.js';
import { ActivityStream } from '../collection/activitystream.js';
import {
  searchQuery, searchQuerySort, queryOrPrivateScopeLinksIds, queryOrPrivateScopeLinks, arrayLinkProperNoObject, queryLink, queryOptions, queryLinkInter, nameToCollection, applyDiacritics,
} from '../helpers.js';

if (Meteor.isClient) {
  window.Organizations = Organizations;
  window.Citoyens = Citoyens;
  window.Projects = Projects;
  window.Events = Events;

  Citoyens.helpers({
    userCredit() {
      return this.userWallet && this.userWallet[`${Session.get('orgaCibleId')}`] && this.userWallet[`${Session.get('orgaCibleId')}`].userCredits ? this.userWallet[`${Session.get('orgaCibleId')}`].userCredits : 0;
    },
  });
}

Citoyens.helpers({
  isVisibleFields(field) {
    if (this.isMe()) {
      return true;
    }
    if (this.isPublicFields(field)) {
      return true;
    }
    if (this.isFollowersMe() && this.isPrivateFields(field)) {
      return true;
    }
    return false;
  },
  isPublicFields(field) {
    return this.preferences && this.preferences.publicFields && _.contains(this.preferences.publicFields, field);
  },
  isPrivateFields(field) {
    return this.preferences && this.preferences.privateFields && _.contains(this.preferences.privateFields, field);
  },
  formatBirthDate() {
    return moment(this.birthDate).format('L');
  },
  documents() {
    return Documents.find({
      id: this._id._str,
      contentKey: 'profil',
    }, { sort: { created: -1 }, limit: 1 });
  },
  rolesLinks(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].roles && this.links[scopeCible][scopeId].roles.join(',');
  },
  funcRoles(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].roles && this.links[scopeCible][scopeId].roles.join(',');
  },
  isRoles(scope, scopeId, rolesMatch) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].roles && rolesMatch && this.links[scopeCible][scopeId].roles.some((role) => rolesMatch.includes(role));
  },
  isFavorites(scope, scopeId) {
    return !!((this.collections && this.collections.favorites && this.collections.favorites[scope] && this.collections.favorites[scope][scopeId]));
  },
  isScope(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return !!((this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].type && this.isIsInviting(scopeCible, scopeId)));
  },
  isScopeAdmin(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return !!((this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].type && this.isIsAdminInviting(scopeCible, scopeId)));
  },
  isIsInviting(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return !((this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].isInviting));
  },
  isIsAdminInviting(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return !((this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].isAdminInviting));
  },
  isInviting(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return !!((this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].isInviting));
  },
  isAdminInviting(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return !!((this.links && this.links[scopeCible] && this.links[scopeCible][scopeId] && this.links[scopeCible][scopeId].isAdminInviting));
  },
  InvitingUser(scope, scopeId) {
    let scopeCible = scope;
    if (scope === 'organizations') {
      scopeCible = 'memberOf';
    }
    return this.links && this.links[scopeCible] && this.links[scopeCible][scopeId];
  },
  isMe() {
    return this._id._str === Meteor.userId();
  },
  isAdmin() {
    return this._id._str === Meteor.userId();
  },
  isSuperAdmin() {
    return this.roles && this.roles.superAdmin && this.roles.superAdmin === true;
  },
  isFollows(followId) {
    return !!((this.links && this.links.follows && this.links.follows[followId]));
  },
  isFollowsMe() {
    return !!((this.links && this.links.follows && this.links.follows[Meteor.userId()]));
  },
  listFollows(search) {
    if (this.links && this.links.follows) {
      const query = queryLink(this.links.follows, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countFollows(search) {
    // return this.links && this.links.follows && _.size(this.links.follows);
    return this.listFollows(search) && this.listFollows(search).count();
  },
  isFollowers(followId) {
    return !!((this.links && this.links.followers && this.links.followers[followId]));
  },
  isFollowersMe() {
    return !!((this.links && this.links.followers && this.links.followers[Meteor.userId()]));
  },
  listFollowers(search) {
    if (this.links && this.links.followers) {
      const query = queryLink(this.links.followers, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countFollowers(search) {
    // return this.links && this.links.followers && _.size(this.links.followers);
    return this.listFollowers(search) && this.listFollowers(search).count();
  },
  listFriends(search) {
    if (this.links && this.links.followers && this.links.follows) {
      const query = queryLinkInter(this.links.followers, this.links.follows, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countFriends(search) {
    // return this.links && this.links.followers && _.size(this.links.followers);
    return this.listFriends(search) && this.listFriends(search).count();
  },
  listMemberOf(search, selectorga) {
    if (this.links && this.links.memberOf) {
      const queryStart = queryLink(this.links.memberOf, search, selectorga);
      const query = queryOrPrivateScopeLinksIds(queryStart, 'members');
      return Organizations.find(query, queryOptions);
    }
    return false;
  },
  countMemberOf(search, selectorga) {
    return this.listMemberOf(search, selectorga) && this.listMemberOf(search, selectorga).count();
  },
  listEvents(search) {
    if (this.links && this.links.events) {
      const queryStart = queryLink(this.links.events, search);
      const query = queryOrPrivateScopeLinksIds(queryStart, 'attendees');
      return Events.find(query, queryOptions);
    }
    return false;
  },
  countEvents(search) {
    // return this.links && this.links.events && _.size(this.links.events);
    return this.listEvents(search) && this.listEvents(search).count();
  },
  listProjects(search) {
    if (this.links && this.links.projects) {
      const queryStart = queryLink(this.links.projects, search);
      const query = queryOrPrivateScopeLinksIds(queryStart, 'contributors');
      return Projects.find(query, queryOptions);
    }
    return false;
  },
  countProjects(search) {
    // return this.links && this.links.projects && _.size(this.links.projects);
    return this.listProjects(search) && this.listProjects(search).count();
  },
  // Citoyens.findOne().listCollections('favorites','projects',search)
  listCollections(type, collections, search) {
    if (this.collections && this.collections[type] && this.collections[type][collections]) {
      const query = queryLink(this.collections[type][collections], search);
      const collection = nameToCollection(collections);
      return collection.find(query, queryOptions);
    }
    return false;
  },
  countCollections(type, collections, search) {
    return this.listCollections(type, collections, search) && this.listCollections(type, collections, search).count();
  },
  listProjectsCreator() {
    const query = queryOrPrivateScopeLinks('contributors', this._id._str);
    return Projects.find(query);
  },
  countProjectsCreator() {
    return this.listProjectsCreator() && this.listProjectsCreator().count();
  },
  listEventsCreator() {
    queryOptions.fields.startDate = 1;
    queryOptions.fields.startDate = 1;
    queryOptions.fields.geo = 1;
    const query = queryOrPrivateScopeLinks('attendees', this._id._str);
    return Events.find(query, queryOptions);
  },
  countEventsCreator() {
    // return this.links && this.links.events && _.size(this.links.events);
    return this.listEventsCreator() && this.listEventsCreator().count();
  },
  listOrganizationsCreator() {
    const query = queryOrPrivateScopeLinks('members', this._id._str);
    return Organizations.find(query);
  },
  countOrganizationsCreator() {
    return this.listOrganizationsCreator() && this.listOrganizationsCreator().count();
  },
  listActionsCreator(type = 'all', status = 'todo', search, searchSort) {
    const bothUserId = Meteor.userId();
    const query = {};
    const inputDate = new Date();
    const linkUserID = `links.contributors.${bothUserId}`;

    let queryone = {};
    queryone.endDate = { $exists: true, $gte: inputDate };
    queryone[linkUserID] = { $exists: true };
    queryone.status = status;
    if (Meteor.isClient) {
      if (search) {
        queryone = searchQuery(queryone, search);
      }
    }

    let querytwo = {};
    querytwo.endDate = { $exists: false };
    querytwo[linkUserID] = { $exists: true };
    querytwo.status = status;
    if (Meteor.isClient) {
      if (search) {
        querytwo = searchQuery(querytwo, search);
      }
    }

    let querythree = {};
    querythree.endDate = { $exists: false };
    querythree.parentId = { $in: [this._id._str] };
    querythree.status = status;
    if (Meteor.isClient) {
      if (search) {
        querythree = searchQuery(querythree, search);
      }
    }

    let queryfour = {};
    queryfour.endDate = { $exists: true, $gte: inputDate };
    queryfour.parentId = { $in: [this._id._str] };
    queryfour.status = status;
    if (Meteor.isClient) {
      if (search) {
        queryfour = searchQuery(queryfour, search);
      }
    }

    if (type === 'aFaire') {
      queryone.credits = { $gt: 0 };
      querytwo.credits = { $gt: 0 };
      querythree.credits = { $gt: 0 };
      queryfour.credits = { $gt: 0 };
    } else if (type === 'depenses') {
      queryone.credits = { $lt: 0 };
      querytwo.credits = { $lt: 0 };
      querythree.credits = { $lt: 0 };
      queryfour.credits = { $lt: 0 };
    }

    query.$or = [];
    query.$or.push(queryone);
    query.$or.push(querytwo);
    query.$or.push(querythree);
    query.$or.push(queryfour);

    const options = {};
    if (Meteor.isClient) {
      if (searchSort) {
        const arraySort = searchQuerySort('actions', searchSort);
        if (arraySort) {
          options.sort = arraySort;
        }
      }
    } else {
      options.sort = {
        startDate: 1,
      };
    }

    // console.log(query);

    return Actions.find(query, options);
  },
  actionsUserAll(userId, etat, search) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();

    // faire un ou si date pas remplie
    const query = {};
    const inputDate = new Date();
    // query.endDate = { $gte: inputDate };

    const fields = {};
    if (search) {
      // regex qui marche coté serveur parcontre seulement sur un mot
      const searchApplyDiacritics = applyDiacritics(search.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'regex');
      const pattern = new RegExp(`.*${searchApplyDiacritics.replace(/\\/g, '\\\\')}.*`, 'i');
      fields.name = { $regex: pattern };
    }

    const finishedObj = {};
    const finished = `finishedBy.${bothUserId}`;
    if (etat === 'aFaire') {
      finishedObj[finished] = { $exists: false };
    } else if (etat === 'enAttente') {
      finishedObj[finished] = 'toModerate';
    }

    query.$or = [];
    query.$or.push({
      endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj,
    });
    query.$or.push({
      endDate: { $exists: false }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj,
    });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    return Actions.find(query);
  },
  countActionsCreator(type = 'all', status = 'todo', search) {
    return this.listActionsCreator(type, status, search) && this.listActionsCreator(type, status, search).count();
  },
  detailRooms(roomId) {
    // if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id._str)) {
    const query = {};
    if (this.isAdmin()) {
      query._id = new Mongo.ObjectID(roomId);
      query.status = 'open';
    } else {
      query.$or = [];
      const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id._str) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id._str).split(',') : null;
      if (roles) {
        query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: true, $in: roles } });
      }
      query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: false } });
    }
    return Rooms.find(query);
    // }
  },
  room() {
    return Rooms.findOne({ _id: new Mongo.ObjectID(Router.current().params.roomId) });
  },
  listNotifications() {
    return ActivityStream.api.isUnread(this._id._str);
  },
  listNotificationsAsk() {
    return ActivityStream.api.isUnreadAsk(this._id._str);
  },
  scopeVar() {
    return 'citoyens';
  },
  scopeEdit() {
    return 'citoyensEdit';
  },
  listScope() {
    return 'listCitoyens';
  },
  actionIndicatorCount(status) {
    const query = {};
    query.parentId = this._id._str;
    query.parentType = 'citoyens';
    if (status !== 'all' && status !== 'contributors' && status !== 'finished' && status !== 'toValidated') {
      query.status = status;
    }
    if (status === 'contributors') {
      query['links.contributors'] = { $exists: true };
    }
    if (status === 'finished') {
      query.finishedBy = { $exists: true };
    }
    if (status === 'toValidated') {
      query.status = 'todo';
      query.finishedBy = { $exists: true };
    }
    return Actions.find(query);
  },
  creditsUserOrgaTotal(organizationId) {
    const creditsUserOrgaTotal = this.userWallet && this.userWallet[organizationId] && this.userWallet[organizationId].userCredits ? this.userWallet[organizationId].userCredits : false;
    return creditsUserOrgaTotal;
  },
  isTransfertCreditOrgaUser(organizationId, credits) {
    const creditsUserOrgaTotal = this.creditsUserOrgaTotal(organizationId);
    if (creditsUserOrgaTotal && creditsUserOrgaTotal >= credits) {
      return true;
    }
    return false;
  },
  newsJournal(target, userId, limit) {
    const query = {};
    const options = {};
    options.sort = { created: -1 };
    query.$or = [];
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const targetId = (typeof target !== 'undefined') ? target : Router.current().params._id;
    if (Meteor.isClient) {
      // const bothLimit = Session.get('limit');
    } else if (typeof limit !== 'undefined') {
      options.limit = limit;
    }
    const scopeTypeArray = ['public', 'restricted'];
    if (bothUserId === targetId) {
      // scopeTypeArray.push('private');
      query.$or.push({ author: targetId, targetIsAuthor: { $exists: false }, type: 'news' });
      query.$or.push({ 'target.id': targetId });
      // query['$or'].push({'mentions.id':targetId,'scope.type':{$in:scopeTypeArray}});
    } else {
      query.$or.push({
        author: targetId, targetIsAuthor: { $exists: false }, type: 'news', 'scope.type': { $in: scopeTypeArray },
      });
      query.$or.push({ 'target.id': targetId, 'scope.type': { $in: scopeTypeArray } });
      // query['$or'].push({'mentions.id':targetId,'scope.type':{$in:scopeTypeArray}});
    }
    if (bothUserId) {
      query.$or.push({ author: bothUserId, 'target.id': targetId });
    }
    return News.find(query, options);
  },
  newsActus(userId, limit) {
    const query = {};
    const options = {};
    options.sort = { created: -1 };
    query.$or = [];
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    if (Meteor.isClient) {
      // const bothLimit = Session.get('limit');
    } else if (typeof limit !== 'undefined') {
      options.limit = limit;
    }

    let projectsArray = [];
    let eventsArray = [];
    let memberOfArray = [];

    // projects
    if (this.links && this.links.projects) {
      projectsArray = arrayLinkProperNoObject(this.links.projects);
      // projectsArray = _.map(this.links.projects, (a, k) => k);
    }
    // events
    if (this.links && this.links.events) {
      eventsArray = arrayLinkProperNoObject(this.links.events);
      // eventsArray = _.map(this.links.events, (a, k) => k);
    }
    // memberOf
    if (this.links && this.links.memberOf) {
      memberOfArray = arrayLinkProperNoObject(this.links.memberOf);
      // memberOfArray = _.map(this.links.memberOf, (a, k) => k);
    }

    // let arrayIds = _.union(projectsArray, eventsArray, memberOfArray);
    let arrayIds = [...projectsArray, ...eventsArray, ...memberOfArray];
    arrayIds.push(bothUserId);
    arrayIds = arrayIds.filter((element) => element !== undefined);
    query.$or.push({ author: bothUserId });
    query.$or.push({ 'target.id': { $in: arrayIds } });
    query.$or.push({ 'mentions.id': { $in: arrayIds } });
    query.$or.push({ sharedBy: bothUserId });

    // follows
    if (this.links && this.links.follows) {
      const followsArray = _.map(this.links.follows, (a, k) => k);
      query.$or.push({ 'target.id': { $in: followsArray }, 'scope.type': { $in: ['public', 'restricted'] } });
    }
    return News.find(query, options);
  },
  new() {
    // console.log(News.findOne({_id:new Mongo.ObjectID(Router.current().params.newsId)}));
    return Router.current().params.newsId && News.findOne({ _id: new Mongo.ObjectID(Router.current().params.newsId) });
  },
});
