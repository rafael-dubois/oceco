/* eslint-disable consistent-return */
/* eslint-disable import/prefer-default-export */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { Jobs } from 'meteor/wildhart:jobs';
import { moment } from 'meteor/momentjs:moment';

import { ActivityStream } from '../collection/activitystream.js';
import { Events } from '../collection/events.js';
import { Organizations } from '../collection/organizations.js';
import { Projects } from '../collection/projects.js';
import { Rooms } from '../collection/rooms.js';
// eslint-disable-next-line no-unused-vars
import { Citoyens } from '../collection/citoyens.js';

import { nameToCollection, notifyDisplay } from '../helpers.js';
import { orgaIdScope } from '../helpersOrga.js';

ActivityStream.api = {
  Unseen(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    if (Counter.get(`notifications.${bothUserId}.Unseen`)) {
      return Counter.get(`notifications.${bothUserId}.Unseen`);
    }
    return undefined;
  },
  UnseenAsk(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    if (Counter.get(`notifications.${bothUserId}.UnseenAsk`)) {
      return Counter.get(`notifications.${bothUserId}.UnseenAsk`);
    }
    return undefined;
  },
  Unread(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    if (Counter.get(`notifications.${bothUserId}.Unread`)) {
      return Counter.get(`notifications.${bothUserId}.Unread`);
    }
    return undefined;
  },
  queryUnseen(userId, scopeId, scope) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const queryUnseen = { type: 'oceco' };
    queryUnseen[`notify.id.${bothUserId}`] = { $exists: 1 };
    queryUnseen[`notify.id.${bothUserId}.isUnseen`] = true;
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        queryUnseen['target.id'] = bothScopeId;
      } else {
        queryUnseen[`target${scopeCap}.id`] = bothScopeId;
      }
    }
    return ActivityStream.find(queryUnseen, { fields: { _id: 1 } });
  },
  queryUnseenAsk(userId, scopeId, scope) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const queryUnseen = { type: 'oceco' };
    queryUnseen[`notify.id.${bothUserId}`] = { $exists: 1 };
    queryUnseen[`notify.id.${bothUserId}.isUnseen`] = true;
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        queryUnseen['target.id'] = bothScopeId;
      } else {
        queryUnseen[`target${scopeCap}.id`] = bothScopeId;
      }

      queryUnseen.verb = { $in: ['ask'] };
    }
    return ActivityStream.find(queryUnseen, { fields: { _id: 1 } });
  },
  queryUnread(userId, scopeId, scope) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const queryUnread = { type: 'oceco' };
    queryUnread[`notify.id.${bothUserId}`] = { $exists: 1 };
    queryUnread[`notify.id.${bothUserId}.isUnread`] = true;
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        queryUnread['target.id'] = bothScopeId;
      } else {
        queryUnread[`target${scopeCap}.id`] = bothScopeId;
      }
    }
    return ActivityStream.find(queryUnread, { fields: { _id: 1 } });
  },
  isUnread(userId, scopeId, scope, limit) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }
      query.verb = { $nin: ['ask'] };
      query[`notify.id.${bothUserId}.isUnread`] = true;
    } else {
      query[`notify.id.${bothUserId}.isUnread`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options.fields = {};
    options.fields[`notify.id.${bothUserId}.isUnread`] = 1;
    options.fields[`notify.id.${bothUserId}.isUnseen`] = 1;
    options.fields['notify.displayName'] = 1;
    options.fields['notify.labelArray'] = 1;
    options.fields['notify.icon'] = 1;
    options.fields['notify.url'] = 1;
    options.fields['notify.objectType'] = 1;
    options.fields.verb = 1;
    options.fields.target = 1;
    options.fields.targetEvent = 1;
    options.fields.targetRoom =console.log(query);.log(query); 1;
    options.fields.targetProject = 1;
    options.fields.object = 1;
    options.fields.created = 1;
    options.fields.author = 1;
    options.fields.type = 1; */
    if (limit) {
      options.limit = limit;
    }
    return ActivityStream.find(query, options);
  },
  isUnseen(userId, scopeId, scope) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }

      query[`notify.id.${bothUserId}.isUnseen`] = true;
    } else {
      query[`notify.id.${bothUserId}.isUnseen`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options['fields'] = {}
    options['fields'][`notify.id.${bothUserId}`] = 1;
    options['fields']['notify.displayName'] = 1;
    options['fields']['notify.icon'] = 1;
    options['fields']['notify.url'] = 1;
    options['fields']['notify.objectType'] = 1;
    options['fields']['verb'] = 1;
    options['fields']['target'] = 1;
    options['fields']['targetEvent'] = 1;
    options['fields']['targetRoom'] = 1;
    options['fields']['targetProject'] = 1;
    options['fields']['created'] = 1;
    options['fields']['author'] = 1;
    options['fields']['type'] = 1; */
    return ActivityStream.find(query, options);
  },
  isUnseenAsk(userId, scopeId, scope) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query.verb = { $in: ['ask'] };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }
    } else {
      query[`notify.id.${bothUserId}.isUnseen`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options['fields'] = {}
    options['fields'][`notify.id.${bothUserId}`] = 1;
    options['fields']['notify.displayName'] = 1;
    options['fields']['notify.icon'] = 1;
    options['fields']['notify.url'] = 1;
    options['fields']['notify.objectType'] = 1;
    options['fields']['verb'] = 1;
    options['fields']['target'] = 1;
    options['fields']['targetEvent'] = 1;
    options['fields']['targetRoom'] = 1;
    options['fields']['targetProject'] = 1;
    options['fields']['created'] = 1;
    options['fields']['author'] = 1;
    options['fields']['type'] = 1; */
    return ActivityStream.find(query, options);
  },
  isUnreadAsk(userId, scopeId, scope, limit) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query.verb = { $in: ['ask'] };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }
    } else {
      query[`notify.id.${bothUserId}.isUnread`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options.fields = {};
    options.fields[`notify.id.${bothUserId}`] = 1;
    options.fields['notify.displayName'] = 1;
    options.fields['notify.labelArray'] = 1;
    options.fields['notify.icon'] = 1;
    options.fields['notify.url'] = 1;
    options.fields['notify.objectType'] = 1;
    options.fields.verb = 1;
    options.fields.target = 1;
    options.fields.targetEvent = 1;
    options.fields.targetRoom = 1;
    options.fields.targetProject = 1;
    options.fields.object = 1;
    options.fields.created = 1;
    options.fields.author = 1;
    options.fields.type = 1; */
    if (limit) {
      options.limit = limit;
    }
    return ActivityStream.find(query, options);
  },
  // eslint-disable-next-line no-unused-vars
  listUserOrga(array, type, idUser = null, author = null, notificationObj, noAdmin) {
    let arrayIdsUsers = [];
    if (type === 'isAdmin') {
      arrayIdsUsers = Object.keys(array)
        .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && array[k].isAdmin === true)
        .map((k) => k);
    } else if (type === 'isMember') {
      // users
      arrayIdsUsers = Object.keys(array)
        .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && array[k].isAdmin !== true)
        .map((k) => k);
    } else if (type === 'isUser' && idUser) {
      // users
      arrayIdsUsers = Object.keys(array)
        .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && k === idUser)
        .map((k) => k);
    } else if (type === 'isActionMembers') {
      if (noAdmin) {
        const arrayIdsAdmin = Object.keys(array)
          .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && array[k].isAdmin === true)
          .map((k) => k);

        arrayIdsUsers = Object.keys(array)
          .filter((k) => array[k].type === 'citoyens')
          .map((k) => k);

        arrayIdsUsers = arrayIdsUsers.filter((x) => !arrayIdsAdmin.includes(x));
      } else {
        arrayIdsUsers = Object.keys(array)
          .filter((k) => array[k].type === 'citoyens')
          .map((k) => k);
      }
    }

    if (arrayIdsUsers.length > 0) {
      const idUsersObj = {};
      arrayIdsUsers.forEach(function (id) {
        if (author && author.id && author.id === id) {
          // author not notif
        } else {
          idUsersObj[id] = {
            isUnread: true,
            isUnseen: true,
          };
        }
      });
      return idUsersObj;
    }
    return null;
  },
  authorNotif(notificationObj, { id, name, type }) {
    notificationObj.author = {};
    notificationObj.author[id] = {};
    notificationObj.author[id].id = id;
    notificationObj.author[id].name = name;
    notificationObj.author[id].type = type;
    return notificationObj;
  },
  targetNotif(notificationObj, { id, name, type }) {
    notificationObj.target = {};
    notificationObj.target.type = type;
    notificationObj.target.id = id;
    notificationObj.target.name = name;
    return notificationObj;
  },
  objectNotif(notificationObj, { id, name, type }) {
    notificationObj.object = {};
    notificationObj.object[id] = {};
    notificationObj.object[id].id = id;
    notificationObj.object[id].type = type;
    notificationObj.object[id].name = name;
    return notificationObj;
  },
  ocecoNotif(notificationObj, { projectOne, eventOne, roomOne }) {
    // project
    if (projectOne) {
      notificationObj.targetProject = {};
      notificationObj.targetProject.type = 'projects';
      notificationObj.targetProject.id = projectOne._id._str;
      notificationObj.targetProject.name = projectOne.name.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, '');
    }
    // event
    if (eventOne) {
      notificationObj.targetEvent = {};
      notificationObj.targetEvent.type = 'events';
      notificationObj.targetEvent.id = eventOne._id._str;
      notificationObj.targetEvent.name = eventOne.name;
    }

    // room
    if (roomOne) {
      notificationObj.targetRoom = {};
      notificationObj.targetRoom.type = 'rooms';
      notificationObj.targetRoom.id = roomOne._id._str;
      notificationObj.targetRoom.name = roomOne.name;
    }
    return notificationObj;
  },
  isNotificationChat(organizationOne) {
    if (organizationOne && organizationOne.oceco && organizationOne.oceco.notificationChat && organizationOne.oceco.notificationChat === true) {
      // notification chat ok
      return true;
    }
    return false;
  },
};

if (Meteor.isServer) {
  import { apiCommunecter } from '../server/api.js';
  import log from '../../startup/server/logger.js';

  ActivityStream.api.add = ({
    target, object, author, mention,
  }, verb, type = 'isMember', noAdmin = null, idUser = null) => {
    let notificationObj = {};
    notificationObj.type = 'oceco';
    notificationObj.verb = verb;

    let targetObj = target;
    let notificationChat = false;

    if (object && object.type === 'logusercredit' && object.parentType === 'organizations') {
      const organizationOne = Organizations.findOne({ _id: new Mongo.ObjectID(object.parentId) });

      notificationChat = ActivityStream.api.isNotificationChat(organizationOne);

      if (!targetObj) {
        // target
        targetObj = {
          id: organizationOne._id._str, name: organizationOne.name, type: 'organizations', links: organizationOne.links,
        };
      }
    } else if (object && (object.type === 'events' || object.type === 'milestone') && object.parentType === 'projects') {
      const projectOne = Projects.findOne({
        _id: new Mongo.ObjectID(object.parentId),
      });

      // parent
      const orgIdArray = orgaIdScope({ scope: object.parentType, scopeId: object.parentId });
      const orgId = orgIdArray && orgIdArray.length > 0 ? orgIdArray[0] : false;
      const organizationOne = orgId ? Organizations.findOne({ _id: new Mongo.ObjectID(orgId) }) : null;
      notificationChat = organizationOne ? ActivityStream.api.isNotificationChat(organizationOne) : false;

      /* const arrayOrgaIds = Object.keys(projectOne.parent);
      const organizationOne = arrayOrgaIds && arrayOrgaIds.length > 0 ? Organizations.findOne({ _id: new Mongo.ObjectID(arrayOrgaIds[0]) }) : null;
      notificationChat = organizationOne ? ActivityStream.api.isNotificationChat(organizationOne) : false; */

      // remplacer par parents
      /* const project = `links.projects.${projectOne._id._str}`;
      const organizationOne = Organizations.findOne({ [project]: { $exists: 1 } });
      notificationChat = ActivityStream.api.isNotificationChat(organizationOne); */

      if (!targetObj) {
        // target
        targetObj = {
          id: organizationOne._id._str, name: organizationOne.name, type: 'organizations', links: organizationOne.links,
        };
      }

      if (type === 'isActionMembers') {
        if (object && object.links && object.links.contributors) {
          targetObj.links.members = object.links.contributors;
        } else {
          targetObj.links.members = null;
        }
      }

      // console.log(targetObj);
      // console.log(object);

      if (projectOne) {
        // oceco
        notificationObj = ActivityStream.api.ocecoNotif(notificationObj, { projectOne });
      }
    } else if (object && object.type === 'actions' && object.parentType === 'organizations') {
      const organizationOne = Organizations.findOne({ _id: new Mongo.ObjectID(object.parentId) });

      notificationChat = ActivityStream.api.isNotificationChat(organizationOne);

      if (!targetObj) {
        // target
        targetObj = {
          id: organizationOne._id._str, name: organizationOne.name, type: 'organizations', links: organizationOne.links,
        };
      }

      if (type === 'isActionMembers') {
        if (object && object.links && object.links.contributors) {
          targetObj.links.members = object.links.contributors;
        } else {
          targetObj.links.members = null;
        }
      }

      // console.log(targetObj);
      // console.log(object);
      const roomOne = Rooms.findOne({
        _id: new Mongo.ObjectID(object.idParentRoom),
      });

      if (roomOne) {
        // oceco
        notificationObj = ActivityStream.api.ocecoNotif(notificationObj, { roomOne });
      }
    } else if (object && object.type === 'actions' && object.parentType === 'projects') {
      const projectOne = Projects.findOne({
        _id: new Mongo.ObjectID(object.parentId),
      });

      // parent
      const orgIdArray = orgaIdScope({ scope: object.parentType, scopeId: object.parentId });
      const orgId = orgIdArray && orgIdArray.length > 0 ? orgIdArray[0] : false;
      const organizationOne = orgId ? Organizations.findOne({ _id: new Mongo.ObjectID(orgId) }) : null;
      notificationChat = organizationOne ? ActivityStream.api.isNotificationChat(organizationOne) : false;

      /* const arrayOrgaIds = Object.keys(projectOne.parent);
      const organizationOne = arrayOrgaIds && arrayOrgaIds.length > 0 ? Organizations.findOne({ _id: new Mongo.ObjectID(arrayOrgaIds[0]) }) : null;
      notificationChat = organizationOne ? ActivityStream.api.isNotificationChat(organizationOne) : false; */

      /* const project = `links.projects.${projectOne._id._str}`;
      const organizationOne = Organizations.findOne({ [project]: { $exists: 1 } });
      notificationChat = ActivityStream.api.isNotificationChat(organizationOne); */

      if (!targetObj) {
        // target
        targetObj = {
          id: organizationOne._id._str, name: organizationOne.name, type: 'organizations', links: organizationOne.links,
        };
      }

      if (type === 'isActionMembers') {
        if (object && object.links && object.links.contributors) {
          targetObj.links.members = object.links.contributors;
        } else {
          targetObj.links.members = null;
        }
      }

      // console.log(targetObj);
      // console.log(object);
      const roomOne = Rooms.findOne({
        _id: new Mongo.ObjectID(object.idParentRoom),
      });

      if (projectOne && roomOne) {
        // oceco
        notificationObj = ActivityStream.api.ocecoNotif(notificationObj, { projectOne, roomOne });
      }
    } else if (object && object.type === 'actions' && object.parentType === 'events') {
      const eventOne = Events.findOne({
        _id: new Mongo.ObjectID(object.parentId),
      });

      const event = `links.events.${eventOne._id._str}`;
      const projectOne = Projects.findOne({ [event]: { $exists: 1 } });

      // parent
      const orgIdArray = orgaIdScope({ scope: object.parentType, scopeId: object.parentId });
      const orgId = orgIdArray && orgIdArray.length > 0 ? orgIdArray[0] : false;
      const organizationOne = orgId ? Organizations.findOne({ _id: new Mongo.ObjectID(orgId) }) : null;
      notificationChat = organizationOne ? ActivityStream.api.isNotificationChat(organizationOne) : false;

      /* const arrayOrgaIds = Object.keys(projectOne.parent);
      const organizationOne = arrayOrgaIds && arrayOrgaIds.length > 0 ? Organizations.findOne({ _id: new Mongo.ObjectID(arrayOrgaIds[0]) }) : null;
      notificationChat = organizationOne ? ActivityStream.api.isNotificationChat(organizationOne) : false; */

      /* const project = `links.projects.${projectOne._id._str}`;
      const organizationOne = Organizations.findOne({ [project]: { $exists: 1 } });
      notificationChat = ActivityStream.api.isNotificationChat(organizationOne); */

      if (!targetObj) {
        // target
        targetObj = {
          id: organizationOne._id._str, name: organizationOne.name, type: 'organizations', links: organizationOne.links,
        };
      }

      if (type === 'isActionMembers') {
        if (object && object.links && object.links.contributors) {
          targetObj.links.members = object.links.contributors;
        } else {
          targetObj.links.members = null;
        }
      }

      // console.log(targetObj);
      // console.log(object);
      const roomOne = Rooms.findOne({
        _id: new Mongo.ObjectID(object.idParentRoom),
      });

      if (eventOne && projectOne && roomOne) {
        // oceco
        notificationObj = ActivityStream.api.ocecoNotif(notificationObj, { projectOne, eventOne, roomOne });
      }
    }

    if (author) {
      // author
      notificationObj = ActivityStream.api.authorNotif(notificationObj, author);
    }

    if (targetObj) {
      // target
      notificationObj = ActivityStream.api.targetNotif(notificationObj, targetObj);
    }

    if (object) {
      // object
      notificationObj = ActivityStream.api.objectNotif(notificationObj, object);
    }

    notificationObj.created = new Date();
    notificationObj.updated = new Date();

    // notifications
    notificationObj.notify = {};
    // objectType sert à quoi ?
    if (object && object.type) {
      notificationObj.notify.objectType = object.type;
    }

    // listes des users à notifier
    // list des citoyens membre de l'orga

    // console.log(targetObj);
    if (targetObj && targetObj.links && targetObj.links.members) {
      // users / admin
      const idUsersObj = ActivityStream.api.listUserOrga(targetObj.links.members, type, idUser, author, notificationObj, noAdmin);

      if (idUsersObj) {
        /* notificationObj.notify.id['55ed9107e41d75a41a558524'] = {
            isUnread: true,
            isUnseen: true
          }; */

        // pattern du text de la notif
        // const pattern = patternNotif(verb, notificationObj.notify, isAdmin);
        let targetNofifScope = {};
        if (notificationObj && notificationObj.targetEvent) {
          targetNofifScope = { ...notificationObj.targetEvent };
        } else if (notificationObj && !notificationObj.targetEvent && notificationObj.targetProject) {
          targetNofifScope = { ...notificationObj.targetProject };
          // console.log(targetNofifScope);
        } else if (notificationObj && !notificationObj.targetEvent && !notificationObj.targetProject) {
          targetNofifScope = { ...targetObj };
          // console.log(targetNofifScope);
        }

        if (verb === 'join') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} participate to the action {what} from {where}';
              // notificationObj.notify.displayName = '{who} participates to {where}';
              notificationObj.notify.icon = 'fa-group';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'joinAssign') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} assign to the action {what} from {where}';
              // notificationObj.notify.displayName = '{who} participates to {where}';
              notificationObj.notify.icon = 'fa-group';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              notificationObj.notify.labelArray['{mentions}'] = [mention.name];
              notificationObj.notify.labelArray['{mentionsusername}'] = [mention.username];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} assign the action {what} from {where}';
              notificationObj.notify.icon = 'fa-group';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'joinSpent') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} spent credit on {what} from {where}';
              // notificationObj.notify.displayName = '{who} participates to {where}';
              notificationObj.notify.icon = 'fa-money';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'joinAssignSpent') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} spent credit at on {what} from {where}';
              // notificationObj.notify.displayName = '{who} participates to {where}';
              notificationObj.notify.icon = 'fa-money';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              if (mention && mention.name) {
                notificationObj.notify.labelArray['{mentions}'] = [mention.name];
              }
              if (mention && mention.username) {
                notificationObj.notify.labelArray['{mentionsusername}'] = [mention.username];
              }
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'leave') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} leave the action {what} from {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'leaveAssign') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} removed {mentions} from the action {what} from {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              notificationObj.notify.labelArray['{mentions}'] = [mention.name];
              notificationObj.notify.labelArray['{mentionsusername}'] = [mention.username];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} removed you from the action {what} from {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'refund') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} refund for the action {what} from {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'refundUser') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} refund {mentions} from the action {what} from {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              notificationObj.notify.labelArray['{mentions}'] = [mention.name];
              notificationObj.notify.labelArray['{mentionsusername}'] = [mention.username];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} refunded you from the action {what} from {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'finish') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} finished the action {what} from {where}';
              notificationObj.notify.icon = 'fa-calendar-check-o';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'logusercredit') {
          if (object.type === 'logusercredit') {
            if (type === 'isAdmin') {
              // isAdmin
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} to update your credits {what}';
              notificationObj.notify.icon = 'fa-user-shield';
              // notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.credits];
              // notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'validate') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              // isAdmin
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} validated the action {what} from {where}';
              notificationObj.notify.icon = 'fa-check';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'noValidate') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              // isAdmin
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} no validated the action {what} from {where}';
              notificationObj.notify.icon = 'fa-remove';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'add') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} added a new action {what} in {where}';
              notificationObj.notify.icon = 'fa-plus';

              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} added a new action {what} in {where}';
              notificationObj.notify.icon = 'fa-plus';

              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isUser') {
              // isUser
            }
          } else if (object.type === 'events') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} added new events on {where}';
              notificationObj.notify.icon = 'fa-calendar';

              // notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${object.type}/detail/${object.id}`;

              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} added new events on {where}';
              notificationObj.notify.icon = 'fa-calendar';

              // notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/event/${object.id}`;

              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'closeMilestone') {
          if (object.type === 'milestone') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} close milestone {what} on {where}';
              notificationObj.notify.icon = 'fa-flag';
              // notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/${object.id}`;
              // /organizations/555eba56c655675cdd65bf19/projects/milestones/59a123b040bb4e4e5ffca350/milestoneId/Yx6PakKZfL4Rgo7fH
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${object.type}/milestones/${object.id}/milestoneId/${object.milestoneId}`;
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isMember') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} close milestone {what} on {where}';
              notificationObj.notify.icon = 'fa-flag';
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'addSpent') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              // isAdmin
            } else if (type === 'isMember') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = 'a possible expense {what} has been added on {where}';
              notificationObj.notify.icon = 'fa-money';

              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isUser') {
              // isUser
            }
          }
        } else if (verb === 'startAction') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              // isAdmin
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = 'start of the action {what} in {where}';
              notificationObj.notify.icon = 'fa-hourglass-start';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              // notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'alertAction') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              // isAdmin
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = 'alert start of the action {what} in {where}';
              notificationObj.notify.icon = 'fa-exclamation-triangle';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              // notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'endAction') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              // isAdmin
            } else if (type === 'isMember') {
              // isMember
            } else if (type === 'isUser') {
              // isUser
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = 'end of the action {what} in {where}';
              notificationObj.notify.icon = 'fa-hourglass-end';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              // notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'addComment') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} commented on action {what} in {where}';
              // notificationObj.notify.displayNameChat = '{who} commented on action {what} {comment} in {where}';

              notificationObj.notify.icon = 'fa-comments';

              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}/comments`;

              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              // notificationObj.notify.labelArray['{comment}'] = [object.comment];
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} commented on action {what} in {where}';
              notificationObj.notify.icon = 'fa-comments';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'addActionimage' || verb === 'addActionDoc') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} add image on action {what} in {where}';
              // notificationObj.notify.displayNameChat = '{who} commented on action {what} {comment} in {where}';

              notificationObj.notify.icon = 'fa-image';

              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}`;

              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              // notificationObj.notify.labelArray['{comment}'] = [object.comment];
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} commented on action {what} in {where}';
              notificationObj.notify.icon = 'fa-comments';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        } else if (verb === 'checkedTask') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} finished tasked {task} on action {what} in {where}';
              // notificationObj.notify.displayNameChat = '{who} commented on action {what} {comment} in {where}';

              notificationObj.notify.icon = 'fa-check';

              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}/comments`;

              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              // notificationObj.notify.labelArray['{comment}'] = [object.comment];
              notificationObj.notify.labelArray['{task}'] = [object.task];
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} finished tasked {task} on action {what} in {where}';
              notificationObj.notify.icon = 'fa-check';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              notificationObj.notify.labelArray['{task}'] = [object.task];
            }
          }
        } else if (verb === 'addTask') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} add tasked {task} on action {what} in {where}';
              // notificationObj.notify.displayNameChat = '{who} commented on action {what} {comment} in {where}';

              notificationObj.notify.icon = 'fa-check';

              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              notificationObj.notify.urlOceco = `/organizations/${notificationObj.target.id}/${targetNofifScope.type}/rooms/${targetNofifScope.id}/room/${notificationObj.targetRoom.id}/action/${object.id}/comments`;

              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              // notificationObj.notify.labelArray['{comment}'] = [object.comment];
              notificationObj.notify.labelArray['{task}'] = [object.task];
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} add tasked {task} on action {what} in {where}';
              notificationObj.notify.icon = 'fa-check';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
              notificationObj.notify.labelArray['{task}'] = [object.task];
            }
          }
        } else if (verb === 'actionDisabled') {
          if (object.type === 'actions') {
            if (type === 'isAdmin') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} to cancel the action {what} in {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            } else if (type === 'isActionMembers') {
              notificationObj.notify.id = idUsersObj;
              notificationObj.notify.displayName = '{who} to cancel the action {what} in {where}';
              notificationObj.notify.icon = 'fa-times';
              notificationObj.notify.url = `page/type/${targetNofifScope.type}/id/${targetNofifScope.id}/view/coop/room/${notificationObj.targetRoom.id}/action/${object.id}`;
              // labelAuthorObject ne sait pas a quoi ça sert
              notificationObj.notify.labelAuthorObject = 'author';
              // remplacement du pattern
              notificationObj.notify.labelArray = {};
              notificationObj.notify.labelArray['{who}'] = [author.name];
              notificationObj.notify.labelArray['{whousername}'] = [author.username];
              notificationObj.notify.labelArray['{what}'] = [object.name];
              notificationObj.notify.labelArray['{where}'] = [targetNofifScope.name];
            }
          }
        }
      }

      if (notificationObj.notify && notificationObj.notify.id) {
        ActivityStream.insert(notificationObj);
        Jobs.run('pushEmail', notificationObj);
        Jobs.run('pushMobile', notificationObj);
        //
        /* Meteor.defer(() => {
          try {
            if (notificationObj && notificationObj.notify && notificationObj.notify.id && notificationObj.notify.displayName) {
              const title = notificationObj.target && notificationObj.target.name && notificationObj.target.type === 'organizations' ? notificationObj.target.name : 'notification';
              const notifsId = Object.keys(notificationObj.notify.id).map((key) => key);
              // verifier que présent dans Meteor.users
              const notifsIdMeteor = Meteor.users.find({ _id: { $in: notifsId } }, { fields: { _id: 1 } }).map((user) => user._id);
              // console.log(notifsIdMeteor);
              if (notifsIdMeteor && notifsIdMeteor.length > 0) {
                notifsIdMeteor.forEach((value) => {
                  const query = {};
                  query.userId = value;
                  const lang = Meteor.users.findOne({ _id: value }, { fields: { 'profile.language': 1 } });
                  const text = lang && lang.profile.language ? notifyDisplay(notificationObj.notify, lang.profile.language) : notifyDisplay(notificationObj.notify, 'en');
                  const textTarget = `${text} - ${notificationObj.target.name}`;
                  const badge = ActivityStream.api.queryUnseen(value).count();
                  if (Meteor.isDevelopment) {
                    if (value === '55ed9107e41d75a41a558524') {
                      Jobs.run('pushMobileUserToken', title, textTarget, query, badge);
                    } else {
                      // eslint-disable-next-line no-console
                      console.log('pushMobileUserToken', title, textTarget, query, badge);
                    }
                  } else {
                    Jobs.run('pushMobileUserToken', title, textTarget, query, badge);
                  }
                }, title, notificationObj);
              }
            }
          } catch (e) {
            throw log.error(`Problem sending notif ${object.parentType} to ${object.parentId}`, e);
          }
        }); */
      }
      if (notificationObj.notify) {
        if (notificationChat === true && type === 'isAdmin') {
          // mais en fr direct
          const text = notifyDisplay(notificationObj.notify, 'fr', false, true);
          // sous quel user je l'envoie ?
          const attachments = [];
          if (notificationObj.verb === 'addComment') {
            attachments.push({
              color: '#E33551',
              text: object.comment,
              button_alignment: 'horizontal',
              actions: [
                {
                  type: 'button',
                  text: 'Répondre',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/comment-dots-regular.png`,
                  msg_in_chat_window: true,
                  msg_processing_type: 'respondWithMessage',
                  msg: `/oceco-add-comment-action ${object.id} `,
                },
                {
                  type: 'button',
                  text: 'Afficher les commentaires',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/comments-solid.png`,
                  msg_in_chat_window: true,
                  msg: `/oceco-list-comment-action ${object.id} `,
                },
                {
                  type: 'button',
                  text: 'Lien oceco',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/link-solid.png`,
                  url: Meteor.absoluteUrl(notificationObj.notify.urlOceco),
                },
              ],
            });
          } else if (notificationObj.verb === 'addActionimage') {
            attachments.push({
              color: '#E33551',
              image_url: `${Meteor.settings.public.urlimage}${object.docPath}`,
              button_alignment: 'horizontal',
              actions: [
                {
                  type: 'button',
                  text: 'Lien oceco',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/link-solid.png`,
                  url: Meteor.absoluteUrl(notificationObj.notify.urlOceco),
                },
              ],
            });
          } else if (notificationObj.verb === 'addActionDoc') {
            attachments.push({
              color: '#E33551',
              title: `${Meteor.settings.public.urlimage}${object.docPath}`,
              title_link: `${Meteor.settings.public.urlimage}${object.docPath}`,
              button_alignment: 'horizontal',
              actions: [
                {
                  type: 'button',
                  text: 'Lien oceco',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/link-solid.png`,
                  url: Meteor.absoluteUrl(notificationObj.notify.urlOceco),
                },
              ],
            });
          } else if (notificationObj.verb === 'checkedTask') {
            attachments.push({
              color: '#E33551',
              text: `\`\`\`[x] ${object.task}\`\`\``,
              button_alignment: 'horizontal',
              actions: [
                {
                  type: 'button',
                  text: 'Répondre',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/comment-dots-regular.png`,
                  msg_in_chat_window: true,
                  msg_processing_type: 'respondWithMessage',
                  msg: `/oceco-add-comment-action ${object.id} `,
                },
                {
                  type: 'button',
                  text: 'Afficher les commentaires',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/comments-solid.png`,
                  msg_in_chat_window: true,
                  msg: `/oceco-list-comment-action ${object.id} `,
                },
                {
                  type: 'button',
                  text: 'Lien oceco',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/link-solid.png`,
                  url: Meteor.absoluteUrl(notificationObj.notify.urlOceco),
                },
              ],
            });
          } else if (notificationObj.verb === 'addTask') {
            attachments.push({
              color: '#E33551',
              text: `\`\`\`[ ] ${object.task}\`\`\``,
              button_alignment: 'horizontal',
              actions: [
                {
                  type: 'button',
                  text: 'Répondre',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/comment-dots-regular.png`,
                  msg_in_chat_window: true,
                  msg_processing_type: 'respondWithMessage',
                  msg: `/oceco-add-comment-action ${object.id} `,
                },
                {
                  type: 'button',
                  text: 'Afficher les commentaires',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/comments-solid.png`,
                  msg_in_chat_window: true,
                  msg: `/oceco-list-comment-action ${object.id} `,
                },
                {
                  type: 'button',
                  text: 'Lien oceco',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/link-solid.png`,
                  url: Meteor.absoluteUrl(notificationObj.notify.urlOceco),
                },
              ],
            });
          } else if (notificationObj.verb === 'add' && (object.startDate || object.endDate)) {
            // Todo : date locale
            moment.locale('fr');
            const startDate = moment(object.startDate).format('LLL');
            const endDate = moment(object.endDate).format('LLL');
            const dateEventTxt = startDate && endDate ? `Début : ${startDate} - Fin : ${endDate}` : `Début : ${startDate}` || `Fin : ${endDate}`;
            attachments.push({
              color: '#E33551',
              text: `\`\`\`${dateEventTxt}\`\`\``,
              button_alignment: 'horizontal',
              actions: [
                {
                  type: 'button',
                  text: 'Lien oceco',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/link-solid.png`,
                  url: Meteor.absoluteUrl(notificationObj.notify.urlOceco),
                },
              ],
            });
          } else if (notificationObj.notify.urlOceco) {
            // text = `${text} [lien oceco](${Meteor.absoluteUrl(notificationObj.notify.urlOceco)})`;
            // console.log(text);
            attachments.push({
              color: '#E33551',
              button_alignment: 'horizontal',
              actions: [
                {
                  type: 'button',
                  text: 'Lien oceco',
                  image_url: `${Meteor.settings.public.rocketchat.host}/images/link-solid.png`,
                  url: Meteor.absoluteUrl(notificationObj.notify.urlOceco),
                },
              ],
            });
          }

          Meteor.defer(() => {
            try {
              ActivityStream.api.sendRC(author.id, object.parentType, object.parentId, text, attachments);
            } catch (e) {
              // console.error(`Problem sending chat notif ${object.parentType} to ${object.parentId}`, e);
              throw log.error(`Problem sending chat notif ${object.parentType} to ${object.parentId}`, e);
            }
          });
        }
      }
    }
  };

  ActivityStream.api.sendRC = (userId, parentType, parentId, msg, attachments) => {
    const collectionScope = nameToCollection(parentType);
    const scopeOne = collectionScope.findOne({
      _id: new Mongo.ObjectID(parentId), 'tools.chat': { $exists: true }, slug: { $exists: true },
    });

    /*
    version simple avec un chat par element en utilisant le slug
    mais si ça change coté communecter il faudra adapter

    oceco.notificationChat
    */

    /*
    il faut que l'user ce soit deja connecter au chat une fois pour ecrire à ça place
    */

    if (scopeOne) {
      const params = {};
      params.text = msg;
      if (attachments) {
        params.attachments = attachments;
      }
      const retour = apiCommunecter.callRCPostMessage(scopeOne.slug, params, userId);
      return retour;
    }
  };
}

ActivityStream.helpers({
  authorId() {
    const keyArray = _.map(this.author, (a, k) => k);
    return keyArray[0];
  },
});
