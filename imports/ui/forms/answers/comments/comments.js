/* eslint-disable meteor/no-session */
/* global Session IonModal IonToast */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';
import { AutoForm } from 'meteor/aldeed:autoform';
import { IonActionSheet } from 'meteor/meteoric:ionic';
import { _ } from 'meteor/underscore';

// collection
import { Events } from '../../../../api/collection/events.js';
import { Organizations } from '../../../../api/collection/organizations.js';
import { Projects } from '../../../../api/collection/projects.js';
import { Citoyens } from '../../../../api/collection/citoyens.js';
import { Answers } from '../../../../api/collection/answers.js';
import { Comments } from '../../../../api/collection/comments.js';

// submanager
// import { singleSubs } from '../../../../api/client/subsmanager.js';

import { nameToCollection } from '../../../../api/helpers.js';

import './comments.html';

window.Answers = Answers;
window.Events = Events;
window.Organizations = Organizations;
window.Projects = Projects;
window.Citoyens = Citoyens;

const pageSession = new ReactiveDict('pageAnswersComments');

Template.answersDetailComments.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.answerId = dataContext.answerId;
      template.orgaCibleId = dataContext.orgaCibleId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.answerId = Router.current().params.answerId;
      template.orgaCibleId = Router.current().params.orgaCibleId;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('answerId', template.answerId);
    pageSession.set('orgaCibleId', template.orgaCibleId);
    Session.setPersistent('orgaCibleId', template.orgaCibleId);

    if (template.orgaCibleId && template.scope && template._id && template.answerId) {
      const handle = Meteor.subscribe('answersDetailComments', template.orgaCibleId, template.scope, template._id, template.answerId);
      if (handle.ready()) {
        template.ready.set(handle.ready());
      }
    }
  });
});

Template.answersDetailComments.helpers({
  scope() {
    if (Template.instance().scope && Template.instance()._id && Template.instance().answerId && Template.instance().ready.get()) {
      const collection = nameToCollection(Template.instance().scope);
      const scope = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
      return scope;
    }
    return undefined;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersDetailComments.events({
  'click .action-comment'(event, instance) {
    const self = this;
    event.preventDefault();
    IonActionSheet.show({
      titleText: i18n.__('Answers Comment'),
      buttons: [
        { text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      ],
      destructiveText: i18n.__('delete'),
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          IonModal.close();
          Router.go('commentsAnswersEditOrga', {
            orgaCibleId: instance.orgaCibleId, _id: instance._id, answerId: instance.answerId, scope: instance.scope, commentId: self._id._str,
          });
        }
        return true;
      },
      destructiveButtonClicked() {
        Meteor.call('deleteComment', self._id._str, function () {
          // Router.go('answersDetail', { _id: instance._id, roomId: instance.roomId, answerId: instance.answerId, scope: instance.scope }, { replaceState: true });
        });
        return true;
      },
    });
  },
  'click .like-comment'(event) {
    Meteor.call('likeScope', this._id._str, 'comments');
    event.preventDefault();
  },
  'click .dislike-comment'(event) {
    Meteor.call('dislikeScope', this._id._str, 'comments');
    event.preventDefault();
  },
});

Template.answersDetailCommentsModal.inheritsHelpersFrom('answersDetailComments');
Template.answersDetailCommentsModal.inheritsEventsFrom('answersDetailComments');
Template.answersDetailCommentsModal.inheritsHooksFrom('answersDetailComments');

Template.commentsAnswersAdd.onRendered(function () {
  const self = this;
  pageSession.set('error', false);
  pageSession.set('queryMention', false);
  pageSession.set('mentions', false);
  self.$('textarea').atwho({
    at: '@',
    limit: 20,
    delay: 600,
    displayTimeout: 300,
    startWithSpace: true,
    displayTpl(item) {
      return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
    },
    // eslint-disable-next-line no-template-curly-in-string
    insertTpl: '${atwho-at}${slug}',
    searchKey: 'name',
  }).on('matched.atwho', function (event, flag, query) {
    // console.log(event, "matched " + flag + " and the result is " + query);
    if (flag === '@' && query) {
      // console.log(pageSession.get('queryMention'));
      if (pageSession.get('queryMention') !== query) {
        pageSession.set('queryMention', query);
        const querySearch = {};
        querySearch.search = query;
        Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
          if (!error) {
            const citoyensArray = _.map(result.citoyens, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
            }));
            const organizationsArray = _.map(result.organizations, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations',
            }));
            const arrayUnions = _.union(citoyensArray, organizationsArray);
            // console.log(citoyensArray);
            self.$('textarea').atwho('load', '@', arrayUnions).atwho('run');
          }
        });
      }
    }
  })
    .on('inserted.atwho', function (event, $li) {
      // console.log(JSON.stringify($li.data('item-data')));

      if ($li.data('item-data')['atwho-at'] === '@') {
        const mentions = {};
        // const arrayMentions = [];
        mentions.name = $li.data('item-data').name;
        mentions.id = $li.data('item-data').id;
        mentions.type = $li.data('item-data').type;
        mentions.avatar = $li.data('item-data').avatar;
        mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
        mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
        if (pageSession.get('mentions')) {
          const arrayMentions = pageSession.get('mentions');
          arrayMentions.push(mentions);
          pageSession.set('mentions', arrayMentions);
        } else {
          pageSession.set('mentions', [mentions]);
        }
      }
    });
});

Template.commentsAnswersAdd.onDestroyed(function () {
  this.$('textarea').atwho('destroy');
});

Template.commentsAnswersAdd.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.answerId = dataContext.answerId;
      template.orgaCibleId = dataContext.orgaCibleId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.answerId = Router.current().params.answerId;
      template.orgaCibleId = Router.current().params.orgaCibleId;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('answerId', template.answerId);
    pageSession.set('orgaCibleId', template.orgaCibleId);
    Session.setPersistent('orgaCibleId', template.orgaCibleId);
  });

  pageSession.set('error', false);
});

Template.commentsAnswersAdd.onRendered(function () {
  pageSession.set('error', false);
});

Template.commentsAnswersAdd.helpers({
  error() {
    return pageSession.get('error');
  },
});

Template.commentsAnswersAdd.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'keyup/focus textarea[name="text"]'(event, instance) {
    const element = instance.find('textarea[name="text"]');
    // element.style.height = '5px';
    element.style.height = `${element.scrollHeight}px`;
  },
});

Template.commentsAnswersEdit.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  pageSession.set('error', false);
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.answerId = dataContext.answerId;
      template.commentId = dataContext.commentId;
      template.orgaCibleId = dataContext.orgaCibleId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.answerId = Router.current().params.answerId;
      template.commentId = Router.current().params.commentId;
      template.orgaCibleId = Router.current().params.orgaCibleId;
    }
    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('answerId', template.answerId);
    pageSession.set('orgaCibleId', template.orgaCibleId);
    Session.setPersistent('orgaCibleId', template.orgaCibleId);
    pageSession.set('commentId', template.commentId);
  });

  self.autorun(function () {
    const handle = Meteor.subscribe('answersDetailComments', template.orgaCibleId, template.scope, template._id, template.answerId);
    if (handle.ready()) {
      self.ready.set(handle.ready());
    }
  });
});

Template.commentsAnswersEdit.onRendered(function () {
  const self = this;
  self.$('textarea').atwho({
    at: '@',
    limit: 20,
    delay: 600,
    displayTimeout: 300,
    startWithSpace: true,
    displayTpl(item) {
      return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
    },
    // eslint-disable-next-line no-template-curly-in-string
    insertTpl: '${atwho-at}${slug}',
    searchKey: 'name',
  }).on('matched.atwho', function (event, flag, query) {
    // console.log(event, "matched " + flag + " and the result is " + query);
    if (flag === '@' && query) {
      // console.log(pageSession.get('queryMention'));
      if (pageSession.get('queryMention') !== query) {
        pageSession.set('queryMention', query);
        const querySearch = {};
        querySearch.search = query;
        Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
          if (!error) {
            const citoyensArray = _.map(result.citoyens, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
            }));
            const organizationsArray = _.map(result.organizations, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations',
            }));
            const arrayUnions = _.union(citoyensArray, organizationsArray);
            // console.log(citoyensArray);
            self.$('textarea').atwho('load', '@', arrayUnions).atwho('run');
          }
        });
      }
    }
  })
    .on('inserted.atwho', function (event, $li) {
      // console.log(JSON.stringify($li.data('item-data')));

      if ($li.data('item-data')['atwho-at'] === '@') {
        const mentions = {};
        // const arrayMentions = [];
        mentions.name = $li.data('item-data').name;
        mentions.id = $li.data('item-data').id;
        mentions.type = $li.data('item-data').type;
        mentions.avatar = $li.data('item-data').avatar;
        mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
        mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
        if (pageSession.get('mentions')) {
          const arrayMentions = pageSession.get('mentions');
          arrayMentions.push(mentions);
          pageSession.set('mentions', arrayMentions);
        } else {
          pageSession.set('mentions', [mentions]);
        }
      }
    });
});

Template.commentsAnswersEdit.onDestroyed(function () {
  this.$('textarea').atwho('destroy');
});

Template.commentsAnswersEdit.helpers({
  comment() {
    const comment = Comments.findOne({ _id: new Mongo.ObjectID(Template.instance().commentId) });
    comment._id = comment._id._str;
    return comment;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.commentsAnswersEdit.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'keyup/focus textarea[name="text"]'(event, instance) {
    const element = instance.find('textarea[name="text"]');
    // element.style.height = '5px';
    element.style.height = `${element.scrollHeight}px`;
  },
});

AutoForm.addHooks(['addAnswersComment', 'editAnswersComment'], {
  before: {
    method(doc) {
      const answerId = pageSession.get('answerId');
      doc.contextType = 'answers';
      doc.contextId = answerId;
      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => doc.text.match(`@${array.value}`) !== null);
        doc.mentions = arrayMentions;
      }
      return doc;
    },
    'method-update'(modifier) {
      const answerId = pageSession.get('answerId');
      modifier.$set.contextType = 'answers';
      modifier.$set.contextId = answerId;
      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => modifier.$set.text.match(`@${array.value}`) !== null);
        modifier.$set.mentions = arrayMentions;
      }
      return modifier;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(':', ' '));
      }
    }
  },
});

AutoForm.addHooks(['addAnswersComment'], {
  after: {
    method(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('comment')}`,
        });
      }
    },
  },
});

AutoForm.addHooks(['editAnswersComment'], {
  after: {
    'method-update'(error) {
      if (!error) {
        Router.go('answersDetailCommentsOrga', {
          orgaCibleId: pageSession.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
});
