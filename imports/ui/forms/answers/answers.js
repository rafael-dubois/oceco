/* eslint-disable no-shadow */
/* eslint-disable no-undef */
/* eslint-disable no-underscore-dangle */
/* eslint-disable meteor/no-session */
/* eslint-disable consistent-return */
/* global AutoForm Session IonActionSheet IonToast _ PhotoViewer */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';
import i18n from 'meteor/universe:i18n';
import { IonPopup } from 'meteor/meteoric:ionic';
import { MeteorCameraUI } from 'meteor/aboire:camera-ui';
import { $ } from 'meteor/jquery';
import { moment } from 'meteor/momentjs:moment';
import { Accounts } from 'meteor/accounts-base';
import { HTTP } from 'meteor/jkuester:http';
import { saveAs } from 'file-saver';

import { Answers } from '../../../api/collection/answers.js';
import { Organizations } from '../../../api/collection/organizations.js';
import { Projects } from '../../../api/collection/projects.js';

import { nameToCollection, removeObjectArray } from '../../../api/helpers.js';

import { pageSession } from '../../../api/client/reactive.js';

import { Citoyens } from '../../../api/collection/citoyens.js';

import './answers.html';

window.Organizations = Organizations;
window.Projects = Projects;

Template.detailAnswers.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template.scopeId = dataContext.scopeId;
      template.answerId = dataContext.answerId;
      template.orgaCibleId = dataContext.orgaCibleId;
      template.keyDepense = dataContext.keyDepense;
      template.depensekey = dataContext.depensekey;
      template.estimatekey = dataContext.estimatekey;
      template.financekey = dataContext.financekey;
      template.financekey = dataContext.financekey;
      template.actionkey = dataContext.actionkey;
      template.taskkey = dataContext.taskkey;
      template.redirect = dataContext.redirect;
    } else {
      template.scope = Router.current().params.scope;
      template.scopeId = Router.current().params._id;
      template.answerId = Router.current().params.answerId;
      template.orgaCibleId = Router.current().params.orgaCibleId;
      template.keyDepense = Router.current().params.key;
      template.depensekey = Router.current().params.depensekey;
      template.estimatekey = Router.current().params.key;
      template.financekey = Router.current().params.key;
      template.actionkey = Router.current().params.actionkey;
      template.taskkey = Router.current().params.key;
      template.redirect = Router.current().params.redirect;
    }

    pageSession.set('scopeId', template.scopeId);
    pageSession.set('scope', template.scope);
    pageSession.set('answerId', template.answerId);
    pageSession.set('orgaCibleId', template.orgaCibleId);
    pageSession.set('keyDepense', template.keyDepense);
    pageSession.set('depensekey', template.depensekey);
    pageSession.set('estimatekey', template.estimatekey);
    pageSession.set('financekey', template.financekey);
    pageSession.set('actionkey', template.actionkey);
    pageSession.set('taskkey', template.taskkey);
    pageSession.set('redirect', template.redirect);
    Session.setPersistent('orgaCibleId', template.orgaCibleId);

    if (template.scope && template.answerId && template.orgaCibleId) {
      const handle = Meteor.subscribe('detailAnswers', template.orgaCibleId, template.scope, template.scopeId, template.answerId);
      if (handle.ready()) {
        if (!template.scopeId) {
          const answerOne = Answers.findOne({ _id: new Mongo.ObjectID(template.answerId) }, { fields: { form: 1 } });
          template.scopeId = answerOne.form;
          pageSession.set('scopeId', template.scopeId);
        }
        this.ready.set(handle.ready());
      }
    }
  }.bind(this));
});

Template.detailAnswers.helpers({
  scope() {
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (scope && scopeId && Template.instance().ready.get()) {
      const collection = nameToCollection(scope);
      return collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
    }
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.detailAnswersModal.inheritsHelpersFrom('detailAnswers');
Template.detailAnswersModal.inheritsEventsFrom('detailAnswers');
Template.detailAnswersModal.inheritsHooksFrom('detailAnswers');

Template.detailViewAnswers.helpers({
  taskContributor(objectContributor) {
    const arrayContributor = objectContributor && Object.keys(objectContributor).length > 0 ? Object.keys(objectContributor) : null;
    if (arrayContributor) {
      const arrayIds = arrayContributor.map((contributor) => new Mongo.ObjectID(contributor));
      return Citoyens.find({ _id: { $in: arrayIds } }, { fields: { name: 1 } });
    }
  },
  taskCheckedUser(userId) {
    if (userId) {
      return Citoyens.findOne({ _id: new Mongo.ObjectID(userId) }, { fields: { name: 1 } }).name;
    }
  },
  userRole(roles) {
    /*
      {{#if userRole (paramsRoles "aapStep2" "canEdit")}}
    true
  {{else}}
    false
  {{/if}}
  */
    const scopeId = pageSession.get('scopeId');
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isRoles('organizations', scopeId, roles);
  },
});

Template.detailViewAnswers.events({
  'click .actions-answer-remove-js'(event) {
    event.preventDefault();
    const answer = this;
    IonPopup.confirm({
      title: i18n.__('delete'),
      template: i18n.__('answers.Delete this answer ?'),
      onOk() {
        Meteor.call('deleteAnswer', { id: answer.answerId }, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
            });
          } else {
            Router.go('answersListOrga', {
              orgaCibleId: answer.orgaCibleId, _id: answer._id, scope: answer.scope,
            });
          }
        });
      },
      onCancel() {
      },
      cancelText: i18n.__('no'),
      okText: i18n.__('yes'),
    });
  },
  'click .actions-answer-js'(event) {
    event.preventDefault();
    const depense = this;
    const sheetObjet = {
      modalClose: true,
      titleText: i18n.__('answers.Answer actions'),
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('answers.Delete this answer ?'),
          onOk() {
            Meteor.call('deleteAnswer', { id: depense.answerId, depenseKey: depense.key }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              } else {
                Router.go('answersListOrga', {
                  orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 'edit') {
          Router.go('answersEdit', {
            orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId,
          });
        }
        if (index === 'add-depense') {
          Router.go('answersDepenseAdd', {
            orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId,
          });
        }
        if (index === 'generate-project') {
          if (!depense.isProject) {
            IonPopup.confirm({
              title: i18n.__('answers.generate project'),
              template: i18n.__('answers.Generate a project ?'),
              onOk() {
                Meteor.call('generateProject', { answerId: depense.answerId }, (error) => {
                  if (error) {
                    IonToast.show({
                      title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                    });
                  }
                });
              },
              onCancel() {
              },
              cancelText: i18n.__('no'),
              okText: i18n.__('yes'),
            });
          }
        }

        return true;
      },
    };
    sheetObjet.buttons = [
      { name: 'edit', text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      { name: 'add-depense', text: `${i18n.__('answers.Add an expense line')} <i class="icon fa fa-euro"></i>` },
      { name: 'generate-project', text: `${i18n.__('answers.Generate project')} <i class="icon fa fa-cog"></i>` },
    ];

    if (depense.isProject) {
      sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'generate-project');
    }
    sheetObjet.buttonClicked = (index) => {
      if (index === 'edit') {
        Router.go('answersEdit', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId,
        });
      }
      if (index === 'add-depense') {
        Router.go('answersDepenseAdd', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId,
        });
      }
      if (index === 'generate-project') {
        if (!depense.isProject) {
          IonPopup.confirm({
            title: i18n.__('answers.generate project'),
            template: i18n.__('answers.Generate a project ?'),
            onOk() {
              Meteor.call('generateProject', { answerId: depense.answerId }, (error) => {
                if (error) {
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                  });
                }
              });
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
        }
      }

      return true;
    };
    IonActionSheet.show(sheetObjet);
  },
  'click .actions-answer-depense-js'(event) {
    event.preventDefault();
    const depense = this;
    const sheetObjet = {
      modalClose: true,
      titleText: i18n.__('answers.Spent actions'),
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('answers.Delete this spent ?'),
          onOk() {
            Meteor.call('deleteDepenseAnswer', { id: depense.answerId, depenseKey: depense.key }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
    };

    sheetObjet.buttons = [
      { name: 'edit', text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      { name: 'add-estimate', text: `${i18n.__('answers.Add estimate')} <i class="icon fa fa-euro"></i>` },
      { name: 'add-funder', text: `${i18n.__('answers.Add a funder')} <i class="icon fa fa-euro"></i>` },
      // { name: 'add-project-manager', text: `${i18n.__('answers.Add project manager')} <i class="icon fa fa-briefcase"></i>` },
      // { name: 'add-payment', text: `${i18n.__('answers.Add payment')} <i class="icon fa fa-euro"></i>` },
    ];

    if (depense.estimatesKeys && depense.estimatesKeys.find((estimate) => estimate.userId === Meteor.userId())) {
      sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'add-estimate');
    } else {
      if (depense.isFinancer) {
        sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'add-estimate');
      }
    }

    if (depense.isWorker) {
      sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'add-project-manager');
      sheetObjet.buttons.push({ name: 'delete-project-manager', text: `${i18n.__('answers.Delete project manager')} <i class="icon fa fa-briefcase"></i>` });
    }

    if (depense.isCompleteFinancer) {
      sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'add-funder');
    }

    /* if (depense.isCompletePayement) {
      sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'add-payment');
    } else {
      if (depense.isFinancer) {
        sheetObjet.buttons.push({ name: 'add-payment', text: `${i18n.__('answers.Add payment')} <i class="icon fa fa-euro"></i>` });
      }
    } */

    if (!depense.isProject) {
      sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'add-payment');
    } else if (depense.isAction) {
      if (!depense.isCompleteTaskCredit) {
        sheetObjet.buttons.push({ name: 'add-task', text: `${i18n.__('answers.Add task')} <i class="icon fa fa-euro"></i>` });
      }
      sheetObjet.buttons.push({ name: 'link-action', text: `${i18n.__('answers.go on the action')} <i class="icon fa fa-link"></i>` });
    }

    sheetObjet.buttonClicked = (index) => {
      if (index === 'edit') {
        Router.go('answersDepenseEdit', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, key: depense.key,
        });
      }
      if (index === 'add-estimate') {
        Router.go('answersDepenseEstimatesAdd', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, depensekey: depense.key,
        });
      }
      if (index === 'add-funder') {
        Router.go('answersDepenseFinanceAdd', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, depensekey: depense.key,
        });
      }
      if (index === 'add-payment') {
        Router.go('answersDepensePayementAdd', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, depensekey: depense.key,
        });
      }
      if (index === 'add-project-manager') {
        Router.go('answersDepenseWorkerAdd', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, depensekey: depense.key,
        });
      }
      if (index === 'link-action') {
        const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(depense.isAction) });
        Router.go('actionsDetailOrga', {
          orgaCibleId: depense.orgaCibleId, _id: actionOne.parentId, scope: actionOne.parentType, roomId: actionOne.idParentRoom, actionId: depense.isAction,
        });
      }
      if (index === 'delete-project-manager') {
        if (depense.isWorker) {
          IonPopup.confirm({
            title: i18n.__('delete'),
            template: i18n.__('answers.Delete the project manager ?'),
            onOk() {
              Meteor.call('deleteDepenseAnswerWorker', { id: depense.answerId, depenseKey: depense.key }, (error) => {
                if (error) {
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                  });
                }
              });
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
        }
      }
      if (index === 'add-task') {
        Router.go('answersDepenseTaskAdd', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, depensekey: depense.key, actionkey: depense.isAction,
        });
      }
      return true;
    };
    IonActionSheet.show(sheetObjet);
  },
  'click .actions-answer-depense-estimate-js'(event) {
    event.preventDefault();
    const depense = this;
    const sheetObjet = {
      modalClose: true,
      titleText: i18n.__('answers.Estimate actions'),
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('answers.Delete this estimate ?'),
          onOk() {
            Meteor.call('deleteDepenseAnswerEstimate', { id: depense.answerId, depenseKey: depense.key, userId: depense.userId }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
    };
    sheetObjet.buttons = [
      { name: 'edit', text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      { name: 'choose-estimate', text: `${i18n.__('answers.choose estimate')} <i class="icon fa fa-euro"></i>` },
    ];
    if (depense.selected) {
      sheetObjet.buttons = removeObjectArray(sheetObjet.buttons, 'name', 'choose-estimate');
    }
    sheetObjet.buttonClicked = (index) => {
      if (index === 'edit') {
        Router.go('answersDepenseEstimatesEdit', {
          orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, depensekey: depense.key, key: depense.userId,
        });
      }
      if (index === 'choose-estimate') {
        IonPopup.confirm({
          title: i18n.__('estimate'),
          template: i18n.__('answers.choose estimate ?'),
          onOk() {
            Meteor.call('chooseDepenseAnswerEstimate', { id: depense.answerId, depenseKey: depense.key, userId: depense.userId }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
      }
      return true;
    };
    IonActionSheet.show(sheetObjet);
  },
  'click .actions-answer-depense-finance-js'(event) {
    event.preventDefault();
    const depense = this;
    IonActionSheet.show({
      modalClose: true,
      titleText: i18n.__('answers.Funding actions'),
      buttons: [
        { text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      ],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('answers.Delete this funding ?'),
          onOk() {
            Meteor.call('deleteDepenseAnswerFinance', { id: depense.answerId, depenseKey: depense.key, financeKey: depense.financeKey }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Router.go('answersDepenseFinanceEdit', {
            orgaCibleId: depense.orgaCibleId, _id: depense._id, scope: depense.scope, answerId: depense.answerId, depensekey: depense.key, key: depense.financeKey,
          });
        }
        return true;
      },
    });
  },
  'click .actions-answer-depense-payement-js'(event) {
    event.preventDefault();
    const depense = this;
    IonActionSheet.show({
      modalClose: true,
      titleText: i18n.__('answers.Payment actions'),
      buttons: [

      ],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('answers.Delete this payment ?'),
          onOk() {
            Meteor.call('deleteDepenseAnswerPayement', { id: depense.answerId, depenseKey: parseInt(depense.key), payementKey: parseInt(depense.payementKey) }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked() {
        return true;
      },
    });
  },
  'click .actions-answer-depense-task-js'(event) {
    event.preventDefault();
    const depense = this;
    // taskCheckedUserId
    const sheetObjet = {
      modalClose: true,
      titleText: i18n.__('answers.Tasks actions'),
      buttons: [
        // { text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      ],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('answers.Delete this task ?'),
          onOk() {
            Meteor.call('deleteDepenseAnswerTask', {
              id: depense.answerId, depenseKey: depense.key, actionKey: depense.actionKey, taskKey: depense.taskKey,
            }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
    };
    sheetObjet.buttons = [];
    if (depense.checked && depense.taskCheckedUserId && !depense.payed) {
      sheetObjet.buttons.push({ name: 'pay', text: `${i18n.__('answers.pay')} <i class="icon fa fa-euro"></i>` });
    }
    sheetObjet.buttonClicked = (index) => {
      if (index === 'pay') {
        if (depense.checked && depense.taskCheckedUserId && !depense.payed) {
          const insert = {};
          insert._id = depense.answerId;
          insert.modifier = {};
          insert.modifier.$set = {};
          insert.modifier.$set.beneficiaryId = depense.taskCheckedUserId;
          insert.modifier.$set.amount = depense.amount;
          insert.modifier.$set.parentType = pageSession.get('scope');
          insert.modifier.$set.parentId = pageSession.get('scopeId');
          insert.modifier.$set.answerId = depense.answerId;
          insert.modifier.$set.keyDepense = depense.key.toString();
          insert.modifier.$set.actionKey = depense.actionKey;
          insert.modifier.$set.taskKey = depense.taskKey;
          IonPopup.confirm({
            title: i18n.__('pay'),
            template: i18n.__('answers.pay ?'),
            onOk() {
              if (depense.contributors) {
                const keysContributors = Object.keys(depense.contributors);
                const CountContributors = keysContributors && keysContributors.length > 0 ? keysContributors.length : 0;
                Object.keys(depense.contributors).forEach((key) => {
                  insert.modifier.$set.beneficiaryId = key;

                  const credits = Math.round((parseInt(depense.amount) / CountContributors) * 100) / 100;
                  if (depense.taskCheckedUserId === key) {
                    const reste = parseInt(depense.amount) - (credits * CountContributors);
                    insert.modifier.$set.amount = credits + reste;
                  } else {
                    insert.modifier.$set.amount = credits;
                  }

                  Meteor.call('insertDepenseAnswerPayement', { ...insert }, (error) => {
                    if (error) {
                      IonToast.show({
                        title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                      });
                    }
                  });
                });
              } else {
                Meteor.call('insertDepenseAnswerPayement', { ...insert }, (error) => {
                  if (error) {
                    IonToast.show({
                      title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                    });
                  }
                });
              }
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
        }
      }
      return true;
    };
    IonActionSheet.show(sheetObjet);
  },
  'click .photo-link-scope'(event, instance) {
    event.preventDefault();
    const self = this;
    const scope = 'answers';
    if (Meteor.isDesktop) {
      instance.$('#file-upload-answer').trigger('click');
    } else if (Meteor.isCordova) {
      const options = {
        width: 640,
        height: 480,
        quality: 75,
      };
      MeteorCameraUI.getPicture(options, function (error, data) {
        if (!error) {
          const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
          Meteor.call('photoScope', scope, data, str, self._id._str, function (errorPhoto) {
            if (!errorPhoto) {
              // console.log(result);
            } else {
              // console.log('error', error);
            }
          });
        }
      });
    } else {
      instance.$('#file-upload-answer').trigger('click');
    }
  },
  'change #file-upload-answer'(event, instance) {
    event.preventDefault();
    const self = this;
    const scope = 'answers';

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      _.each(instance.find('#file-upload-answer').files, function (file) {
        if (file.size > 1) {
          const reader = new FileReader();
          reader.onload = function () {
            const str = file.name;
            const dataURI = reader.result;
            Meteor.call('photoScope', scope, dataURI, str, self._id._str, function (error) {
              if (!error) {
                // console.log(result);
              } else {
                // console.log('error',error);
              }
            });
          };
          reader.readAsDataURL(file);
        }
      });
    }
  },
});

Template.answersAdd.onCreated(function () {
  const template = Template.instance();
  template.ready = new ReactiveVar();
  pageSession.set('error', false);

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    const handle = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
    if (handle.ready()) {
      template.ready.set(handle.ready());
    }
  });
});

Template.answersFields.onDestroyed(function () {
  const self = this;
  self.$("input[name='tagsText']").atwho('destroy');
});

Template.answersFields.helpers({
  isCordova() {
    return Meteor.isCordova;
  },
});

Template.answersFields.onRendered(function () {
  const self = this;
  const template = Template.instance();
  template.find('input[name=name]').focus();

  if (Meteor.isCordova) {
    // mobile predictive desactived
    const tagText = template.find("input[name='tagsText']");
    if (tagText && tagText.value !== '') {
      tagText.type = 'text';
    }
    self.$("input[name='tagsText']").on('focus', function () {
      this.type = 'text';
      if (!this.value) {
        this.value = '#';
      }
    });
  } else {
    self.$("input[name='tagsText']").on('focus', function () {
      if (!this.value) {
        this.value = '#';
      }
    });
  }

  const formOne = Forms.findOne({ _id: new Mongo.ObjectID(pageSession.get('scopeId')) });
  pageSession.set('queryTag', false);
  pageSession.set('tags', false);
  self.$("input[name='tagsText']").atwho({
    at: '#',
    data: formOne && formOne.tags ? formOne.tags : [],
    limit: formOne && formOne.tags && formOne.tags.length > 0 ? formOne.tags.length : 0,
  }).on('inserted.atwho', function (event, $li) {
    // console.log(JSON.stringify($li.data('item-data')));
    if ($li.data('item-data')['atwho-at'] === '#') {
      const tag = $li.data('item-data').name;
      if (pageSession.get('tags')) {
        const arrayTags = pageSession.get('tags');
        arrayTags.push(tag);
        pageSession.set('tags', arrayTags);
      } else {
        pageSession.set('tags', [tag]);
      }
    }
  });
});

Template.answersAdd.helpers({
  answer() {
    const answerEdit = {};

    return answerEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersAdd.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
});

Template.answersEdit.inheritsHooksFrom('detailAnswers');

Template.answersEdit.helpers({
  answer() {
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const answerEdit = {};
    if (answer) {
      answerEdit._id = answer._id._str;
      if (answer.answers && answer.answers.aapStep1) {
        if (answer.answers.aapStep1.titre) {
          answerEdit.name = answer.answers.aapStep1.titre;
        }
        if (answer.answers.aapStep1.description) {
          answerEdit.description = answer.answers.aapStep1.description;
        }
        if (answer.answers.aapStep1.tags && answer.answers.aapStep1.tags.length > 0) {
          answerEdit.tagsText = answer.answers.aapStep1.tags.map((tag) => `#${tag}`).join(' ');
        }
        if (answer.answers.aapStep1.urgency) {
          const urgentArray = answer.answers.aapStep1.urgency.filter((m) => m === 'Urgent');
          if (urgentArray && urgentArray.length > 0) {
            answerEdit.urgent = true;
          }
        }
      }
    }
    return answerEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addAnswer', 'editAnswer'], {
  after: {
    method(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_added')}`,
        });
        Router.go('answersList', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope') }, { replaceState: true });
      }
    },
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_updated')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    method(doc) {
      doc.parentType = pageSession.get('scope');
      doc.parentId = pageSession.get('scopeId');
      return doc;
    },
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      return modifier;
    },
  },
});

Template.answersDepenseAdd.inheritsHooksFrom('detailAnswers');

Template.answersDepenseAdd.helpers({
  depense() {
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const depenseEdit = {};
    depenseEdit._id = answer._id._str;
    return depenseEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersDepenseEdit.inheritsHooksFrom('detailAnswers');

Template.answersDepenseEdit.helpers({
  depense() {
    const answerId = pageSession.get('answerId');
    const key = pageSession.get('keyDepense');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const depenseEdit = {};
    depenseEdit._id = answer._id._str;
    // key depense
    if (answer && answer.answers && answer.answers.aapStep1 && answer.answers.aapStep1.depense && key && answer.answers.aapStep1.depense[key]) {
      if (answer.answers.aapStep1.depense[key].group) {
        // depenseEdit.group = answer.answers.aapStep1.depense[key].group.split(',');
        depenseEdit.group = answer.answers.aapStep1.depense[key].group;
      }
      if (answer.answers.aapStep1.depense[key].nature) {
        // depenseEdit.nature = answer.answers.aapStep1.depense[key].nature.split(',');
        depenseEdit.nature = answer.answers.aapStep1.depense[key].nature;
      }
      if (answer.answers.aapStep1.depense[key].poste) {
        depenseEdit.poste = answer.answers.aapStep1.depense[key].poste;
      }
      if (answer.answers.aapStep1.depense[key].price) {
        depenseEdit.price = answer.answers.aapStep1.depense[key].price;
      }
    }
    return depenseEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_added')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      return modifier;
    },
  },
});

AutoForm.addHooks(['editDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_updated')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('keyDepense');
      return modifier;
    },
  },
});

Template.answersDepenseFields.helpers({
  optionsGroup() {
    const formsOne = Forms.findOne({ _id: new Mongo.ObjectID(pageSession.get('scopeId')) });
    if (formsOne && formsOne.budgetdepenseGroup()) {
      return formsOne.budgetdepenseGroup().map(function (c) {
        return { label: c, value: c };
      });
    }
  },
  group() {
    return pageSession.get('group') || AutoForm.getFieldValue('group');
  },
  optionsNature() {
    const formsOne = Forms.findOne({ _id: new Mongo.ObjectID(pageSession.get('scopeId')) });
    if (formsOne && formsOne.budgetdepenseNature()) {
      return formsOne.budgetdepenseNature().map(function (c) {
        return { label: c, value: c };
      });
    }
  },
  nature() {
    return pageSession.get('nature') || AutoForm.getFieldValue('nature');
  },
  parentId() {
    return pageSession.get('scopeId');
  },
  answerId() {
    return pageSession.get('answerId');
  }
});

Template.answersDepenseEstimatesAdd.inheritsHooksFrom('detailAnswers');

Template.answersDepenseEstimatesAdd.helpers({
  estimate() {
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const estimateEdit = {};
    estimateEdit._id = answer._id._str;
    return estimateEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersDepenseEstimatesEdit.inheritsHooksFrom('detailAnswers');

Template.answersDepenseEstimatesEdit.helpers({
  estimate() {
    const answerId = pageSession.get('answerId');
    const depensekey = pageSession.get('depensekey');
    const key = pageSession.get('estimatekey');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const estimateEdit = {};
    estimateEdit._id = answer._id._str;
    // key depense
    if (answer && answer.answers && answer.answers.aapStep1 && answer.answers.aapStep1.depense && depensekey && key && answer.answers.aapStep1.depense[depensekey] && answer.answers.aapStep1.depense[depensekey].estimates && answer.answers.aapStep1.depense[depensekey].estimates[key]) {
      if (answer.answers.aapStep1.depense[depensekey].estimates[key].days) {
        estimateEdit.days = answer.answers.aapStep1.depense[depensekey].estimates[key].days;
      }
      if (answer.answers.aapStep1.depense[depensekey].estimates[key].price) {
        estimateEdit.price = answer.answers.aapStep1.depense[depensekey].estimates[key].price;
      }
    }
    return estimateEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addEstimatesDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_estimate_added')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      return modifier;
    },
  },
});

AutoForm.addHooks(['editEstimatesDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_estimate_updated')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      return modifier;
    },
  },
});

Template.answersDepenseFinanceAdd.inheritsHooksFrom('detailAnswers');

Template.answersDepenseFinanceAdd.helpers({
  finance() {
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(pageSession.get('answerId')) });
    const financeEdit = {};
    financeEdit._id = answer._id._str;
    financeEdit.communaute = true;
    return financeEdit;
  },
  depenseArray() {
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(pageSession.get('answerId')) });
    const depenseArrayOne = answer.depenseArray(pageSession.get('depensekey'));
    return depenseArrayOne && depenseArrayOne[0] ? depenseArrayOne[0] : {};
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersDepenseFinanceFields.helpers({
  optionsFinanceur() {
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    if (orgaOne && orgaOne.listRoleType('Financeur')) {
      return orgaOne.listRoleType('Financeur').map(function (c) {
        return { label: c.name, value: c._id._str };
      });
    }
  },
  financeurId() {
    return pageSession.get('financeurId') || AutoForm.getFieldValue('financeurId');
  },
});

Template.answersDepenseFinanceEdit.inheritsHooksFrom('detailAnswers');

Template.answersDepenseFinanceFields.events({
  'keyup input[name="amount"]'(event) {
    const amount = parseInt(event.currentTarget.value);
    const max = parseInt(event.currentTarget.max);
    if (amount > max) {
      event.currentTarget.value = max;
    }
  },
});

Template.answersDepenseFinanceEdit.helpers({
  finance() {
    const answerId = pageSession.get('answerId');
    const depensekey = pageSession.get('depensekey');
    const key = pageSession.get('financekey');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const financeEdit = {};
    financeEdit._id = answer._id._str;
    // key depense
    if (answer && answer.answers && answer.answers.aapStep1 && answer.answers.aapStep1.depense && depensekey && key && answer.answers.aapStep1.depense[depensekey] && answer.answers.aapStep1.depense[depensekey].financer && answer.answers.aapStep1.depense[depensekey].financer[key]) {
      if (answer.answers.aapStep1.depense[depensekey].financer[key].line) {
        financeEdit.line = answer.answers.aapStep1.depense[depensekey].financer[key].line;
      }
      if (answer.answers.aapStep1.depense[depensekey].financer[key].amount) {
        financeEdit.amount = answer.answers.aapStep1.depense[depensekey].financer[key].amount;
      }
      if (answer.answers.aapStep1.depense[depensekey].financer[key].name) {
        financeEdit.name = answer.answers.aapStep1.depense[depensekey].financer[key].name;
      }
      if (answer.answers.aapStep1.depense[depensekey].financer[key].email) {
        financeEdit.email = answer.answers.aapStep1.depense[depensekey].financer[key].email;
      }
      if (answer.answers.aapStep1.depense[depensekey].financer[key].id) {
        financeEdit.financeurId = answer.answers.aapStep1.depense[depensekey].financer[key].id;
        financeEdit.communaute = true;
      } else {
        financeEdit.communaute = false;
      }
    }
    return financeEdit;
  },
  depenseArray() {
    const answerId = pageSession.get('answerId');
    const depensekey = pageSession.get('depensekey');
    const financekey = pageSession.get('financekey');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const { amount } = answer.answers.aapStep1.depense[depensekey].financer[financekey];
    // ! attention on modifie aprés anwser et les key ne sont plus bonne
    const depenseArrayOne = answer.depenseArray(depensekey);
    const { price, totalAmount, isCompleteFinancer } = depenseArrayOne[0];
    depenseArrayOne[0].totalRemaining = isCompleteFinancer === true ? price - (totalAmount - parseInt(amount)) : price - (totalAmount - parseInt(amount));
    return depenseArrayOne && depenseArrayOne[0] ? depenseArrayOne[0] : {};
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addFinanceDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_finance_added')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      return modifier;
    },
  },
});

AutoForm.addHooks(['editFinanceDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_finance_updated')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      modifier.$set.keyFinance = pageSession.get('financekey');
      return modifier;
    },
  },
});

Template.answersDepenseWorkerAdd.inheritsHooksFrom('detailAnswers');

Template.answersDepenseWorkerAdd.helpers({
  worker() {
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const workerEdit = {};
    workerEdit._id = answer._id._str;
    return workerEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersDepenseWorkerFields.helpers({
  optionsWorker() {
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    if (orgaOne && orgaOne.listRoleType('maitreOuvrage')) {
      return orgaOne.listRoleType('maitreOuvrage').map(function (c) {
        return { label: c.name, value: c._id._str };
      });
    }
  },
  workerId() {
    return pageSession.get('workerId') || AutoForm.getFieldValue('workerId');
  },
});

Template.answersDepenseWorkerEdit.inheritsHooksFrom('detailAnswers');

Template.answersDepenseWorkerEdit.helpers({
  worker() {
    const answerId = pageSession.get('answerId');
    const depensekey = pageSession.get('depensekey');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const workerEdit = {};
    workerEdit._id = answer._id._str;
    // key depense
    if (answer && answer.answers && answer.answers.aapStep1 && answer.answers.aapStep1.depense && depensekey && answer.answers.aapStep1.depense[depensekey] && answer.answers.aapStep1.depense[depensekey].worker) {
      if (answer.answers.aapStep1.depense[depensekey].worker.workType) {
        workerEdit.workType = answer.answers.aapStep1.depense[depensekey].worker.workType;
      }
      if (answer.answers.aapStep1.depense[depensekey].worker.id) {
        workerEdit.workerId = answer.answers.aapStep1.depense[depensekey].worker.id;
      }
    }
    return workerEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addWorkerDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_worker_added')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      return modifier;
    },
  },
});

AutoForm.addHooks(['editWorkerDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_worker_updated')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      return modifier;
    },
  },
});

Template.answersDepensePayementAdd.inheritsHooksFrom('detailAnswers');

Template.answersDepensePayementAdd.helpers({
  payement() {
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const payementEdit = {};
    payementEdit._id = answer._id._str;
    return payementEdit;
  },
  depenseArray() {
    const answerId = pageSession.get('answerId');
    const depensekey = pageSession.get('depensekey');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const depenseArrayOne = answer.depenseArray(depensekey);
    return depenseArrayOne && depenseArrayOne[0] ? depenseArrayOne[0] : {};
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersDepensePayementFields.helpers({
  optionsFinanceur() {
    // todo: ajouter la liste venant de depense.financer
    const depensekey = pageSession.get('depensekey');
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const depenseArrayOne = answer.depenseArray(depensekey);
    if (depenseArrayOne && depenseArrayOne[0] && depenseArrayOne[0].financer) {
      const financerArray = depenseArrayOne[0].financer.map(function (c) {
        return { label: c.name, value: (c.id || c.name) };
      });
      const uniqueObjArray = [...new Map(financerArray.map((item) => [item.value, item])).values()];
      return uniqueObjArray;
    }
  },
  optionsBeneficiary() {
    const answerId = pageSession.get('answerId');
    const answersOne = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    if (answersOne && answersOne.project) {
      return answersOne.projectOne().listContributors().map(function (c) {
        return { label: c.name, value: c._id._str };
      });
    }
  },
  financerArray() {
    const depensekey = pageSession.get('depensekey');
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const depenseArrayOne = answer.depenseArray(depensekey);
    const paiementObj = {};
    if (depenseArrayOne && depenseArrayOne[0] && depenseArrayOne[0].financer) {
      const arrayFinanceOne = depenseArrayOne[0].financer.map((c) => c.amount);
      paiementObj.totalFinancer = arrayFinanceOne && arrayFinanceOne.length > 0 ? arrayFinanceOne.reduce((previousValue, currentValue) => previousValue + currentValue) : 0;
    }
    if (depenseArrayOne && depenseArrayOne[0] && depenseArrayOne[0].payement) {
      const arrayPayementOne = depenseArrayOne[0].payement.map((c) => c.amount);
      paiementObj.totalPayement = arrayPayementOne && arrayPayementOne.length > 0 ? arrayPayementOne.reduce((previousValue, currentValue) => previousValue + currentValue) : 0;
    }
    if (!paiementObj.totalPayement) {
      paiementObj.totalPayement = 0;
    }
    if (!paiementObj.totalFinancer) {
      paiementObj.totalFinancer = 0;
    }

    paiementObj.totalReste = paiementObj.totalFinancer - paiementObj.totalPayement;
    paiementObj.isCompletePayement = paiementObj.totalReste === 0;
    return paiementObj;
  },
  financerArrayOld(financeurId) {
    const depensekey = pageSession.get('depensekey');
    const answerId = pageSession.get('answerId');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const depenseArrayOne = answer.depenseArray(depensekey);
    const paiementObj = {};
    if (depenseArrayOne && depenseArrayOne[0] && depenseArrayOne[0].financer) {
      const arrayFinanceOne = depenseArrayOne[0].financer.filter((f) => f.id === financeurId || f.name === financeurId).map((c) => c.amount);
      paiementObj.totalFinancer = arrayFinanceOne && arrayFinanceOne.length > 0 ? arrayFinanceOne.reduce((previousValue, currentValue) => previousValue + currentValue) : 0;
    }
    if (depenseArrayOne && depenseArrayOne[0] && depenseArrayOne[0].payement) {
      const arrayPayementOne = depenseArrayOne[0].payement.filter((f) => f.financeur.id === financeurId || f.financeur.name === financeurId).map((c) => c.amount);
      paiementObj.totalPayement = arrayPayementOne && arrayPayementOne.length > 0 ? arrayPayementOne.reduce((previousValue, currentValue) => previousValue + currentValue) : 0;
    }
    if (!paiementObj.totalPayement) {
      paiementObj.totalPayement = 0;
    }
    if (!paiementObj.totalFinancer) {
      paiementObj.totalFinancer = 0;
    }

    paiementObj.totalReste = paiementObj.totalFinancer - paiementObj.totalPayement;
    paiementObj.isCompletePayement = paiementObj.totalReste === 0;
    return paiementObj;
  },
  financeurId() {
    return pageSession.get('financeurId') || AutoForm.getFieldValue('financeurId');
  },
  beneficiaryId() {
    return pageSession.get('beneficiaryId') || AutoForm.getFieldValue('beneficiaryId');
  },
});

Template.answersDepensePayementFields.events({
  'keyup input[name="amount"]'(event) {
    const amount = parseInt(event.currentTarget.value);
    const max = parseInt(event.currentTarget.max);
    if (amount > max) {
      event.currentTarget.value = max;
    }
  },
  'change select[name="financeurId"]'(event, instance) {
    pageSession.set('financeurId', instance.$(event.currentTarget).val());
  },
});

AutoForm.addHooks(['addPayementDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_payement_added')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      return modifier;
    },
  },
});

//
Template.answersDepenseTaskAdd.inheritsHooksFrom('detailAnswers');

Template.answersDepenseTaskAdd.helpers({
  task() {
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(pageSession.get('answerId')) });
    const taskEdit = {};
    taskEdit._id = answer._id._str;
    return taskEdit;
  },
  depenseArray() {
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(pageSession.get('answerId')) });
    // trouvé depense key par actionid
    if (!pageSession.get('depensekey')) {
      const depenseKeys = Object.keys(answer.answers.aapStep1.depense);
      const depense = Object.values(answer.answers.aapStep1.depense);
      const depenseArray = depense.filter((f, depenseKey) => {
        f.depenseKey = depenseKeys[depenseKey];
        return f.actionid === pageSession.get('actionkey');
      });

      if (depenseArray && depenseArray.length > 0) {
        const depenseArrayOne = answer.depenseArray(depenseArray[0].depenseKey);
        return depenseArrayOne && depenseArrayOne[0] ? depenseArrayOne[0] : {};
      }
    }

    const depenseArrayOne = answer.depenseArray(pageSession.get('depensekey'));
    return depenseArrayOne && depenseArrayOne[0] ? depenseArrayOne[0] : {};
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.answersDepenseTaskFields.helpers({
  optionsBeneficiary() {
    const answerId = pageSession.get('answerId');
    const answersOne = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    if (answersOne && answersOne.project) {
      return answersOne.projectOne().listContributors().map(function (c) {
        return { label: c.name, value: c._id._str };
      });
    }
  },
});

Template.answersDepenseTaskEdit.inheritsHooksFrom('detailAnswers');

Template.answersDepenseTaskFields.onRendered(function () {
  const self = this;
  const template = Template.instance();

  pageSession.set('queryMention', false);
  pageSession.set('mentions', false);

  if (Meteor.isCordova) {
    // mobile predictive desactived
    const tagText = template.find("input[name='assignText']");
    if (tagText && tagText.value !== '') {
      tagText.type = 'text';
    }
    self.$("input[name='assignText']").on('focus', function () {
      this.type = 'text';
      if (!this.value) {
        this.value = '@';
      }
    });
  } else {
    self.$("input[name='assignText']").on('focus', function () {
      if (!this.value) {
        this.value = '@';
      }
    });
  }

  // #tags
  if (Router.current().params.scope !== 'citoyens') {
    self.$("input[name='assignText']").atwho({
      at: '@',
      limit: 100,
      delay: 600,
      displayTimeout: 300,
      // startWithSpace: true,
      displayTpl(item) {
        return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
      },
      // eslint-disable-next-line no-template-curly-in-string
      insertTpl: '${atwho-at}${slug}',
      searchKey: 'name',
    }).on('matched.atwho', function (event, flag, query) {
      // console.log(event, "matched " + flag + " and the result is " + query);
      if (flag === '@' && query) {
        // console.log(pageSession.get('queryMention'));
        if (pageSession.get('queryMention') !== query) {
          pageSession.set('queryMention', query);
          const querySearch = {};
          querySearch.search = query;
          querySearch.orgaCibleId = Session.get('orgaCibleId');
          Meteor.call('searchListAssignActionsautocomplete', querySearch, function (error, result) {
            if (!error) {
              // console.log(result);
              const citoyensArray = _.map(result, (array) => (array.profilThumbImageUrl ? {
                id: array._id._str, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
              } : {
                id: array._id._str, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
              }));
              if (citoyensArray && citoyensArray.length > 0) {
                // const uniqueArray = pageSession.get('mentions') && pageSession.get('mentions').length > 1 ? citoyensArray.filter((obj) => !pageSession.get('mentions').some((obj2) => obj.id === obj2.id)) : citoyensArray;
                self.$("input[name='assignText']").atwho('load', '@', citoyensArray).atwho('run');
              }
            }
          });
        }
      }
    })
      .on('inserted.atwho', function (event, $li) {
        if ($li.data('item-data')['atwho-at'] === '@') {
          const mentions = {};
          // const arrayMentions = [];
          mentions.name = $li.data('item-data').name;
          mentions.id = $li.data('item-data').id;
          mentions.avatar = $li.data('item-data').avatar;
          mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
          mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
          if (pageSession.get('mentions')) {
            const arrayMentions = pageSession.get('mentions');
            arrayMentions.push(mentions);
            pageSession.set('mentions', arrayMentions);
          } else {
            pageSession.set('mentions', [mentions]);
          }
        }
      });
  }
});

Template.answersDepenseTaskFields.events({
  'keyup input[name="amount"]'(event) {
    const amount = parseInt(event.currentTarget.value);
    const max = parseInt(event.currentTarget.max);
    if (amount > max) {
      event.currentTarget.value = max;
    }
  },
});

Template.answersDepenseTaskFields.onDestroyed(function () {
  const self = this;
  self.$("input[name='assignText']").atwho('destroy');
  // pageSession.set('milestoneId', null);
});

Template.answersDepenseTaskEdit.helpers({
  task() {
    const answerId = pageSession.get('answerId');
    const depensekey = pageSession.get('depensekey');
    const taskkey = pageSession.get('taskkey');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    const taskEdit = {};
    const depenseArrayOne = answer.depenseArray(depensekey);
    const { tasks } = depenseArrayOne[0];
    const taskOneArray = tasks.filter((task) => task.taskId === taskkey);

    taskEdit._id = answer._id._str;
    if (taskOneArray && taskOneArray[0]) {
      if (taskOneArray[0].credits) {
        taskEdit.amount = parseInt(taskOneArray[0].credits);
      }
      if (taskOneArray[0].endDate) {
        taskEdit.endDate = moment(taskOneArray[0].endDate).format('YYYY-MM-DD');
      }
      if (taskOneArray[0].task) {
        taskEdit.task = taskOneArray[0].task;
      }
    }
    return taskEdit;
  },
  depenseArray() {
    const answerId = pageSession.get('answerId');
    const depensekey = pageSession.get('depensekey');
    const actionkey = pageSession.get('actionkey');
    const taskkey = pageSession.get('taskkey');
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(answerId) });
    // ! attention on modifie aprés anwser et les key ne sont plus bonne
    const depenseArrayOne = answer.depenseArray(depensekey);
    const { price, totalCreditTask, isCompleteTaskCredit, tasks } = depenseArrayOne[0];
    const taskOneArray = tasks.filter((task) => task.taskId === taskkey);
    depenseArrayOne[0].totalRemainingCreditTask = isCompleteTaskCredit === true ? price - (totalCreditTask - parseInt(taskOneArray[0].credits)) : price - (totalCreditTask - parseInt(taskOneArray[0].credits));
    return depenseArrayOne && depenseArrayOne[0] ? depenseArrayOne[0] : {};
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addTaskDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_finance_added')}`,
        });
        if (pageSession.get('redirect')) {
          // organizations/:orgaCibleId/:scope/rooms/:_id/room/:roomId/action/:actionId'
          /* Router.go('actionsDetailOrga', {
            orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
          }, { replaceState: true }); */
          history.back();
        } else {
          Router.go('answersDetailOrga', {
            orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
          }, { replaceState: true });
        }
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      if (pageSession.get('mentions') && modifier.$set.assignText) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => modifier.$set.assignText.match(`@${array.value}`) !== null);
        if (arrayMentions && arrayMentions.length > 0) {
          modifier.$set.assign = arrayMentions.map((member) => member.id);
        }
        delete modifier.$set.assignText;
      } else {
        delete modifier.$set.assignText;
      }
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      modifier.$set.keyAction = pageSession.get('actionkey');
      return modifier;
    },
  },
});

AutoForm.addHooks(['editTaskDepenseAnswer'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_finance_updated')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      modifier.$set.keyDepense = pageSession.get('depensekey');
      modifier.$set.keyAction = pageSession.get('actionkey');
      modifier.$set.keyTask = pageSession.get('taskkey');
      return modifier;
    },
  },
});

Template.answersEvaluation.helpers({
  evaluation() {
    const self = this;
    const answer = Answers.findOne({ _id: new Mongo.ObjectID(pageSession.get('answerId')) });
    const evaluationEdit = {};
    evaluationEdit._id = answer._id._str;
    const evaluationUSer = answer && answer.answers && answer.answers.aapStep2 && answer.answers.aapStep2.evaluation && answer.answers.aapStep2.evaluation[Meteor.userId()] ? answer.answers.aapStep2.evaluation[Meteor.userId()] : {};
    // evaluation
    // id user evaluation
    evaluationEdit[self.labelNet] = 0;
    if (evaluationUSer) {
      Object.values(evaluationUSer).forEach((evaluation) => {
        const labelNet = evaluation.label.replace(/\s/g, '');
        if (labelNet === self.labelNet) {
          evaluationEdit[labelNet] = parseFloat(evaluation.note);
        }
      });
    }
    return evaluationEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addEvaluationAnswer1', 'addEvaluationAnswer2', 'addEvaluationAnswer3'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          title: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, template: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_evaluation_updated')}`,
        });
        Router.go('answersDetailOrga', {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), formId: pageSession.get('formId'), answerId: pageSession.get('answerId'),
        }, { replaceState: true });
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      modifier.$set.answerId = pageSession.get('answerId');
      return modifier;
    },
  },
});

Template.formsEdit.inheritsHooksFrom('detailAnswers');

Template.formsEdit.helpers({
  form() {
    const form = Forms.findOne({ _id: new Mongo.ObjectID(pageSession.get('scopeId')) });
    const formEdit = {};
    formEdit._id = form._id._str;
    if (form.params && form.params.budgetdepense && form.params.budgetdepense.group && form.params.budgetdepense.group.length > 0) {
      formEdit.group = form.params.budgetdepense.group;
    }
    if (form.params && form.params.budgetdepense && form.params.budgetdepense.nature && form.params.budgetdepense.nature.length > 0) {
      formEdit.nature = form.params.budgetdepense.nature;
    }
    return formEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['editForm'], {
  after: {
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('answers.answer_depense_updated')}`,
        });
        history.back();
      }
    },
  },
  before: {
    'method-update'(modifier) {
      modifier.$set = modifier.$set ? modifier.$set : {};
      modifier.$set.parentType = pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('scopeId');
      return modifier;
    },
  },
});

AutoForm.addHooks(['addAnswer', 'editAnswer', 'addDepenseAnswer', 'editDepenseAnswer', 'addEstimatesDepenseAnswer', 'editEstimatesDepenseAnswer', 'addFinanceDepenseAnswer', 'editFinanceDepenseAnswer', 'addWorkerDepenseAnswer', 'editWorkerDepenseAnswer', 'addPayementDepenseAnswer', 'addTaskDepenseAnswer', 'editTaskDepenseAnswer', 'addEvaluationAnswer1', 'addEvaluationAnswer2', 'addEvaluationAnswer3', 'editForm'], {
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(': ', ''));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      }
    }
  },
});

Template.scopeFileUploadAnswers.onRendered(function () {
  const self = this;
  const template = Template.instance();

  pageSession.set('drop', false);
  pageSession.set('fileCount', 0);
  pageSession.set('fileArray', []);

  /* this.autorun(function () {
    if (pageSession.get('fileName')) {
      const arrayFiles = pageSession.get('fileArray');
      const newArr = arrayFiles.filter(name => name !== pageSession.get('fileName'));
      pageSession.set('fileArray', newArr);
    }
  }); */

  window.addEventListener('paste', (e) => {
    // file-upload-action
    // console.log(e.clipboardData.files);
    // console.log('paste', e.clipboardData);
    if (e.clipboardData.files) {
      if (e.clipboardData.files.length === 1) {
        if (e.clipboardData.files[0].type === 'image/png' || e.clipboardData.files[0].type === 'image/jpg' || e.clipboardData.files[0].type === 'image/jpge' || e.clipboardData.files[0].type === 'image/jpeg') {
          const fileInput = template.find('#file-upload-answer');
          if (fileInput) {
            fileInput.files = e.clipboardData.files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload-answer').trigger('change');
          }
        }
      }
    }
  });

  const dropZone = document.body;
  if (dropZone) {
    const hoverClassName = 'drop';

    dropZone.addEventListener('dragenter', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragover', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragleave', function (e) {
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
    });

    dropZone.addEventListener('drop', (e) => {
      // file-upload-action
      // console.log(e.clipboardData.files);
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
      // console.log('drop', e.clipboardData);
      // console.log('drop', e.dataTransfer);
      const { files } = e.dataTransfer;
      if (files) {
        /* pageSession.set('fileCount', files.length);
        const arrayFile = Object.keys(files).map((k) => {
          return files.item(k).name;
        });
        pageSession.set('fileArray', arrayFile); */

        if (files.length === 1) {
          if (files[0].type === 'image/png' || files[0].type === 'image/jpg' || files[0].type === 'image/jpge' || files[0].type === 'image/jpeg' || files[0].type === 'image/jpeg') {
            const fileInput = template.find('#file-upload-answer');
            // console.log(fileInput);
            fileInput.files = files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload-answer').trigger('change');
          }
        }
      }
    });
  }
});
