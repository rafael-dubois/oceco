/* eslint-disable no-shadow */
/* global IonPopup moment */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';

import { Pomodoro } from '../../../../api/client/podomoro.js';
import { Actions } from '../../../../api/collection/actions.js';
import { Citoyens } from '../../../../api/collection/citoyens.js';

/*
pomodoroStatus
pomodoroTimer
pomodoroCount
lastPlayedTask
playerStatus
*/

Template.podomoroPlayer.onCreated(function () {
  this.ready = new ReactiveVar();
  this.autorun(function () {
    const handle = Meteor.subscribe('actionPodomoro');
    if (handle.ready()) {
      this.ready.set(handle.ready());
    }
  }.bind(this));

  Pomodoro.initialize();
  // console.log('initialize credted');
  Pomodoro.requestNotificationPermission();
  Pomodoro.podomoroSessionSetDefault('pomodoroCount', 0);

  this.autorun(function () {
    if (Pomodoro.podomoroSessionGet('pomodoroInProgress')) {
      // Pomodoro.pause();
      // console.log('initialize play pomodoroInProgress');
    }
  });

  /* work: this.podomoroSessionGet('pomodoroTimeWork'),
    short_rest: this.podomoroSessionGet('pomodoroTimeShortRest'),
      long_rest: this.podomoroSessionGet('pomodoroTimeLongRest'), */

  this.autorun(function () {
    if (this.ready.get()) {
      // console.log(Pomodoro.podomoroSessionGet('pomodoroStatus'));
      // not state : yes
      if (!Pomodoro.podomoroSessionGet('pomodoroStatus') && !Pomodoro.podomoroSessionGet('lastPlayedTask') && (!Pomodoro.podomoroSessionGet('playerStatus') || Pomodoro.podomoroSessionGet('playerStatus') === 'pause')) {
        const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) });
        if (citoyenOne && citoyenOne.oceco && citoyenOne.oceco.podomoro && citoyenOne.oceco.podomoro.working && citoyenOne.oceco.podomoro.working.statusPodomoro !== 'pending') {
          // console.log(citoyenOne.oceco.podomoro.working.statusPodomoro);
          if (citoyenOne.oceco.podomoro.working.statusPodomoro === 'working') {
            // console.log('deja working');
            if (Pomodoro.isTimeEnd(citoyenOne.oceco.podomoro.working.startedAt, 'work')) {
              // console.log('pas finis');
              IonPopup.confirm({
                title: 'Pomodoro',
                template: 'le dernier pomodoro est resté en état de marche, souhaitez-vous le reprendre ou l\'interrompre ?',
                onOk() {
                  Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
                  Pomodoro.restartWorkingNotFinish(citoyenOne.oceco.podomoro.working.startedAt, 'work');
                },
                onCancel() {
                  Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
                  Pomodoro.initialize();
                  Meteor.call('podomoroActionStatus', { actionId: citoyenOne.oceco.podomoro.working.actionId, statusPodomoro: 'pendingInt' });
                  Pomodoro.podomoroSessionSet('playerStatus', 'play');
                },
                cancelText: 'Interrompre',
                okText: 'Reprendre',
              });
            } else {
              // console.log('finis');
              IonPopup.confirm({
                title: 'Pomodoro',
                template: 'le dernier pomodoro est resté en état de marche, mais le temps est dépassé souhaitez vous le valider ou l\'interrompre ?',
                onOk() {
                  Pomodoro.initialize();
                  Meteor.call('podomoroActionStatus', { actionId: citoyenOne.oceco.podomoro.working.actionId, statusPodomoro: 'validateInt' });
                  Pomodoro.podomoroSessionSet('playerStatus', 'play');
                },
                onCancel() {
                  Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
                  Pomodoro.initialize();
                  Meteor.call('podomoroActionStatus', { actionId: citoyenOne.oceco.podomoro.working.actionId, statusPodomoro: 'pendingInt' });
                  Pomodoro.podomoroSessionSet('playerStatus', 'play');
                },
                cancelText: 'Interrompre',
                okText: 'Valider',
              });
            }
          } else if (citoyenOne.oceco.podomoro.working.statusPodomoro === 'paused' && citoyenOne.oceco.podomoro.working.remainingTime) {
            // console.log('deja paused');
            IonPopup.confirm({
              title: 'Pomodoro',
              template: 'le dernier pomodoro est resté en pause, souhaitez-vous le reprendre ou l\'interrompre ?',
              onOk() {
                Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
                Pomodoro.restartPausedNotFinish(citoyenOne.oceco.podomoro.working.remainingTime);
              },
              onCancel() {
                Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
                Pomodoro.initialize();
                Meteor.call('podomoroActionStatus', { actionId: citoyenOne.oceco.podomoro.working.actionId, statusPodomoro: 'pendingInt' });
                Pomodoro.podomoroSessionSet('playerStatus', 'play');
              },
              cancelText: 'Interrompre',
              okText: 'Reprendre',
            });
          } else if (citoyenOne.oceco.podomoro.working.statusPodomoro === 'resting') {
            // console.log('deja resting');
            let typeRest;
            Pomodoro.podomoroSessionSet('pomodoroCount', citoyenOne.oceco.podomoro.working.pomodoroCount);
            if (citoyenOne.oceco.podomoro.working.pomodoroCount % 4 === 0) {
              typeRest = 'long_rest';
            } else {
              typeRest = 'short_rest';
            }
            if (Pomodoro.isTimeEnd(citoyenOne.oceco.podomoro.working.restingStart, typeRest)) {
              // console.log('pas finis');
              IonPopup.confirm({
                title: 'Pomodoro',
                template: 'le dernier pomodoro est resté en état de repos, souhaitez-vous le reprendre ou l\'interrompre ?',
                onOk() {
                  Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
                  Pomodoro.podomoroSessionSet('pomodoroStatus', 'resting');
                  Pomodoro.restartWorkingNotFinish(citoyenOne.oceco.podomoro.working.restingStart, typeRest);
                },
                onCancel() {
                  Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
                  Pomodoro.initialize();
                  Meteor.call('podomoroActionStatus', { statusPodomoro: 'pending' });
                  Pomodoro.podomoroSessionSet('playerStatus', 'play');
                },
                cancelText: 'Interrompre',
                okText: 'Reprendre',
              });
            } else {
              // console.log('finis');
              Pomodoro.podomoroSessionSet('lastPlayedTask', citoyenOne.oceco.podomoro.working.actionId);
              Pomodoro.initialize();
              Meteor.call('podomoroActionStatus', { statusPodomoro: 'pending' });
              Pomodoro.podomoroSessionSet('playerStatus', 'play');
            }
          }
        }
      }
    }
  }.bind(this));
});

Template.podomoroPlayer.helpers({

  actionFindOne() {
    const selectUserid = `podomoro.${Meteor.userId()}.statusPodomoro`;
    return Actions.findOne({ [selectUserid]: { $in: ['working', 'paused', 'resting'] } });
  },

  statusLecteur() {
    const playerStatus = Pomodoro.podomoroSessionGet('playerStatus');
    return (playerStatus || 'play');
  },

  statusCss() {
    const playerStatus = Pomodoro.podomoroSessionGet('playerStatus');
    if (playerStatus === 'pause') {
      return 'energized';
    }
    return 'balanced';
  },

  statusTxt() {
    const playerStatus = Pomodoro.podomoroSessionGet('playerStatus');
    if (playerStatus === 'pause') {
      return 'positive';
    } if (playerStatus === 'stop') {
      return 'positive';
    }
    return 'energized';
  },

  timer() {
    return Pomodoro.podomoroSessionGet('pomodoroTimer');
  },

  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.podomoroPlayer.events({
  'click .controls .play'() {
    const self = this;
    const secondes = Pomodoro.getSecondes();
    const lastPlayedTask = Pomodoro.podomoroSessionGet('lastPlayedTask');
    const totalSecondes = Pomodoro.getEndTime();
    const secondesEcoule = totalSecondes - secondes;
    // const secondesEcouleStr = moment.utc(moment.duration(secondesEcoule, 'seconds').asMilliseconds()).format('HH:mm:ss');
    if (lastPlayedTask !== this._id._str) {
      Meteor.call('podomoroActionStatus', { actionId: this._id._str, statusPodomoro: 'pausedInt', secondes: Pomodoro.getSecondes() }, (err) => {
        if (err) {
          IonPopup.alert({ template: i18n.__(err.reason) });
        } else {
          Pomodoro.pause();
          IonPopup.confirm({
            title: `Ajouter le temps passé sur l'action ? (${secondesEcoule})`,
            template: '',
            onOk() {
              Meteor.call('podomoroActionStatus', {
                actionId: lastPlayedTask, statusPodomoro: 'pending', secondes, totalSecondes,
              }, (err) => {
                if (err) {
                  IonPopup.alert({ template: i18n.__(err.reason) });
                } else {
                  Pomodoro.play();
                  Pomodoro.podomoroSessionSet('lastPlayedTask', self._id._str);
                  Meteor.call('podomoroActionStatus', { actionId: self._id._str, statusPodomoro: 'working' });
                }
              });
            },
            onCancel() {
              Meteor.call('podomoroActionStatus', { actionId: lastPlayedTask, statusPodomoro: 'pending' }, (err) => {
                if (err) {
                  IonPopup.alert({ template: i18n.__(err.reason) });
                } else {
                  Pomodoro.play();
                  Pomodoro.podomoroSessionSet('lastPlayedTask', self._id._str);
                  Meteor.call('podomoroActionStatus', { actionId: self._id._str, statusPodomoro: 'working' });
                }
              });
            },
            cancelText: 'Non',
            okText: 'Oui',
          });
        }
      });
    } else {
      Pomodoro.play();
      Meteor.call('podomoroActionStatus', { actionId: Pomodoro.podomoroSessionGet('lastPlayedTask'), statusPodomoro: 'pending' }, (err) => {
        if (err) {
          IonPopup.alert({ template: i18n.__(err.reason) });
        } else {
          Pomodoro.podomoroSessionSet('lastPlayedTask', self._id._str);
          Meteor.call('podomoroActionStatus', { actionId: self._id._str, statusPodomoro: 'working' });
        }
      });
    }
  },

  'click .controls .pause'() {
    Meteor.call('podomoroActionStatus', { actionId: this._id._str, statusPodomoro: 'pausedInt', secondes: Pomodoro.getSecondes() }, (err) => {
      if (err) {
        IonPopup.alert({ template: i18n.__(err.reason) });
      } else {
        Pomodoro.pause();
      }
    });
  },

  'click .controls .reset'() {
    const self = this;
    const secondes = Pomodoro.getSecondes();
    const totalSecondes = Pomodoro.getEndTime();

    const secondesEcoule = totalSecondes - secondes;
    const secondesEcouleStr = moment.utc(moment.duration(secondesEcoule, 'seconds').asMilliseconds()).format('LTS'); // HH:mm:ss
    Meteor.call('podomoroActionStatus', { actionId: this._id._str, statusPodomoro: 'pausedInt', secondes: Pomodoro.getSecondes() }, (err) => {
      if (err) {
        IonPopup.alert({ template: i18n.__(err.reason) });
      } else {
        Pomodoro.pause();
        IonPopup.confirm({
          title: `Ajouter le temps passé sur l'action ? ${secondesEcouleStr}`,
          template: '',
          onOk() {
            Meteor.call('podomoroActionStatus', {
              actionId: self._id._str, statusPodomoro: 'pendingInt', secondes, totalSecondes,
            }, (err) => {
              if (err) {
                IonPopup.alert({ template: i18n.__(err.reason) });
              } else {
                Pomodoro.podomoroSessionSet('pomodoroInProgress', false);
                Pomodoro.initialize();
                Pomodoro.podomoroSessionSet('playerStatus', 'play');
              }
            });
          },
          onCancel() {
            Meteor.call('podomoroActionStatus', { actionId: self._id._str, statusPodomoro: 'pendingInt' }, (err) => {
              if (err) {
                IonPopup.alert({ template: i18n.__(err.reason) });
              } else {
                Pomodoro.podomoroSessionSet('pomodoroInProgress', false);
                Pomodoro.initialize();
                Pomodoro.podomoroSessionSet('playerStatus', 'play');
              }
            });
          },
          cancelText: 'Non',
          okText: 'Oui',
        });
      }
    });
  },
});
