/* eslint-disable consistent-return */
/* eslint-disable meteor/no-session */
/* global Session device cordova */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import { ReactiveVar } from 'meteor/reactive-var';
import { $ } from 'meteor/jquery';
import { ActivityStream } from '../api/collection/activitystream.js';
import QRCode from 'qrcode';
import { StatusTimes } from '../api/helpers.js';

import './notifications/notifications.js';

import './pixel.html';

import { Organizations } from '../api/collection/organizations.js';
import { Citoyens } from '../api/collection/citoyens.js';
import { Projects } from '../api/collection/projects.js';

import './components/scope/item.js';

import { Pomodoro } from '../api/client/podomoro.js';

import './rooms/actions/podomoro/podomoro.html';
import './rooms/actions/podomoro/podomoro.js';

Template.layout.onCreated(function () {
  Meteor.subscribe('notificationsCountUser');

  this.ready = new ReactiveVar(false);
  this.autorun(function () {
    if (Session.get('orgaCibleId')) {
      const handleScopeDetail = Meteor.subscribe('scopeDetail', 'organizations', Session.get('orgaCibleId'));
      const handleDirectoryListProjects = Meteor.subscribe('directoryListProjects', 'organizations', Session.get('orgaCibleId'));
      const handleCitoyen = Meteor.subscribe('citoyen');
      if (handleCitoyen.ready()) {
        Pomodoro.podomoroSessionSetDefault('pomodoroTimeWork', StatusTimes.work);
        Pomodoro.podomoroSessionSetDefault('pomodoroTimeShortRest', StatusTimes.short_rest);
        Pomodoro.podomoroSessionSetDefault('pomodoroTimeLongRest', StatusTimes.long_rest);
      }

      if (handleScopeDetail.ready() && handleDirectoryListProjects.ready() && handleCitoyen.ready()) {
        /* "oceco" : {
        "pole" : true,
        "organizationAction" : false,
        "projectAction" : false,
        "eventAction" : true,
        "commentsAction": false,
        "memberAuto" : false,
        "memberValidationInviteAuto" : false,
        "contributorValidationInviteAuto" : false,
        "agenda" : true,
        "costum" : {
            "projects" : {
                "form" : {
                    "geo" : false
                }
            },
            "events" : {
                "form" : {
                    "geo" : false
                }
            }
        },
        account: {
          textVotreCreditTemps:'Votre crédit temps',
          textUnite : 'R'
        },
        home: {
          textTitre: 'Choix du pole',
          textInfo: 'Sur cette page vous devrez choisir parmis les differents poles de votre organisation afin de voir les évenements et les actions qui en font partie. Si vous voulez voir les évenement par date merci de vous rendre dans "agenda"'
        },
        wallet: {
          textBouton: 'Espace temps',
          textTitre: 'Espace temps',
          textInfo: 'Votre éspace temps vous permet de voire à la fois vos crédits temps gagné ou dépensés et vos actions futur et à venir',
          coupDeMain: {
            textBouton: 'Coup de main',
            textTitre: 'Coup de main',
            textInfo: 'C\'est la liste de toute vos actions à faire au quels vous êtes inscrit'
          },
          enAttente: {
            textBouton: 'En attente',
            textTitre: 'En attente',
            textInfo: 'C\'est la liste de toute vos actions finis qui doivent êtres validé par un administrateur'
          },
          valides: {
            textBouton: 'Validés',
            textTitre: 'Validés',
            textInfo: 'C\'est la liste de vos 100 dernières anciennes actions qui vous ont rapporté ou qui vous ont couté des crédits'
          }
        }
    } */
        const orgaOne = Organizations.findOne({
          _id: new Mongo.ObjectID(Session.get('orgaCibleId')),
        });
        if (orgaOne && orgaOne.oceco) {
          Session.setPersistent('settingOceco', orgaOne.oceco);
          this.ready.set(handleScopeDetail.ready());
        }
        if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.pixelhumain && Session.get('orgaCibleId')) {
          if (orgaOne && orgaOne.isMembers()) {
            Session.setPersistent(`isMembreOrga${Session.get('orgaCibleId')}`, true);
          }

          if (orgaOne && orgaOne.isAdmin()) {
            Session.setPersistent(`isAdmin${Session.get('orgaCibleId')}`, true);
            Session.setPersistent(`isAdminOrga${Session.get('orgaCibleId')}`, true);
          } else {
            const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
            if (orgaOne && orgaOne.links && orgaOne.links.projects && userC && userC.links && userC.links.projects) {
              // eslint-disable-next-line no-unused-vars
              const arrayIds = Object.keys(orgaOne.links.projects)
                .filter((k) => userC.links.projects[k] && userC.links.projects[k].isAdmin && !userC.links.projects[k].toBeValidated && !userC.links.projects[k].isAdminPending && !userC.links.projects[k].isInviting)
                // eslint-disable-next-line array-callback-return
                .map((k) => new Mongo.ObjectID(k));
              // ? info : on compte pour sur que le projets soit parent
              const countProject = Projects.find({ _id: { $in: arrayIds } }).count();
              const isAdmin = !!(countProject > 0);
              Session.setPersistent(`isAdmin${Session.get('orgaCibleId')}`, isAdmin);
              Session.setPersistent(`isAdminOrga${Session.get('orgaCibleId')}`, false);
            }
          }
        }
      }
    }
  }.bind(this));

  this.autorun(function () {
    if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { 'oceco.pomodoroTimeWork': 1, 'oceco.pomodoroTimeShortRest': 1, 'oceco.pomodoroTimeLongRest': 1 } })) {
      const citoyenOne = () => (Meteor.userId() && Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { oceco: 1 } }) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { oceco: 1 } }) : null);
      if (citoyenOne && citoyenOne().oceco) {
        Pomodoro.podomoroSessionSet('pomodoroTimeWork', (citoyenOne().oceco.pomodoroTimeWork * 60) || StatusTimes.work);
        Pomodoro.podomoroSessionSet('pomodoroTimeShortRest', (citoyenOne().oceco.pomodoroTimeShortRest * 60) || StatusTimes.short_rest);
        Pomodoro.podomoroSessionSet('pomodoroTimeLongRest', (citoyenOne().oceco.pomodoroTimeLongRest * 60) || StatusTimes.long_rest);
      }
    }
  });
});

Template.layout.events({
  'click [target=_blank]'(event) {
    event.preventDefault();
    const url = $(event.currentTarget).attr('href');
    if (Meteor.isCordova) {
      cordova.InAppBrowser.open(url, '_system');
    } else {
      window.open(url, '_blank');
    }
  },
  'click [target=_system]'(event) {
    event.preventDefault();
    const url = $(event.currentTarget).attr('href');
    if (Meteor.isCordova) {
      cordova.InAppBrowser.open(url, '_system');
    } else {
      window.open(url, '_system');
    }
  },
  'change .all-read input'() {
    Meteor.call('allSeen');
    Meteor.call('allRead');
  },
  'click .all-seen'() {
    Meteor.call('allSeen');
  },
});

Template.layout.helpers({
  scope() {
    const orgaOne = Organizations.findOne({
      _id: new Mongo.ObjectID(Session.get('orgaCibleId')),
    });
    return orgaOne;
  },
  allReadChecked(notificationsCount) {
    if (notificationsCount === 0) {
      return 'checked';
    }
    return undefined;
  },
  notifications() {
    return ActivityStream.api.isUnread();
  },
  routeSwitch() {
    return Router.current().route.getName() !== 'switch';
  },
  lastPlayedTask() {
    return Pomodoro.podomoroSessionGet('lastPlayedTask');
  }
});

Template.forceUpdateAvailable.events({
  'click .positive-url'(event) {
    event.preventDefault();
    const url = event.currentTarget.getAttribute('href');
    window.open(url, '_system');
  },
});

Template.shareUrl.events({
  'focus input[name="shareurl"]'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="shareurl"]');
    element.select();
  },
  'click #copyurl'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="shareurl"]');
    element.select();
    navigator.permissions.query({ name: 'clipboard-write' }).then((result) => {
      if (result.state === 'granted' || result.state === 'prompt') {
        navigator.clipboard.writeText(element.value).then(function () {
          /* clipboard successfully set */
        }, function () {
          /* clipboard write failed */
        });
      }
    });
  },
  'click #shareurl'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="shareurl"]');
    element.select();
    if (navigator.share) {
      navigator.share({
        url: element.value,
      })
        .then(() => console.log('Successful share'))
        .catch((error) => console.log('Error sharing', error));
    }
  },
});



Template.shareUrl.helpers({
  shareUrl() {
    return Meteor.absoluteUrl(window.location.pathname.replace(/\//, ''));
  },
  canShare() {
    return navigator.share;
  },
});

Template.generateQRCode.helpers({
  getQRCode() {
    const instance = Template.instance();
    return instance.Qrcode.get();
  }
});

Template.generateQRCode.onCreated(function () {
  const instance = this;
  instance.Qrcode = new ReactiveVar();
  QRCode.toDataURL(this.data.url)
    .then((url) => {
      // console.log(url);
      instance.Qrcode.set(url);
    })
    .catch((err) => {
      console.error(err);
    });
});