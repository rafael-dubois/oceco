import { Template } from 'meteor/templating';
import { lazy } from '../iolazyload.js';

import './item.html';

Template.scopeItemList.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});

Template.scopeItemListEvent.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});

Template.scopeItemListForm.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});
