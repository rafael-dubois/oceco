/* eslint-disable consistent-return */
/* eslint-disable meteor/no-session */
/* global Session $ _ Platform */
import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { ReactiveVar } from 'meteor/reactive-var';
import { Router } from 'meteor/iron:router';
import i18n from 'meteor/universe:i18n';
import { moment } from 'meteor/momentjs:moment';

import './home.html';

// collection
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';

import { searchAction } from '../../api/client/reactive.js';
import {
  compareValues, searchQuerySort, searchQuerySortActived, applyDiacritics,
} from '../../api/helpers.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Actions } from '../../api/collection/actions';

window.Organizations = Organizations;
window.Projects = Projects;

Template.homeView.onCreated(function () {
  // searchAction.set('search', null);
  if (Session.get('urlRedirect')) {
    Router.go(Session.get('urlRedirect'));
    Session.set('urlRedirect', null);
  }
});

Template.homeView.helpers({
  scope() {
    return Organizations.findOne({
      _id: new Mongo.ObjectID(Session.get('orgaCibleId')),
    });
  },
  RaffineriePoles() {
    if (Session.get('settingOceco').pole === true) {
      const id = new Mongo.ObjectID(Session.get('orgaCibleId'));
      const raffinerieCursor = Organizations.findOne({ _id: id });
      if (raffinerieCursor) {
        const raffinerieArray = raffinerieCursor.listProjectsCreator();
        const raffinerieTags = raffinerieArray ? raffinerieArray.map((tag) => tag.tags && tag.tags[0]).filter((tag) => typeof tag !== 'undefined') : null;
        const uniqueRaffinerieTags = raffinerieTags ? Array.from(new Set(raffinerieTags)) : null;
        return uniqueRaffinerieTags || [];
      }
    } else {
      const id = new Mongo.ObjectID(Session.get('orgaCibleId'));
      const raffinerieCursor = Organizations.findOne({ _id: id });
      return raffinerieCursor.listProjectsCreator() || [];
    }
  },
  search() {
    return searchAction.get('search');
  },
});

Template.projectsView.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.autorun(function () {
    const handle = this.subscribe('all.actions2', Session.get('orgaCibleId'));
    const handleAvatar = this.subscribe('all.avatarOne', Session.get('orgaCibleId'));
    const handleEvents = this.subscribe('poles.events', Session.get('orgaCibleId'));
    if (handle.ready() && handleEvents.ready() && handleAvatar.ready()) {
      this.ready.set(handle.ready());
    }
  }.bind(this));
});

Template.projectsView.helpers({
  poleProjects2() {
    const dataContext = Template.currentData();
    const search = dataContext && dataContext.user ? `@${dataContext.user.username}` : searchAction.get('search');
    // const search = searchAction.get('search');
    const searchSort = searchAction.get('searchSort');

    const queryProjectId = `parent.${Session.get('orgaCibleId')}`;
    const query = {};
    const options = {};
    query[queryProjectId] = { $exists: 1 };
    if (search && search.charAt(0) === ':' && search.length > 1) {
      query.name = { $regex: `.*${search.substr(1).replace(/[.*+?^${}()|[\]\\]/g, '\\$&')}.*`, $options: 'i' };
    }
    if (searchSort) {
      const arraySort = searchQuerySort('projects', searchSort);
      if (arraySort) {
        // options.sort = { ...arraySort };
        options.sort = arraySort;
      }
    }
    return Projects.find(query, options);
  },
  searchMaintenance() {
    return searchAction.get('searchMaintenance');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.searchActions.onRendered(function () {
  const wrap = $('.view .content.overflow-scroll');
  const search = $('.view .content.overflow-scroll #search');
  if (wrap.length > 0 && search.length > 0) {
    wrap.on('scroll', function () {
      if (!Platform.isIOS()) {
        if (this.scrollTop > 147) {
          wrap.addClass('fix-search');
        }
        if (this.scrollTop < 82) {
          wrap.removeClass('fix-search');
        }
      }
    });
  } else {
    const wrapModal = $('.modal .content.overflow-scroll');
    const searchModal = $('.modal .content.overflow-scroll #search');
    if (wrapModal.length > 0 && searchModal.length > 0) {
      wrapModal.on('scroll', function () {
        if (!Platform.isIOS()) {
          if (this.scrollTop > 147) {
            wrapModal.addClass('fix-search');
          }
          if (this.scrollTop < 82) {
            wrapModal.removeClass('fix-search');
          }
        }
      });
    }
  }
});

Template.searchActions.helpers({
  search() {
    const search = searchAction.get('search');
    if (Router.current().route.getName() === 'actionsList' || Router.current().route.getName() === 'actionsListOrga') {
      if (search && search.charAt(0) === ':') {
        searchAction.set('search', null);
      }
    }
    return searchAction.get('search');
  },
  searchTag() {
    return searchAction.get('searchTag');
  },
  searchMilestone() {
    return searchAction.get('searchMilestone');
  },
  searchUser() {
    return searchAction.get('searchUser');
  },
  searchHelp() {
    return searchAction.get('searchHelp');
  },
  searchMaintenance() {
    return searchAction.get('searchMaintenance');
  },
  sortActived() {
    if (searchAction.get('searchSort')) {
      if (Router.current().route.getName() === 'actionsList' || Router.current().route.getName() === 'actionsListOrga') {
        const actionArray = searchAction.get('searchSort');
        const searchPick = { actions: [...actionArray.actions] };
        return searchQuerySortActived(searchPick);
      }
      return searchQuerySortActived(searchAction.get('searchSort'));
    }
    return false;
  },
  allTags() {
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    if (!orgaOne) {
      return null;
    }
    const searchTag = searchAction.get('searchTag');
    const arrayAll = orgaOne.actionsAll().map((action) => action.tags).filter(Boolean);
    const mergeDedupe = (arr) => [...new Set([].concat(...arr))];
    const arrayAllMerge = mergeDedupe(arrayAll);

    return searchTag && searchTag.length > 1 ? arrayAllMerge.filter((item) => item.includes(searchTag.substr(1))) : arrayAllMerge;
  },
  allMilestones() {
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    if (!orgaOne) {
      return null;
    }
    const searchMilestone = searchAction.get('searchMilestone');
    const arrayAll = orgaOne.actionsAll().map((action) => action.milestone && action.milestone.name).filter(Boolean);
    const mergeDedupe = (arr) => [...new Set([].concat(...arr))];
    const arrayAllMerge = mergeDedupe(arrayAll);

    return searchMilestone && searchMilestone.length > 1 ? arrayAllMerge.filter((item) => item.includes(searchMilestone.substr(1))) : arrayAllMerge;
  },
  allUsers() {
    const search = searchAction.get('search');
    if (search && search.charAt(0) === '@' && search.length > 1) {
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'regex');
      const query = {};
      query.$or = [];
      const queryName = {};
      const queryUsername = {};
      queryName.name = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
      queryUsername.username = { $regex: `.*${search.substr(1)}.*`, $options: 'i' };
      query.$or.push(queryName);
      query.$or.push(queryUsername);
      return Citoyens.find(query);
    }
    return Citoyens.find();
  },
});

Template.usersViewUser.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.autorun(function () {
    if (Session.get('settingOceco') && Session.get('settingOceco').dashboardUser) {
      // si retrouve reglage dans oceco orga utiliser
      const handle = this.subscribe('all.actions2', Session.get('orgaCibleId'));
      const handleAvatar = this.subscribe('all.avatarOne', Session.get('orgaCibleId'));
      const handleEvents = this.subscribe('poles.events', Session.get('orgaCibleId'));
      if (handle.ready() && handleEvents.ready() && handleAvatar.ready()) {
        this.ready.set(handle.ready());
      }
    } else {
      // pas de reglage
      // console.log(Router.current().params._id);
      this.ready.set(true);
    }
  }.bind(this));
});

Template.usersViewUser.helpers({
  allUsers() {
    if (Template.instance().ready.get()) {
      if (Session.get('settingOceco').dashboardUser && Session.get('settingOceco').dashboardUser.length > 0) {
        const arrayIds = Session.get('settingOceco').dashboardUser.map((k) => new Mongo.ObjectID(k));
        return Citoyens.find({ _id: { $in: arrayIds } });
      }
      // return Citoyens.find();
    }
  },
  currentId() {
    return Router.current().params._id;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.usersViewUserItem.helpers({
  poleProjects2() {
    const dataContext = Template.currentData();
    const search = dataContext && dataContext.user ? `@${dataContext.user.username}` : searchAction.get('search');
    // const search = searchAction.get('search');
    const searchSort = searchAction.get('searchSort');

    const queryProjectId = `parent.${Session.get('orgaCibleId')}`;
    const query = {};
    const options = {};
    query[queryProjectId] = { $exists: 1 };
    if (search && search.charAt(0) === ':' && search.length > 1) {
      query.name = { $regex: `.*${search.substr(1).replace(/[.*+?^${}()|[\]\\]/g, '\\$&')}.*`, $options: 'i' };
    }
    if (searchSort) {
      const arraySort = searchQuerySort('projects', searchSort);
      if (arraySort) {
        // options.sort = { ...arraySort };
        options.sort = arraySort;
      }
    }
    return Projects.find(query, options);
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.searchActions.events({
  'click .searchtag-js'(event) {
    event.preventDefault();
    if (this) {
      searchAction.set('search', `#${this}`);
    } else {
      searchAction.set('search', null);
    }
  },
  'click .searchmilestone-js'(event) {
    event.preventDefault();
    if (this) {
      searchAction.set('search', `~${this}`);
    } else {
      searchAction.set('search', null);
    }
  },
  'click .searchuser-js'(event) {
    event.preventDefault();
    if (this) {
      searchAction.set('search', `@${this.username}`);
    } else {
      searchAction.set('search', null);
    }
  },
  'click .searchfilter-js'(event) {
    event.preventDefault();
    const filter = $(event.currentTarget).data('action');
    if (filter) {
      searchAction.set('search', filter);
      searchAction.set('searchHelp', null);
    } else {
      searchAction.set('search', null);
    }
  },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);

      if (event.currentTarget.value.charAt(0) === '#') {
        searchAction.set('searchTag', event.currentTarget.value);
      } else {
        searchAction.set('searchTag', null);
      }

      if (event.currentTarget.value.charAt(0) === '~') {
        searchAction.set('searchMilestone', event.currentTarget.value);
      } else {
        searchAction.set('searchMilestone', null);
      }

      if (event.currentTarget.value.length === 1 && event.currentTarget.value.charAt(0) === '?') {
        searchAction.set('searchHelp', true);
      } else {
        searchAction.set('searchHelp', null);
      }

      if (event.currentTarget.value.length > 0 && event.currentTarget.value.charAt(0) === '@') {
        searchAction.set('searchUser', true);
      } else {
        searchAction.set('searchUser', null);
      }

      if (event.currentTarget.value.length === 4 && event.currentTarget.value === '?old') {
        searchAction.set('searchMaintenance', true);
      } else {
        searchAction.set('searchMaintenance', null);
      }

      searchAction.set('search', event.currentTarget.value);
      searchAction.set('actionName', event.currentTarget.value);
    } else {
      searchAction.set('search', null);
      searchAction.set('searchTag', null);
      searchAction.set('searchMilestone', null);
      searchAction.set('searchHelp', null);
      searchAction.set('searchMaintenance', null);
      searchAction.set('actionName', null);
      searchAction.set('searchUser', null);
    }
  }, 500),
  // Pressing Ctrl+Enter should submit action
  'keydown #search'(event) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      // savoir si je suis dans le detail d'un element
      // récupérer les valeurs utiles pour soit publier
      // soit rediriger vers le form action avce le name préremplie
      if (event.currentTarget.value.length > 0) {
        if (Router.current().route.getName() === 'actionsList' || Router.current().route.getName() === 'actionsListOrga') {
          if (Template.currentData().isAdmin()) {
            searchAction.set('actionName', event.currentTarget.value);
            Router.go('actionsAdd', { _id: Router.current().params._id, scope: Router.current().params.scope });
          }
        }
      }
    }
  },
});

Template.searchSort.helpers({
  isHome() {
    return Router.current().route.getName() === 'home';
  },
  searchSort() {
    if (searchAction.get('searchSort')) {
      return searchAction.get('searchSort');
    }
    // Todo : translate label
    const sortDefault = {
      projects: [
        {
          label: i18n.__('sortModal.name'), type: 'projects', field: 'name', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.activity_date'), type: 'projects', field: 'modified', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.start_date'), type: 'projects', field: 'startDate', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.end_date'), type: 'projects', field: 'endDate', existField: true, checked: false, fieldDesc: false,
        },
      ],
      events: [
        {
          label: i18n.__('sortModal.name'), type: 'events', field: 'name', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.activity_date'), type: 'events', field: 'modified', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.start_date'), type: 'events', field: 'startDate', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.end_date'), type: 'events', field: 'endDate', existField: true, checked: false, fieldDesc: false,
        },
      ],
      actions: [
        {
          label: i18n.__('sortModal.name'), type: 'actions', field: 'name', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.activity_date'), type: 'actions', field: 'modified', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.creation_date'), type: 'actions', field: 'created', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.start_date'), type: 'actions', field: 'startDate', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.start_date'), type: 'actions', field: 'endDate', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.contributor'), type: 'actions', field: 'links.contributors', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.comment'), type: 'actions', field: 'commentCount', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.credits'), type: 'actions', field: 'credits', existField: true, checked: false, fieldDesc: false,
        },
      ],
    };
    searchAction.set('searchSort', sortDefault);
    return searchAction.get('searchSort');
  },
  orderType(type) {
    if (type) {
      const sort = searchAction.get('searchSort');
      return sort && sort[type] ? [...sort[type]].filter((item) => item.checked === true).sort(compareValues('order')) : [];
    }
  },
});

Template.searchSortItemToggle.events({
  'click .sort-checked-js'(event) {
    const self = this;
    if (this.type) {
      const sort = searchAction.get('searchSort');
      const arrayOrder = sort[this.type].filter((item) => item.checked === true);
      const countOrder = arrayOrder && arrayOrder.length > 0 ? arrayOrder.length : 0;
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.checked = event.currentTarget.checked;
          if (event.currentTarget.checked) {
            item.order = countOrder + 1;
          } else {
            delete item.order;
          }
          return item;
        }
        return item;
      });
      searchAction.set('searchSort', sort);
    }
  },
  'click .sort-desc-js'(event) {
    const self = this;
    if (this.type) {
      const sort = searchAction.get('searchSort');
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.fieldDesc = event.currentTarget.checked;
          return item;
        }
        return item;
      });
      searchAction.set('searchSort', sort);
    }
  },
});

const dateUnixSubtract = (months) => {
  const inputDate = moment(new Date()).subtract(months, 'months');
  const formatDate = moment(inputDate).format();
  const inputUnix = moment(formatDate).unix();
  return inputUnix;
};

Template.actionsMaintenanceItem.helpers({
  maintenanceChiffreCount(month, contributor = true) {
    const inputUnix = dateUnixSubtract(month);
    const query = {};
    query.status = 'todo';
    query.created = { $exists: true, $lt: inputUnix };
    // sans contributeur
    if (!contributor) {
      query['links.contributors'] = { $exists: false };
    }
    // console.log('count action plus de 18 mois', Actions.find(query).count());
    return Actions.find(query).count();
  },
  maintenanceChiffre(month, contributor = true) {
    const inputUnix = dateUnixSubtract(month);
    const query = {};
    query.status = 'todo';
    query.created = { $exists: true, $lt: inputUnix };
    // sans contributeur
    if (!contributor) {
      query['links.contributors'] = { $exists: false };
    }
    // console.log('count action plus de 18 mois', Actions.find(query).count());
    return Actions.find(query, {
      sort: { created: -1 },
    });
  },
  maintenanceChiffreStartEndCount(monthStart, monthEnd, contributor = true) {
    const inputUnixStart = dateUnixSubtract(monthStart);
    const inputUnixEnd = monthEnd ? dateUnixSubtract(monthEnd) : new Date().getTime();
    const query = {};
    query.status = 'todo';
    query.created = { $exists: true, $lt: inputUnixEnd, $gt: inputUnixStart };
    // sans contributeur
    if (!contributor) {
      query['links.contributors'] = { $exists: false };
    }
    // console.log('count action plus de 18 mois', Actions.find(query).count());
    return Actions.find(query).count();
  },
  maintenanceChiffreStartEnd(monthStart, monthEnd, contributor = true) {
    const inputUnixStart = dateUnixSubtract(monthStart);
    const inputUnixEnd = monthEnd ? dateUnixSubtract(monthEnd) : new Date().getTime();
    const query = {};
    query.status = 'todo';
    query.created = { $exists: true, $lt: inputUnixEnd, $gt: inputUnixStart };
    // sans contributeur
    if (!contributor) {
      query['links.contributors'] = { $exists: false };
    }
    // console.log('count action plus de 18 mois', Actions.find(query).count());
    return Actions.find(query, {
      sort: { created: -1 },
    });
  },
});
