/* eslint-disable no-underscore-dangle */
/* eslint-disable no-lonely-if */
/* eslint-disable meteor/no-session */
/* global Session IonPopup IonModal IonToast AutoForm */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import i18n from 'meteor/universe:i18n';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { Mongo } from 'meteor/mongo';
import { $ } from 'meteor/jquery';

// import { actionsSubs } from '../../api/client/subsmanager.js';

import './admin.html';

// collection
// import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
// import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Actions } from '../../api/collection/actions';

import '../components/directory/list.js';
import '../components/news/button-card.js';
// import '../components/news/card.js';

import { arrayLinkToModerate, arrayLinkValidated } from '../../api/helpers.js';

const pageSession = new ReactiveDict('pageAdmin');

/* window.Events = Events;
window.Organizations = Organizations;
window.Projects = Projects;
window.Citoyens = Citoyens;
window.Actions = Actions; */

Template.adminDashboard.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.autorun(function () {
    const handleScope = this.subscribe('scopeDetail', 'organizations', Session.get('orgaCibleId'));
    if (handleScope.ready()) {
      this.ready.set(handleScope.ready());
    }
  }.bind(this));
  this.selectview = new ReactiveVar('aValider');
});

Template.actionToValidate.onCreated(function () {
  this.scrollValidateAction = new ReactiveVar(false);
});

Template.actionToValidate.events({
  'click .button-validate-js'(event) {
    event.preventDefault();
    if (!Template.instance().scrollValidateAction.get()) {
      Template.instance().scrollValidateAction.set(true);
    } else {
      Template.instance().scrollValidateAction.set(false);
    }
  },
});
Template.adminDashboard.events({
  // 'click .admin-validation-js'(event, instance) {
  //   event.preventDefault();
  //   const usrId = $(event.currentTarget).attr('usrId');
  //   const actionId = $(event.currentTarget).attr('actionId');
  //   Meteor.call('ValidateAction', {
  //     actId: actionId, usrId,orgId: Session.get('orgaCibleId'),

  //   }, (err, res) => {
  //     if (err) {
  //       alert(err);
  //     } else {
  //     }
  //   });
  // },
  'click .change-selectview-js'(event) {
    event.preventDefault();
    Template.instance().selectview.set(event.currentTarget.id);
  },
});

Template.adminDashboard.helpers({
  scope() {
    return Organizations.findOne({
      _id: new Mongo.ObjectID(Session.get('orgaCibleId')),
    });
  },
  actionToaccept() {
    const actionArray = [];
    Actions.find({ finishedBy: { $exists: true } }).forEach((action) => {
      if (action.finishedBy) {
        $.each(action.finishedBy, function (index, value) {
          if (value === 'toModerate') {
            actionArray.push(action._id);
          }
        });
      }
    });
    return Actions.find({ _id: { $in: actionArray } });
  },

  dataReady() {
    return Template.instance().ready.get();
  },
  selectview() {
    return Template.instance().selectview.get();
  },
});

Template.listProjectsAValiderRaf.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Session.get('orgaCibleId'));
    pageSession.set('scope', 'organizations');
  });

  this.autorun(function () {
    const handle = this.subscribe('directoryProjectsListEventsActions', 'organizations', Session.get('orgaCibleId'), 'todo');
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.listProjectsAValiderRaf.helpers({
  actionToaccept() {
    return Actions.find({ finishedBy: { $exists: true } });
  },
  numberTovalidate(actions) {
    return actions && arrayLinkToModerate(actions) && arrayLinkToModerate(actions).length ? arrayLinkToModerate(actions).length : 0;
  },
  userTovalidate(actions) {
    const objIdArray = arrayLinkToModerate(actions);
    return Citoyens.find({ _id: { $in: objIdArray } });
  },
  numberIsvalidate(actions) {
    return actions && arrayLinkValidated(actions) && arrayLinkValidated(actions).length ? arrayLinkValidated(actions).length : 0;
  },
  userIsvalidate(actions) {
    const objIdArray = arrayLinkValidated(actions);
    return Citoyens.find({ _id: { $in: objIdArray } });
  },
  validateEqualContributor(action) {
    const numberValidate = action && action.finishedBy && arrayLinkValidated(action.finishedBy) && arrayLinkValidated(action.finishedBy).length ? arrayLinkValidated(action.finishedBy).length : 0;
    const numberContributor = action && action.countContributors();
    if (numberValidate === numberContributor && numberContributor > 0) {
      return true;
    }
    return false;
  },
  isValidatedContributor(actions) {
    return actions && actions.countContributors() > 0;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.adminButton.events({
  'click .admin-all-actions-all-finish-js'(event) {
    event.preventDefault();
    const actions = this.organization.listProjectsEventsActionsCreator();
    const { organization } = this;
    IonPopup.confirm({
      title: i18n.__('finish'),
      template: i18n.__('Warning, are you sure you want to set all users of all actions in the list to finish ?'),
      onOk() {
        actions.forEach((action) => {
          const organizationId = organization._id._str;
          const actionId = action._id._str;
          if (action.parentType === 'events') {
            // events
            if (action && action.countContributors() > 0) {
              action.listContributors().forEach((contributor) => {
                if (action.userTovalidate(contributor._id._str) || action.userIsValidated(contributor._id._str) || action.userIsNoValidated(contributor._id._str)) {
                  //
                } else {
                  Meteor.call('finishActionAdmin', {
                    actId: actionId,
                    usrId: contributor._id._str,
                    orgId: organizationId,
                  }, (err) => {
                    if (err) {
                      IonToast.show({
                        title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
                      });
                    } else {
                      // Todo : message i18n avec action.name et contributor.name terminé
                      /* IonToast.show({
                        title: i18n.__(''), position: 'bottom', type: 'success', showClose: false, template: `<i class="icon ion-checkmark"></i> ${i18n.__('')}`,
                      }); */
                    }
                  });
                }
              });
            }
          }
        });
      },
      onCancel() {

      },
      cancelText: i18n.__('cancel'),
      okText: i18n.__('finish'),
    });

  },
  'click .admin-all-actions-all-validate-js'(event) {
    event.preventDefault();
    // console.log('admin-all-actions-all-validate-js', this);
    const actions = this.organization.listProjectsEventsActionsCreator();
    // console.log('actions', actions);
    const { organization } = this;

    IonPopup.confirm({
      title: i18n.__('Validate'),
      template: i18n.__('Warning, are you sure you want to validate all users of all actions ?'),
      onOk() {
        actions.forEach((action) => {
          // console.log('action', action);
          if (action.finishedBy) {
            const objIdArray = arrayLinkToModerate(action.finishedBy);
            const organizationId = organization._id._str;
            const actionId = action._id._str;
            const { credits } = action;
            if (action && action.countContributors() > 0) {
              if (!action.options || (action.options && !action.options.creditSharePorteur)) {
                Citoyens.find({ _id: { $in: objIdArray } }, { fields: { _id: 1, name: 1 } }).forEach((citoyen) => {
                  Meteor.call('validateUserActions', {
                    actionId,
                    userId: citoyen._id._str,
                    organizationId,
                    commentaire: 'nocomment',
                    credits,
                  }, (err) => {
                    if (err) {
                      IonToast.show({
                        title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
                      });
                    } else {
                      // Todo : message i18n avce action.name et citoyens.name validé
                      IonToast.show({
                        title: i18n.__('Validate'), position: 'bottom', type: 'success', showClose: false, template: `<i class="icon ion-checkmark"></i> ${i18n.__('user was validated on the action', { action: action.name, user: citoyen.name })}`,
                      });
                    }
                  });
                });
              }
            }
          }
        });
      },
      onCancel() {

      },
      cancelText: i18n.__('cancel'),
      okText: i18n.__('Validate'),
    });
  },
  'click .admin-all-validate-js'(event) {
    event.preventDefault();
    const objIdArray = arrayLinkToModerate(this.action.finishedBy);
    const organizationId = this.organization._id._str;
    const actionId = this.action._id._str;
    const actionName = this.action.name;
    const { credits } = this.action;

    IonPopup.confirm({
      title: i18n.__('Validate'),
      template: i18n.__('Warning, are you sure you want to validate all users of this action ?'),
      onOk() {
        Citoyens.find({ _id: { $in: objIdArray } }, { fields: { _id: 1, name: 1 } }).forEach((citoyen) => {
          Meteor.call('validateUserActions', {
            actionId,
            userId: citoyen._id._str,
            organizationId,
            commentaire: 'nocomment',
            credits,
          }, (err) => {
            if (err) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
              });
            } else {
              IonToast.show({
                title: i18n.__('Validate'), position: 'bottom', type: 'success', showClose: false, template: `<i class="icon ion-checkmark"></i> ${i18n.__('user was validated on the action', { action: actionName, user: citoyen.name })}`,
              });
            }
          });
        });
      },
      onCancel() {

      },
      cancelText: i18n.__('cancel'),
      okText: i18n.__('Validate'),
    });
  },
  'click .admin-creditsdistributed-js'(event) {
    event.preventDefault();
    const usrId = $(event.currentTarget).attr('usrId');
    const actionId = $(event.currentTarget).attr('actionId');
    if (usrId && actionId) {
      const parentDataContext = {
        usrId, actionId, action: this.action, user: this.user, organization: this.organization,
      };
      IonModal.open('creditsDistributed', parentDataContext);
    }
  },
  'click .admin-validation-js'(event) {
    event.preventDefault();
    const usrId = $(event.currentTarget).attr('usrId');
    const actionId = $(event.currentTarget).attr('actionId');
    if (usrId && actionId) {
      Meteor.call('ValidateAction', {
        actId: actionId,
        usrId,
        orgId: Session.get('orgaCibleId'),
      }, (err) => {
        if (err) {
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
          });
        }
      });
    }
  },
  'click .admin-no-validation-js'(event) {
    event.preventDefault();
    const usrId = $(event.currentTarget).attr('usrId');
    const actionId = $(event.currentTarget).attr('actionId');
    if (usrId && actionId) {
      Meteor.call('noValidateAction', {
        actId: actionId,
        usrId,
        orgId: Session.get('orgaCibleId'),
      }, (err) => {
        if (err) {
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
          });
        }
      });
    }
  },
  'click .admin-finish-action-js'(event) {
    event.preventDefault();
    const usrId = $(event.currentTarget).attr('usrId');
    const actionId = $(event.currentTarget).attr('actionId');
    if (usrId && actionId) {
      Meteor.call('finishActionAdmin', {
        actId: actionId,
        usrId,
        orgId: Session.get('orgaCibleId'),
      }, (error) => {
        if (error) {
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
          });
        }
      });
    }
  },
  'click .admin-sortir-action-js'(event) {
    event.preventDefault();
    const memberId = $(event.currentTarget).attr('usrId');
    const id = $(event.currentTarget).attr('actionId');
    if (memberId && id) {
      Meteor.call('exitAction', {
        id,
        memberId,
        orgId: Session.get('orgaCibleId'),
      }, (error) => {
        if (error) {
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
          });
        }
      });
    }
  },
  'click .admin-rembourser-action-js'(event) {
    event.preventDefault();
    const memberId = $(event.currentTarget).attr('usrId');
    const id = $(event.currentTarget).attr('actionId');
    if (memberId && id) {
      IonPopup.confirm({
        title: 'Remboursement',
        template: 'Rembourser cet utilisateur ?',
        onOk() {
          Meteor.call('refundAdminAction', {
            id,
            memberId,
            orgId: Session.get('orgaCibleId'),
          }, (error) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
              });
            }
          });
        },
        onCancel() {
        },
        cancelText: i18n.__('no'),
        okText: i18n.__('yes'),
      });
    }
  },

});

Template.creditsDistributed.onCreated(function () {
  const dataContext = Template.currentData();
  pageSession.set('credits', null);
  this.autorun(function () {
    pageSession.set('citoyenId', dataContext.user._id._str);
    pageSession.set('actionId', dataContext.action._id._str);
    if (dataContext.action.options && dataContext.action.options.creditSharePorteur) {
      pageSession.set('actionCredits', dataContext.action.creditPartage());
    } else {
      pageSession.set('actionCredits', dataContext.action.credits);
    }
  });
});

Template.creditsDistributed.helpers({
  diffCredits() {
    if (pageSession.get('credits')) {
      if (this.action.options && this.action.options.creditSharePorteur) {
        return pageSession.get('credits') !== this.action.creditPartage();
      }
      return pageSession.get('credits') !== this.action.credits;
    }
    return false;
  },
  changeCredits() {
    return pageSession.get('credits');
  },
});

Template.creditsDistributed.events({
  'keyup input[name="credits"]'(event, instance) {
    event.preventDefault();
    if (instance.$(event.currentTarget).val()) {
      pageSession.set('credits', parseInt(instance.$(event.currentTarget).val()));
    } else {
      if (instance.data.action.options && instance.data.action.options.creditSharePorteur) {
        pageSession.set('credits', instance.data.action.creditPartage());
      } else {
        pageSession.set('credits', instance.data.action.credits);
      }
    }
  },
  'click .admin-validation-js'(event) {
    event.preventDefault();
    const usrId = $(event.currentTarget).attr('usrId');
    const actionId = $(event.currentTarget).attr('actionId');
    if (usrId && actionId) {
      Meteor.call('ValidateAction', {
        actId: actionId,
        usrId,
        orgId: Session.get('orgaCibleId'),
      }, (err) => {
        if (err) {
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${err && err.reason ? err.reason.replace(': ', '') : err}`,
          });
        }
      });
    }
  },
});

AutoForm.addHooks(['validateUserActions'], {
  after: {
    method(error) {
      if (!error) {
        IonModal.close();
      }
    },
  },
  before: {
    method(doc) {
      doc.organizationId = Session.get('orgaCibleId');
      doc.userId = pageSession.get('citoyenId');
      doc.actionId = pageSession.get('actionId');
      // eslint-disable-next-line no-empty
      if (doc.credits !== pageSession.get('actionCredits')) {

      } else {
        doc.commentaire = 'nocomment';
      }

      if (!doc.commentaire) {
        doc.commentaire = 'nocomment';
      }

      return doc;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(': ', ''));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      }
    }
  },
});

Template.listProjectsRaf.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Session.get('orgaCibleId'));
    pageSession.set('scope', 'organizations');
  });

  /* this.autorun(function () {
    const handle = this.subscribe('directoryProjectsListEvents', 'organizations', Session.get('orgaCibleId'));
    this.ready.set(handle.ready());
  }.bind(this)); */
});

Template.listProjectsRaf.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.listProjectsEventsRaf.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Session.get('orgaCibleId'));
    pageSession.set('scope', 'organizations');
    const handle = this.subscribe('directoryProjectsListEventsAdmin', 'organizations', Session.get('orgaCibleId'));
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.listProjectsEventsRaf.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.listProjectsEventsActionsRaf.onCreated(function () {
  this.ready = new ReactiveVar();

  pageSession.setDefault('limit', 10);
  pageSession.setDefault('incremente', 10);

  this.autorun(function () {
    pageSession.set('scopeId', Session.get('orgaCibleId'));
    pageSession.set('scope', 'organizations');
  });

  this.autorun(function () {
    if (pageSession.get('limit')) {
      const handleCounter = this.subscribe('directoryActionsAllCounter', 'organizations', Session.get('orgaCibleId'));
      const handle = this.subscribe('directoryActionsAll', 'organizations', Session.get('orgaCibleId'), 'all', pageSession.get('limit'));
      this.ready.set(handle.ready() && handleCounter.ready());
    }
  }.bind(this));
});

Template.listProjectsEventsActionsRaf.onRendered(function () {
  const showMoreVisible = () => {
    const $target = $('#showMoreResults');
    if (!$target.length) {
      return;
    }
    const threshold = $('.content.overflow-scroll').scrollTop()
      + $('.content.overflow-scroll').height() + 10;
    const heightLimit = $('.content.overflow-scroll .list').height();
    if (heightLimit < threshold) {
      if (!$target.data('visible')) {
        $target.data('visible', true);
        pageSession.set('limit', pageSession.get('limit') + pageSession.get('incremente'));
      }
    } else if ($target.data('visible')) {
      $target.data('visible', false);
    }
  };

  $('.content.overflow-scroll').scroll(showMoreVisible);
});

Template.listProjectsEventsActionsRaf.helpers({
  isLimit(countActions) {
    return countActions > pageSession.get('limit');
  },
  countActions() {
    return Counter.get(`countActionsAll.${Session.get('orgaCibleId')}`);
  },
  limit() {
    return pageSession.get('limit');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.listProjectsEventsActionsRaf.events({
  'click .give-me-more'(event) {
    event.preventDefault();
    const newLimit = pageSession.get('limit') + pageSession.get('incremente');
    pageSession.set('limit', newLimit);
  },
});

Template.actionToValidate.helpers({
  numberTovalidate(action) {
    const actionArray = [];
    if (action.finishedBy) {
      $.each(action.finishedBy, function (index, value) {
        if (value === 'toModerate') {
          const usrId = new Mongo.ObjectID(index);
          actionArray.push(usrId);
        }
      });
    }
    return Citoyens.find({ _id: { $in: actionArray } }).count();
  },
  buttonActivate() {
    return Template.instance().scrollValidateAction.get();
  },
  userTovalidate(action) {
    const actionArray = [];
    if (action.finishedBy) {
      $.each(action.finishedBy, function (index, value) {
        if (value === 'toModerate') {
          const usrId = new Mongo.ObjectID(index);
          actionArray.push(usrId);
        }
      });
    }
    return Citoyens.find({ _id: { $in: actionArray } });
  },
});
