/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable no-useless-escape */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* global PushNotification IonToast */
import { Meteor } from 'meteor/meteor';
import { Push } from 'meteor/raix:push';
import { Router } from 'meteor/iron:router';
import { Tracker } from 'meteor/tracker';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import i18n from 'meteor/universe:i18n';

import { ActivityStream } from '../../api/collection/activitystream.js';

import { notifyDisplay } from '../../api/helpers.js';

if (Meteor.isDevelopment) {
  Push.debug = true;
}

Meteor.startup(function () {
  if (Meteor.isCordova) {
    PushNotification.createChannel(
      function () {
        // console.log('Channel Created!');
      },
      function () {
        // console.log('Channel not created :(');
      },
      {
        id: 'PushPluginChannel',
        description: 'Channel Name Shown To Users',
        importance: 3,
        vibration: true,
      },
    );

    Push.Configure({
      cordovaOptions: {
        // Options here are passed to phonegap-plugin-push
        android: {
          sound: true,
          vibrate: true,
          clearBadge: false,
          clearNotifications: true,
          forceShow: false,
          icon: 'ic_stat_co_24',
          iconColor: '#6B97AF',
        },
        ios: {
          alert: true,
          badge: true,
          sound: true,
          clearBadge: true,
        },
      },
      appName: 'main',
    });

    Push.addListener('startup', function () {
      Router.go('/notifications');
    });

    Push.addListener('message', function (notification) {
      IonToast.show({
        template: notification.message, position: 'top', type: 'info', showClose: true, title: `<i class="icon ion-android-notifications"></i> ${i18n.__('Notifications')}`,
      });
    });
  } else if (!('Notification' in window)) {
    // eslint-disable-next-line no-alert
    // alert('This browser does not support desktop notification');
  } else {
    if (Notification.permission !== 'denied') {
      Notification.requestPermission(function () {
      });
    }

    if (Notification.permission === 'granted') {
      const query = {};
      query.created = { $gt: new Date() };
      const options = {};
      options.sort = { created: 1 };
      var initNotifystart = ActivityStream.find(query, options).observe({
        added(notification) {
          if (!initNotifystart) return;
          const title = notification.target && notification.target.name && notification.target.type === 'organizations' ? notification.target.name : 'notification';
          const textTarget = `${notifyDisplay(notification.notify)} - ${notification.target.name}`;
          const options = {
            body: textTarget,
            icon: '/icon.png',
            data: notification,
          };
          const n = new Notification(title, options);
          n.onclick = function () {
            if (notification.notify.url) {
              Router.go('/notifications');
              window.focus();
            } else {
              Router.go('/notifications');
              window.focus();
            }
          };
          Meteor.setTimeout(n.close.bind(n), 5000);
          IonToast.show({
            template: textTarget, position: 'top', type: 'info', showClose: true, title: `<i class="icon ion-android-notifications"></i> ${i18n.__('Notifications')}`,
          });
        },
      });
    }
  }
});

Tracker.autorun(() => {
  if (Meteor.userId()) {
    if (Counter.get(`notifications.${Meteor.userId()}.Unseen`)) {
      if (Meteor.isCordova) {
        Push.setBadge(Counter.get(`notifications.${Meteor.userId()}.Unseen`));
      } else {
        // web
      }
    }
  }
});
