/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Answers } from '../../api/collection/answers.js';
import { Actions } from '../../api/collection/actions.js';

Answers.after.update(function (userId, doc, fieldNames, modifier, options) {
  // console.log('Answers.after.update');
  if (modifier && modifier.$set) {
    const keys = Object.keys(modifier.$set);
    keys.forEach((key) => {
      const regex = /answers\.aapStep1\.depense\.([^\.]+)\.poste/;
      const found = key.match(regex);
      if (found && found.length > 1) {
        const actionId = doc && doc.answers && doc.answers.aapStep1 && doc.answers.aapStep1.depense && doc.answers.aapStep1.depense[found[1]] && doc.answers.aapStep1.depense[found[1]].actionid ? doc.answers.aapStep1.depense[found[1]].actionid : null;
        if (actionId) {
          const query = {
            _id: new Mongo.ObjectID(actionId),
          };
          const update = {
            $set: {
              name: modifier.$set[`answers.aapStep1.depense.${found[1]}.poste`],
            },
          };
          // console.log(query, update);
          Actions.direct.update(query, update);
        }
      }
    });
  }
});
