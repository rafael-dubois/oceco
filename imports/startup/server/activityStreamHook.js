/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { Meteor } from 'meteor/meteor';
import { ActivityStream, ActivityStreamReference } from '../../api/collection/activitystream.js';

// Index
ActivityStreamReference.rawCollection().createIndex(
  { type: 1, userId: 1, updated: -1 },
  { name: 'index_first', background: true },
  (e) => {
    if (e) {
      console.log(e);
    }
  },
);

ActivityStreamReference.rawCollection().createIndex(
  {
    type: 1, userId: 1, isUnseen: 1, updated: -1,
  },
  { name: 'index_second', background: true },
  (e) => {
    if (e) {
      console.log(e);
    }
  },
);

ActivityStreamReference.rawCollection().createIndex(
  {
    type: 1, userId: 1, isUnread: 1, updated: -1,
  },
  { name: 'index_three', background: true },
  (e) => {
    if (e) {
      console.log(e);
    }
  },
);

ActivityStreamReference.rawCollection().createIndex(
  {
    verb: 1, targetId: 1, targetType: 1, updated: -1,
  },
  { name: 'index_get_by_construct', background: true },
  (e) => {
    if (e) {
      console.log(e);
    }
  },
);

ActivityStream.after.insert(function (userId, doc) {
  if (doc.notify.id && doc.notify.id) {
    const authorArray = doc.author ? Object.keys(doc.author) : null;
    const objectArray = doc.object ? Object.keys(doc.object) : null;
    const reference = {
      notificationId: doc._id._str,
      type: doc.type,
      updated: doc.updated,
      verb: doc.verb,
      targetId: doc.target.id,
      targetType: doc.target.type,
      notifyObjectType: doc.notify.objectType,
    };
    if (doc.targetEvent && doc.targetEvent.id) {
      reference.targetEvent = doc.targetEvent.id;
    }
    if (doc.targetRoom && doc.targetRoom.id) {
      reference.targetRoom = doc.targetRoom.id;
    }
    if (doc.targetProject && doc.targetProject.id) {
      reference.targetProject = doc.targetProject.id;
    }

    if (authorArray && authorArray.length > 0) {
      [reference.author] = authorArray;
    }
    if (objectArray && objectArray.length > 0) {
      [reference.objectId] = objectArray;
      reference.objectType = doc.object[reference.objectId].type;
    }
    Object.keys(doc.notify.id).forEach((key) => {
      reference.userId = key;
      reference.isUnread = doc.notify.id[key].isUnread === true ? true : false;
      reference.isUnseen = doc.notify.id[key].isUnseen === true ? true : false;
      ActivityStreamReference.insert(reference);
    });
  }
});

ActivityStream.after.update(function (userId, doc, fieldNames, modifier, options) {
  /*console.log(doc);
  console.log(fieldNames);
  console.log(modifier);
  console.log(options);*/
  if (modifier && modifier.$unset) {
    const [key] = Object.keys(modifier.$unset);
    const regex = /notify\.id\.([^\.]+)\.isUnread/;
    const found = key.match(regex);
    if (found && found.length > 1) {
      const query = {
        notificationId: doc._id._str,
        userId: found[1],
      };
      const update = {
        $set: {
          isUnread: false,
        },
      };
      ActivityStreamReference.update(query, update);
    }

    const regex2 = /notify\.id\.([^\.]+)\.isUnseen/;
    const found2 = key.match(regex2);
    if (found2 && found2.length > 1) {
      const query = {
        notificationId: doc._id._str,
        userId: found2[1],
      };
      const update = {
        $set: {
          isUnseen: false,
        },
      };
      ActivityStreamReference.update(query, update);
    }
  }
});
