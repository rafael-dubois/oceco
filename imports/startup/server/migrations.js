/* global Migrations */
/* eslint-disable object-shorthand */
import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import { Answers } from '../../api/collection/answers.js';
import { Forms } from '../../api/collection/forms.js';
import { Actions } from '../../api/collection/actions.js';

function parseBool(val) {
  if ((typeof val === 'string' && (val.toLowerCase() === 'true' || val.toLowerCase() === 'yes')) || val === 1) { return true; }
  if ((typeof val === 'string' && (val.toLowerCase() === 'false' || val.toLowerCase() === 'no')) || val === 0) { return false; }
  return null;
}

Migrations.config({
  // Log job run details to console
  log: true,

  // Use a custom logger function (defaults to Meteor's logging package)
  logger: null,

  // Enable/disable logging "Not migrating, already at version {number}"
  logIfLatest: true,

  // migrations collection name to use in the database
  collectionName: 'migrationsdb',
});

Migrations.add({
  version: 1,
  name: 'modify answers type correct',
  up: function () {
    Forms.find({ subType: 'ocecoform', type: 'aap' }).forEach(function (form) {
      Answers.find({ 'answers.aapStep1': { $exists: true }, form: form._id._str }).forEach(function (doc) {
        if (doc.project && doc.project.startDate) {
          if (typeof (doc.project.startDate) === 'string') {
            doc.project.startDate = new Date(moment(doc.project.startDate, 'D/M/YYYY').toISOString());
            Answers.update({ _id: doc._id }, { $set: { 'project.startDate': doc.project.startDate } });
          }
        }
        if (doc.answers && doc.answers.aapStep1 && doc.answers.aapStep1.depense) {
          Object.keys(doc.answers.aapStep1.depense).forEach(function (key) {
            // console.log(key);
            if (typeof (doc.answers.aapStep1.depense[key].date) === 'string') {
              doc.answers.aapStep1.depense[key].date = new Date(moment(doc.answers.aapStep1.depense[key].date, 'D/M/YYYY').toISOString());
            }
            if (typeof (doc.answers.aapStep1.depense[key].price) === 'string') {
              doc.answers.aapStep1.depense[key].price = parseFloat(doc.answers.aapStep1.depense[key].price);
            }
            if (doc.answers.aapStep1.depense[key].financer) {
              Object.keys(doc.answers.aapStep1.depense[key].financer).forEach(function (keyf) {
                if (typeof (doc.answers.aapStep1.depense[key].financer[keyf].date) === 'string') {
                  doc.answers.aapStep1.depense[key].financer[keyf].date = new Date(moment(doc.answers.aapStep1.depense[key].financer[keyf].date, 'D/M/YYYY').toISOString());
                }
                if (typeof (doc.answers.aapStep1.depense[key].financer[keyf].amount) === 'string') {
                  doc.answers.aapStep1.depense[key].financer[keyf].amount = parseFloat(doc.answers.aapStep1.depense[key].financer[keyf].amount);
                }
              });
            }
            if (doc.answers.aapStep1.depense[key].estimates) {
              Object.keys(doc.answers.aapStep1.depense[key].estimates).forEach(function (keye) {
                if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].date) === 'string') {
                  doc.answers.aapStep1.depense[key].estimates[keye].date = new Date(moment(doc.answers.aapStep1.depense[key].estimates[keye].date, 'D/M/YYYY').toISOString());
                }
                if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].price) === 'string') {
                  doc.answers.aapStep1.depense[key].estimates[keye].price = parseFloat(doc.answers.aapStep1.depense[key].estimates[keye].price);
                }
                if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].days) === 'string') {
                  doc.answers.aapStep1.depense[key].estimates[keye].days = parseInt(doc.answers.aapStep1.depense[key].estimates[keye].days);
                }
                if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].selected) === 'string') {
                  doc.answers.aapStep1.depense[key].estimates[keye].selected = parseBool(doc.answers.aapStep1.depense[key].estimates[keye].selected);
                }
              });
            }
            if (doc.answers.aapStep1.depense[key].payement) {
              Object.keys(doc.answers.aapStep1.depense[key].payement).forEach(function (keyp) {
                if (typeof (doc.answers.aapStep1.depense[key].payement[keyp].date) === 'string') {
                  doc.answers.aapStep1.depense[key].payement[keyp].date = new Date(moment(doc.answers.aapStep1.depense[key].payement[keyp].date, 'D/M/YYYY').toISOString());
                }
                if (typeof (doc.answers.aapStep1.depense[key].payement[keyp].amount) === 'string') {
                  doc.answers.aapStep1.depense[key].payement[keyp].amount = parseFloat(doc.answers.aapStep1.depense[key].payement[keyp].amount);
                }
              });
            }
            // console.log(EJSON.stringify(doc));
            Answers.update({ _id: doc._id }, { $set: { 'answers.aapStep1.depense': doc.answers.aapStep1.depense } });
          });
        }
        if (!Array.isArray(doc.status)) {
          delete doc.status;
          Answers.update({ _id: doc._id }, { $unset: { status: '' } });
        }
      });
    });

    return true;
  },
  down: function () {
    return true;
  },
});

Migrations.add({
  version: 2,
  name: 'modify actions type correct',
  up: function () {
    Actions.find({}).forEach(function (doc) {
      if (typeof (doc.credits) === 'string') {
        doc.credits = parseFloat(doc.credits);
      }
      /* if (typeof (doc.startDate) === 'string') {
          doc.startDate = new Date(moment(doc.startDate, 'D/M/YYYY').toISOString());
        }
        if (typeof (doc.endDate) === 'string') {
          doc.endDate = new Date(moment(doc.endDate, 'D/M/YYYY').toISOString());
        } */
      if (typeof (doc.min) === 'string') {
        doc.min = parseFloat(doc.min);
      }
      if (typeof (doc.max) === 'string') {
        doc.max = parseFloat(doc.max);
      }
      if (doc.options && doc.options.creditAddPorteur && typeof (doc.options.creditAddPorteur) === 'string') {
        doc.options.creditAddPorteur = parseBool(doc.options.creditAddPorteur);
      }
      if (doc.options && doc.options.creditSharePorteur && typeof (doc.options.creditSharePorteur) === 'string') {
        doc.options.creditSharePorteur = parseBool(doc.options.creditSharePorteur);
      }
      if (doc.options && doc.options.possibleStartActionBeforeStartDate && typeof (doc.options.possibleStartActionBeforeStartDate) === 'string') {
        doc.options.possibleStartActionBeforeStartDate = parseBool(doc.options.possibleStartActionBeforeStartDate);
      }
      if (doc.noStartDate && typeof (doc.noStartDate) === 'string') {
        doc.noStartDate = parseBool(doc.noStartDate);
      }
      if (doc.tasks) {
        Object.keys(doc.tasks).forEach(function (key) {
          if (doc.tasks[key].checked && typeof (doc.tasks[key].checked) === 'string') {
            doc.tasks[key].checked = parseBool(doc.tasks[key].checked);
          }
          /* if (doc.tasks[key].createdAt && typeof (doc.tasks[key].createdAt) === 'string') {
            doc.tasks[key].createdAt = new Date(moment(doc.tasks[key].createdAt, 'D/M/YYYY').toISOString());
          }
          if (doc.tasks[key].checkedAt && typeof (doc.tasks[key].checkedAt) === 'string') {
            doc.tasks[key].checkedAt = new Date(moment(doc.tasks[key].checkedAt, 'D/M/YYYY').toISOString());
          } */
          if (typeof (doc.tasks[key].credits) === 'string') {
            doc.tasks[key].credits = parseFloat(doc.tasks[key].credits);
          }
          if (doc.tasks[key].endDate && typeof (doc.tasks[key].endDate) === 'string') {
            doc.tasks[key].endDate = new Date(moment(doc.tasks[key].endDate, 'D/M/YYYY').toISOString());
          }
          if (doc.tasks[key].payed && typeof (doc.tasks[key].payed) === 'string') {
            doc.tasks[key].payed = parseBool(doc.tasks[key].payed);
          }
        });
      }
      Actions.update({ _id: doc._id }, { $set: doc });
    });
    return true;
  },
  down: function () {
    return true;
  },
});

/* Migrations.add({
  version: 3,
  name: 'modify checkBox radio dans answers, forms, inputs',
  up: function () {
    Answers.find({ 'answers.aapStep1.titre': { $exists: true } }).forEach(function (v) {
      if (v.answers && v.answers.aapStep1 && v.answers.aapStep1.multiCheckboxPlusinterventionArea) {
        const interventionArea = [];
        v.answers.aapStep1.multiCheckboxPlusinterventionArea.forEach(function (vIntArea) {
          interventionArea.push(Object.keys(vIntArea)[0]);
        });
        Answers.update({ _id: v._id }, { $set: { 'answers.aapStep1.interventionArea': interventionArea } });
        Answers.update({ _id: v._id }, { $unset: { 'answers.aapStep1.multiCheckboxPlusinterventionArea': 1 } });
      }

      if (v.answers && v.answers.aapStep1 && v.answers.aapStep1.multiCheckboxPlusurgency) {
        const urgency = [];
        v.answers.aapStep1.multiCheckboxPlusurgency.forEach(function (vUrgency) {
          urgency.push(Object.keys(vUrgency)[0]);
        });
        Answers.update({ _id: v._id }, { $set: { 'answers.aapStep1.urgency': urgency } });
        Answers.update({ _id: v._id }, { $unset: { 'answers.aapStep1.multiCheckboxPlusurgency': 1 } });
      }

      if (v.answers && v.answers.aapStep1 && v.answers.aapStep1.multiRadioaxesTFPB && v.answers.aapStep1.multiRadioaxesTFPB.value) {
        const axesTFPB = v.answers.aapStep1.multiRadioaxesTFPB.value;
        Answers.update({ _id: v._id }, { $set: { 'answers.aapStep1.axesTFPB': axesTFPB } });
        Answers.update({ _id: v._id }, { $unset: { 'answers.aapStep1.multiRadioaxesTFPB': 1 } });
      }
      if (v.answers && v.answers.aapStep1 && v.answers.aapStep1.multiRadiopublicCible && v.answers.aapStep1.multiRadiopublicCible.value) {
        const publicCible = v.answers.aapStep1.multiRadiopublicCible.value;
        Answers.update({ _id: v._id }, { $set: { 'answers.aapStep1.publicCible': publicCible } });
        Answers.update({ _id: v._id }, { $unset: { 'answers.aapStep1.multiRadiopublicCible': 1 } });
      }
    });

    Forms.find({ type: 'aap' }).forEach(function (v) {
      if (v.params) {
        Object.keys(v.params).forEach(function (k) {
          if (v.params[k] && v.params[k].global) {
            const isCheckboxBeginWith = new RegExp('^' + 'multiCheckboxPlus').test(k);
            const isRadioBeginWith = new RegExp('^' + 'multiRadio').test(k);

            let key = '';
            if (isCheckboxBeginWith) {
              key = k.replace('multiCheckboxPlus', 'checkboxNew');
            } else if (isRadioBeginWith) {
              key = k.replace('multiRadio', 'radioNew');
            }

            if (key !== '') {
              const set = {};
              set[`params.${key}`] = v.params[k].global;
              Forms.update({ _id: v._id }, { $set: set });

              const unset = {};
              unset[`params.${key}.dependOn`] = 1;
              unset[`params.${k}`] = 1;
              Forms.update({ _id: v._id }, { $unset: unset });
            }
          }
        });
      }
    });
    return true;
  },
  down: function () {
    return true;
  },
}); */

Meteor.startup(() => {
  // todo : decommenter pour activer les migrations
  Migrations.migrateTo('latest');
});
