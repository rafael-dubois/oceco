// moment.js makes `moment` global on the window (or global) object, while Meteor expects a file-scoped global variable
//import dayjs from 'dayjs';
var dayjs = require('dayjs');

require('dayjs/locale/en');
require('dayjs/locale/fr');
//import 'dayjs/locale/en';
//import 'dayjs/locale/fr';

var duration = require('dayjs/plugin/duration');
var utc = require('dayjs/plugin/utc');
var relativeTime = require('dayjs/plugin/relativeTime');
var localizedFormat = require('dayjs/plugin/localizedFormat');
var customParseFormat = require('dayjs/plugin/customParseFormat');
//import duration from 'dayjs/plugin/duration';
//import utc from 'dayjs/plugin/utc';
//import relativeTime from 'dayjs/plugin/relativeTime';
//import localizedFormat from 'dayjs/plugin/localizedFormat';

dayjs.extend(duration);
dayjs.extend(utc);
dayjs.extend(relativeTime);
dayjs.extend(localizedFormat);
dayjs.extend(customParseFormat);

// eslint-disable-next-line no-undef
// eslint-disable-next-line no-global-assign
global.moment = dayjs;
global.moment.isMoment = dayjs.isDayjs;

// eslint-disable-next-line import/prefer-default-export
export { dayjs as moment };
